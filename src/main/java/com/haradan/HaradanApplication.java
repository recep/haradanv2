package com.haradan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableAsync
@EnableRetry
@EnableCaching
@EnableScheduling
@SpringBootApplication(scanBasePackages = {"com.haradan"})
@EnableConfigurationProperties
public class HaradanApplication {
    public static void main(String[] args) {
        SpringApplication.run(HaradanApplication.class, args);
    }
}

