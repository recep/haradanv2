package com.haradan.domain.dto;

import com.haradan.common.enumeration.ArticleType;
import com.haradan.common.enumeration.EntityStatus;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ArticleSearchDto {
    private String id;
    private EntityStatus status;
    private ArticleType type;
}