package com.haradan.domain.dto;

import lombok.Data;

import java.util.Map;

@Data
public class SmsDto {
    private String phone;
    private String subject;
    private String text;
    private Map<String, Object> props;
}