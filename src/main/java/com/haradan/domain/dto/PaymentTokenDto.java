package com.haradan.domain.dto;

import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement
public class PaymentTokenDto {
    private String merchantId;
    private String productId;
    private String productName;
    private String paymentAmount;
    private String userName;
    private String userEmail;
    private String userAddress;
    private String userPhone;
    private String ip;
    private Object[][] userBasketList;
    private String status;
    private String okUrl;
    private String failUrl;
    private String hash;
    private String failedReasonCode;
    private String failedReasonMsg;
}