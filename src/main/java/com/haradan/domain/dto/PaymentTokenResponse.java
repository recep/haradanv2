package com.haradan.domain.dto;

import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashMap;
import java.util.Map;

@Data
@XmlRootElement
public class PaymentTokenResponse {

    private String status;
    private String token;
    private String reason;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
}