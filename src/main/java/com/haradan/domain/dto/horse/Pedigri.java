package com.haradan.domain.dto.horse;

import lombok.Data;

@Data
public class Pedigri {
	private String father;
	private String mother;
}