package com.haradan.domain.dto.horse;

import com.haradan.common.enumeration.horse.SiblingType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HorseSiblingDto {
	private SiblingType siblingType;
	private String siblingName;
	private String fatherName;
	private String motherName;
	private Integer raceCount;
	private Integer first;
	private Integer second;
	private Integer third;
	private Integer fourth;
	private Long earning;
}
