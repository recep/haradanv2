package com.haradan.domain.dto.horse;

import com.haradan.common.enumeration.EntityStatus;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
public class HorseDto {
	private static final long serialVersionUID = 20120824L;
	private EntityStatus status;
	private Long tjkNo;
	private Long identityNo;
	private String name;
	private String race;
	private Integer age;
	private String donu;
	private String gender;
	private String fatherName;
	private String motherName;
	private String maidenFatherName;
	private LocalDate birthdate;
	private Integer hPoint;
	private String owner;
	private String grower;
	private String coach;
	private String last6RaceResult;
	private LocalDate lastRaceDate;
	private Long earning;
	private String title;
	private List<HorseStatisticDto> horseStatistics;
	private List<HorseSiblingDto> horseSiblings;
	private List<Pedigri> pedigri;
}
