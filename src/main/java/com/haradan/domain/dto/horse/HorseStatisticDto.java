package com.haradan.domain.dto.horse;

import com.haradan.common.enumeration.horse.PistType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HorseStatisticDto {
	private PistType pistType;
	private Integer year;
	private Integer raceCount;
	private Integer first;
	private Integer second;
	private Integer third;
	private Integer fourth;
	private Integer fifth;
	private Long earning;
}
