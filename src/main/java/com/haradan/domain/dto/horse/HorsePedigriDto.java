package com.haradan.domain.dto.horse;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HorsePedigriDto {
	private String pedigri;
}