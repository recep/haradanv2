package com.haradan.domain.dto.horse;

import com.haradan.domain.dao.entity.horse.HorseSummary;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class HorseSummaryDto {
    private List<HorseSummary> horseSummaries = new ArrayList<>();
    private Integer horseCount = 0;
}