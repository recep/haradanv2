package com.haradan.domain.dto;

import com.haradan.common.enumeration.BannerType;
import com.haradan.common.enumeration.EntityStatus;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StableSearchDto {
    private String name;
}