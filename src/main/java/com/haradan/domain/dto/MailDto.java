package com.haradan.domain.dto;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class MailDto {
    private String from = "info@haradan.com";
    private String mailTo;
    private String subject;
    private String template;
    private List<Object> attachments;
    private Map<String, Object> props;
}