package com.haradan.domain.dto;

import com.haradan.common.enumeration.DopingType;
import com.haradan.common.enumeration.EntityStatus;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AdDopingSearchDto {
    private int page;
    private int size;
    private String sort;
    private String direction;
    private String advertId;
    private EntityStatus status;
    private DopingType type;
}
