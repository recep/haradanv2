package com.haradan.domain.dto;

import com.haradan.common.enumeration.EntityStatus;
import lombok.Builder;
import lombok.Data;

import java.util.Map;
import java.util.Set;

@Data
@Builder
public class SearchDto {
    private int page;
    private int size;
    private String sort;
    private String direction;
    private String id;
    private Long advertNo;
    private String categorySearchText;
    private Set<String> categoryIds;
    private String searchText;
    private EntityStatus status;
    private Integer dateInterval;
    private Integer dayBefore;
    private Boolean hasPhoto;
    private Double minPrice;
    private Double maxPrice;
    private String city;
    private String district;
    private String userId;
    private Map<String, String> properties;
}
