
package com.haradan.domain.dto.facebook;

import java.util.HashMap;
import java.util.Map;

public class FacebookLoginResponse {

private String email;
private String first_name;
private String last_name;
private String name;
private String id;
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

public String getEmail() {
return email;
}

public void setEmail(String email) {
this.email = email;
}

public String getFirst_name() {
return first_name;
}

public void setFirst_name(String first_name) {
this.first_name = first_name;
}

public String getLast_name() {
return last_name;
}

public void setLast_name(String last_name) {
this.last_name = last_name;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}