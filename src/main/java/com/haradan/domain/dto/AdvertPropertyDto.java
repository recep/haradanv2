package com.haradan.domain.dto;

import java.io.Serializable;

import com.haradan.common.enumeration.PropertyType;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class AdvertPropertyDto implements Serializable {
    private static final long serialVersionUID = 3809104814337239678L;
    private String identifier;
    private PropertyType type;
    private String name;
    private String value;
    private String propertyId;
}
