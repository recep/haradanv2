package com.haradan.domain.dto;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class CategoryDto implements Serializable {
    private static final long serialVersionUID = 3809104814337239678L;
    private String identifier;
    private String name;
    private String searchText;
    private Double price;
}
