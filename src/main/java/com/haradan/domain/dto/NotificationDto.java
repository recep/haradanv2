package com.haradan.domain.dto;

import com.haradan.common.dao.entity.BaseEntity;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.common.enumeration.NotificationType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class NotificationDto<T extends BaseEntity> {
    private T entity;
    private NotificationType type;
    private EntityStatus oldStatus;
    private EntityStatus status;
    private Integer amount;
}
