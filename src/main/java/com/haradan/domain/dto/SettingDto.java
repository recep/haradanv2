package com.haradan.domain.dto;

import lombok.Data;

@Data
public class SettingDto {
    private boolean showPhone;
    private boolean showEmail;
    private boolean notification;
    private boolean campaignNotification;
}