package com.haradan.domain.schedular;

import com.haradan.common.enumeration.EntityStatus;
import com.haradan.domain.dao.repository.DopingRepository;
import com.haradan.domain.dto.horse.HorseSummaryDto;
import com.haradan.domain.service.AdvertService;
import com.haradan.domain.service.HorseService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@Slf4j
@Component
@RequiredArgsConstructor
@ConditionalOnExpression(value = "${cron.enabled:false}")
public class ScheduledJob {

    private final DopingRepository dopingRepository;
    private final HorseService horseService;
    private final AdvertService advertService;

    @Transactional
    @Scheduled(cron = "${cron.expire.doping}")
    public void expireDopings() {
        try {
            log.info("Expire Dopings job started.");
            dopingRepository.findByStatus(EntityStatus.ACTIVE).stream()
                    .filter(d -> d.getExpireDate().isBefore(LocalDate.now()))
                    .forEach(d -> {
                        d.setStatus(EntityStatus.PASSIVE);
                        dopingRepository.save(d);
                        log.info("{} Advert's Doping {} expired.", d.getAdvert().getAdvertNo(), d.getIdentifier());
                    });
            log.info("Expire Dopings job completed.");
        } catch (Exception e) {
            log.error("Expire Dopings job failed.", e);
        }
    }

    @Transactional
    @Scheduled(cron = "${cron.expire.advert}")
    public void expireAdverts() {
        try {
            log.info("Expire Adverts job started.");
            advertService.expireAdverts(300, EntityStatus.ACTIVE);
            advertService.expireAdverts(7, EntityStatus.NOT_COMPLETED);
            advertService.expireAdverts(30, EntityStatus.WAITING_APPROVAL);
            log.info("Expire Adverts job completed.");
        } catch (Exception e) {
            log.error("Expire Adverts job failed.", e);
        }
    }

    @Scheduled(cron = "${cron.horse.summary.inquiry}")
    public void saveHorsesSummary() {
        try {
            log.info("Horse summary inquiry job executed.");
            HorseSummaryDto horseSummaryDto = saveHorsesSummary(0);
            int horseCount = horseSummaryDto.getHorseCount() > 0 ? horseSummaryDto.getHorseCount() : 70000;

            final ExecutorService executorService = Executors.newFixedThreadPool(10);
            IntStream.range(1, horseCount / 50 + 1).forEach(i -> executorService.execute(() -> saveHorsesSummary(i)));
            executorService.shutdown();
            try {
                executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
            } catch (InterruptedException e) {
                log.error("Horse summary inquiry job executorService interrupted.");
            }
            log.info("Horse summary inquiry job processed succesfully.");
        } catch (Exception e) {
            log.error("An exception occured on Horse summary inquiry Job!", e);
        }
    }

    private HorseSummaryDto saveHorsesSummary(int pageNumber) {
        try {
            HorseSummaryDto horseSummaryDto = horseService.getHorsesSummary(pageNumber);
            horseService.saveHorsesSummary(horseSummaryDto.getHorseSummaries());
            log.info("page {} process completed.", pageNumber);
            return horseSummaryDto;
        } catch (Exception e) {
            log.error("page {} process failed.", pageNumber, e);
            return new HorseSummaryDto();
        }
    }
}