package com.haradan.domain.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.common.enumeration.PaymentProductType;
import com.haradan.common.exception.exceptions.MicroException;
import com.haradan.common.util.Jaxb;
import com.haradan.common.util.Utils;
import com.haradan.domain.dao.entity.Advert;
import com.haradan.domain.dao.entity.Doping;
import com.haradan.domain.dao.entity.Payment;
import com.haradan.domain.dao.repository.PaymentRepository;
import com.haradan.domain.dto.PaymentTokenDto;
import com.haradan.domain.dto.PaymentTokenResponse;
import com.haradan.domain.mapper.PaymentMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class PaymentService {
    private static final String SUCCESS = "success";
    private static final String OK = "OK";
    private static final String PAYTR_NOTIFICATION_FAILED_BAD_HASH = "PAYTR notification failed: bad hash";
    private static final String HMAC_SHA256 = "HmacSHA256";
    private static final String URL = "https://www.paytr.com/odeme/api/get-token";
    private static final String MERCHANT_ID = "124431";
    private static final String MERCHANT_KEY = "JyL6sSQuLYKZg9zc";
    private static final String MERCHANT_SALT = "sEhr7U3zbwEfm47u";
    ObjectMapper mapper = new ObjectMapper();
    private final PaymentRepository paymentRepository;
    private final PaymentMapper paymentMapper;
    private final AdvertService advertService;

    @Value("${system.parameter.baseUrl}")
    private String baseUrl;

    public Payment get(String id) {
        return paymentRepository.findById(id).orElseThrow(() -> MicroException.newDataNotFoundException(Payment.class.getSimpleName(), id));
    }

    public Page<Payment> search(String productId, String email, String name, EntityStatus status, Pageable pageable) {
        return paymentRepository.findAll(PaymentRepository.QueryGeneration.search(productId, email, name, status), pageable);
    }

    public String notify(HttpServletRequest request) throws InvalidKeyException, NoSuchAlgorithmException, JAXBException {
        PaymentTokenDto paymentTokenDto = new PaymentTokenDto();
        paymentTokenDto.setMerchantId(request.getParameter("merchant_oid"));
        paymentTokenDto.setStatus(request.getParameter("status"));
        paymentTokenDto.setPaymentAmount(request.getParameter("total_amount"));
        paymentTokenDto.setHash(request.getParameter("hash"));

        log.error("PAYTR, payment notification!!! merchant_oid: {} , status: {} ", paymentTokenDto.getMerchantId(), paymentTokenDto.getStatus());
        String hash = hash(paymentTokenDto.getMerchantId() + MERCHANT_SALT + paymentTokenDto.getStatus() + paymentTokenDto.getPaymentAmount());

        if (!hash.equals(paymentTokenDto.getHash())) {
            log.error("PAYTR, notification failed: bad hash!!! merchant_oid: {} , status: {} ", paymentTokenDto.getMerchantId(), paymentTokenDto.getStatus());
            updatePaymentLog(paymentTokenDto, PAYTR_NOTIFICATION_FAILED_BAD_HASH, EntityStatus.PASSIVE);
            return PAYTR_NOTIFICATION_FAILED_BAD_HASH;
        }
        if (SUCCESS.equals(paymentTokenDto.getStatus())) { // Ödeme Onaylandı
            log.warn("PAYTR, notification success!!! merchant_oid: {} , status: {} ", paymentTokenDto.getMerchantId(), paymentTokenDto.getStatus());
            updatePaymentLog(paymentTokenDto, OK, EntityStatus.ACTIVE);
            approveDoping(paymentTokenDto.getMerchantId());
            updateAdvertCharged(paymentTokenDto.getMerchantId(), true);
            return OK;
        } else { // Ödemeye Onay Verilmedi
            paymentTokenDto.setFailedReasonCode(request.getParameter("failed_reason_code"));
            paymentTokenDto.setFailedReasonMsg(request.getParameter("failed_reason_msg"));
            log.error("PAYTR, notification error!!! merchant_oid: {}, status: {}, failed_reason_code: {} , failed_reason_msg: {}"
                    , paymentTokenDto.getMerchantId(), paymentTokenDto.getStatus(), paymentTokenDto.getFailedReasonCode(), paymentTokenDto.getFailedReasonMsg());
            updatePaymentLog(paymentTokenDto, OK, EntityStatus.PASSIVE);
            return OK;
        }
    }

    private String hash(String token) throws NoSuchAlgorithmException, InvalidKeyException {
        Mac shaHMAC = Mac.getInstance(HMAC_SHA256);
        SecretKeySpec secretKey = new SecretKeySpec(MERCHANT_KEY.getBytes(StandardCharsets.UTF_8), HMAC_SHA256);
        shaHMAC.init(secretKey);
        return Base64.encodeBase64String(shaHMAC.doFinal(token.getBytes(StandardCharsets.UTF_8)));
    }

    public String getPaytrIframe(HttpServletRequest request, Advert advert, int amount)
            throws JsonProcessingException, NoSuchAlgorithmException, InvalidKeyException, JAXBException {
        PaymentTokenDto paymentTokenDto = new PaymentTokenDto();
        paymentTokenDto.setProductId(advert.getIdentifier());
        paymentTokenDto.setProductName("advert");
        paymentTokenDto.setMerchantId(paymentTokenDto.getProductName() + System.currentTimeMillis());
        paymentTokenDto.setPaymentAmount(amount + "00");
        paymentTokenDto.setUserName(advert.getUser().getFullName());
        paymentTokenDto.setUserEmail(advert.getUser().getEmail());
        paymentTokenDto.setUserPhone(Utils.getshortPhoneNumber(advert.getUser().getPhoneNumber()));
        paymentTokenDto.setUserAddress(advert.getLocation());
        paymentTokenDto.setIp(Utils.getExternalIp(request, baseUrl));
        paymentTokenDto.setOkUrl(baseUrl + "ilan-ver/sonuc/" + paymentTokenDto.getProductId());
        paymentTokenDto.setFailUrl(baseUrl + "ilan-ver/sonuc/" + paymentTokenDto.getProductId());
        String iframeToken = getPaytrIframeToken(paymentTokenDto);
        return "<script src=\"https://www.paytr.com/js/iframeResizer.min.js\"></script>      "
                + "<iframe src=\"https://www.paytr.com/odeme/guvenli/" + iframeToken + "\" id=\"paytriframe\"  "
                + "frameborder=\"0\" scrolling=\"no\" style=\"width: 100%;\"></iframe> <script>iFrameResize({},'#paytriframe');</script>";
    }

    public String getPaytrIframeToken(PaymentTokenDto paymentTokenDto) throws JAXBException, JsonProcessingException, NoSuchAlgorithmException, InvalidKeyException {
        String userBasket = getUserBasket(paymentTokenDto);
        MultiValueMap<String, String> paramMap = new LinkedMultiValueMap<>();
        paramMap.add("merchant_id", MERCHANT_ID);
        paramMap.add("merchant_oid", paymentTokenDto.getMerchantId());
        paramMap.add("user_ip", paymentTokenDto.getIp());
        paramMap.add("email", paymentTokenDto.getUserEmail());
        paramMap.add("payment_amount", paymentTokenDto.getPaymentAmount());
        paramMap.add("user_name", paymentTokenDto.getUserName());
        paramMap.add("user_address", paymentTokenDto.getUserAddress());
        paramMap.add("user_phone", paymentTokenDto.getUserPhone());
        paramMap.add("merchant_ok_url", paymentTokenDto.getOkUrl());
        paramMap.add("merchant_fail_url", paymentTokenDto.getFailUrl());
        paramMap.add("user_basket", userBasket);
        paramMap.add("debug_on", PaymentHelper.DEBUG_ON);
        paramMap.add("test_mode", PaymentHelper.TEST_MODE);
        paramMap.add("no_installment", PaymentHelper.NO_INSTALLMENT);
        paramMap.add("max_installment", PaymentHelper.MAX_INSTALLMENT);
        paramMap.add("timeout_limit", PaymentHelper.TIMEOUT_LIMIT);
        paramMap.add("currency", PaymentHelper.CURRENCY);
        paramMap.add("paytr_token", getpaytrToken(paymentTokenDto));
        PaymentTokenResponse paymentTokenResponse = callPaytrTokenService(URL, paramMap);
        createPaymentLog(paymentTokenDto, paymentTokenResponse);
        return paymentTokenResponse.getToken();
    }

    private String getUserBasket(PaymentTokenDto paymentTokenDto) throws JsonProcessingException {
        String productDesc = PaymentProductType.getPaymentProduct(paymentTokenDto.getProductName()).getDesc();
        Object[][] userBasketList = {new Object[]{productDesc, Utils.removeChar(paymentTokenDto.getPaymentAmount(), 2), 1}};
        paymentTokenDto.setUserBasketList(userBasketList);
        String userBasket = (new ObjectMapper()).writeValueAsString(userBasketList);
        userBasket = Base64.encodeBase64String(userBasket.getBytes(StandardCharsets.UTF_8));
        return userBasket;
    }

    private String getpaytrToken(PaymentTokenDto paymentTokenDto) throws JsonProcessingException, InvalidKeyException, NoSuchAlgorithmException {
        // Token oluşturma fonksiyonu, değiştirilmeden kullanılmalıdır.
        String token = MERCHANT_ID + paymentTokenDto.getIp() + paymentTokenDto.getMerchantId()
                + paymentTokenDto.getUserEmail() + paymentTokenDto.getPaymentAmount() + getUserBasket(paymentTokenDto)
                + PaymentHelper.NO_INSTALLMENT + PaymentHelper.MAX_INSTALLMENT + PaymentHelper.CURRENCY
                + PaymentHelper.TEST_MODE + MERCHANT_SALT;
        return hash(token);
    }

    private void updatePaymentLog(PaymentTokenDto paymentTokenDto, String response, EntityStatus entityStatus) throws JAXBException {
        log.error("INFO: getMerchantId: " + paymentTokenDto.getMerchantId());
        Marshaller marshaller = Jaxb.get(PaymentTokenDto.class).getMarshaller();
        StringWriter request = new StringWriter();
        marshaller.marshal(paymentTokenDto, request);
        Payment payment = paymentRepository.findByMerchantId(paymentTokenDto.getMerchantId());
        if (payment == null) {
            log.error("Payment Not Found! MerchantId: " + paymentTokenDto.getMerchantId());
            return;
        }
        payment.setNotifyRequest(request.toString());
        payment.setNotifyResponse(response);
        payment.setStatus(entityStatus);
        paymentRepository.save(payment);
    }

    private void createPaymentLog(PaymentTokenDto paymentTokenDto, PaymentTokenResponse paymentTokenResponse) throws JAXBException {
        Marshaller marshaller = Jaxb.get(PaymentTokenDto.class).getMarshaller();
        StringWriter request = new StringWriter();
        marshaller.marshal(paymentTokenDto, request);
        marshaller = Jaxb.get(PaymentTokenResponse.class).getMarshaller();
        StringWriter reponse = new StringWriter();
        marshaller.marshal(paymentTokenResponse, reponse);
        Payment payment = paymentMapper.toEntity(paymentTokenDto);
        payment.setPaymentAmount(Utils.removeChar(paymentTokenDto.getPaymentAmount(), 2));
        payment.setTokenRequest(request.toString());
        payment.setTokenResponse(reponse.toString());
        paymentRepository.save(payment);
    }

    private PaymentTokenResponse callPaytrTokenService(String targetUrl, MultiValueMap<String, String> paramMap) {
        RestTemplate restTemplate = new RestTemplate();
        PaymentTokenResponse paymentTokenResponse = new PaymentTokenResponse();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Connect-Type", "text/html;charset=UTF-8");
        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(paramMap, headers);
        try {
            ResponseEntity<String> response = restTemplate.postForEntity(targetUrl, entity, String.class);
            paymentTokenResponse = mapper.readValue(response.getBody(), PaymentTokenResponse.class);
        } catch (Exception e) {
            log.error("Paytr Token servis cagriminda hata olustu! args: {}", paramMap, e);
        }
        return paymentTokenResponse;
    }

    private void approveDoping(String merchantId) {
        Payment payment = paymentRepository.findByMerchantId(merchantId);
        List<Doping> dopings = advertService.getDopingsToActivate(payment.getProductId());
        advertService.updateDopingStatusAsync(payment.getProductId(), dopings);
    }

    private void updateAdvertCharged(String merchantId, boolean charged) {
        Payment payment = paymentRepository.findByMerchantId(merchantId);
        advertService.updateAdvertCharged(payment.getProductId(), charged);
    }

    class PaymentHelper {
        static final String TIMEOUT_LIMIT = "30";
        static final String CURRENCY = "TL";
        static final String DEBUG_ON = "0";
        static final String TEST_MODE = "0";
        static final String NO_INSTALLMENT = "1";
        static final String MAX_INSTALLMENT = "0";
    }
}
