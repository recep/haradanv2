package com.haradan.domain.service;

import com.haradan.common.enumeration.horse.HorseDetailKey;
import com.haradan.common.enumeration.horse.SiblingType;
import com.haradan.common.enumeration.horse.TjkUrlType;
import com.haradan.common.util.HorseInfoUtils;
import com.haradan.common.util.Utils;
import com.haradan.domain.dao.entity.horse.Horse;
import com.haradan.domain.dao.entity.horse.HorsePedigri;
import com.haradan.domain.dao.entity.horse.HorseSibling;
import com.haradan.domain.dao.entity.horse.HorseStatistic;
import com.haradan.domain.dao.entity.horse.HorseSummary;
import com.haradan.domain.dao.repository.HorseRepository;
import com.haradan.domain.dao.repository.HorseSummaryRepository;
import com.haradan.domain.dto.horse.HorseSummaryDto;
import com.haradan.domain.dto.horse.Pedigri;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class HorseService {
    private static final int JSOUP_TIMEOUT = 60000;
    private final HorseRepository horseRepository;
    private final HorseSummaryRepository horseSummaryRepository;

    public Horse getHorseDetailFromDB(Long tjkNo) {
        return horseRepository.findByTjkNo(tjkNo);
    }

    public Horse getHorseDetail(Long tjkNo) throws Exception {
        Horse horse = new Horse();
        horse.setTjkNo(tjkNo);
        Document doc = Jsoup.connect(TjkUrlType.STATISTICS.getUrl(tjkNo.toString())).timeout(JSOUP_TIMEOUT).get();
        Elements tab = doc.select("div.grid_8");
        String key = null;
        for (Element row : tab.select("span")) {
            if (StringUtils.isEmpty(key)) {
                key = row.text();
            } else {
                HorseDetailKey.setdata(horse, key, row.text());
                key = null;
            }
        }
        horse.setHorseStatistics(getStatistics(horse.getTjkNo(), doc));
        return horse;
    }

    @Transactional
    public void saveHorseDetail(Horse horse) {
        Horse current = horseRepository.findByTjkNo(horse.getTjkNo());
        if (current != null) {
            horse.setIdentifier(current.getIdentifier());
            horse.setHorseSiblings(current.getHorseSiblings());
            horse.setHorsePedigri(current.getHorsePedigri());
        }
        horse.getHorseStatistics().forEach(s -> s.setHorse(horse));
        horseRepository.save(horse);
    }

    public List<HorseStatistic> getStatistics(Long tjkNo, Document doc) throws Exception {
        List<HorseStatistic> horseStatistics = new ArrayList<>();
        if (doc == null) {
            doc = Jsoup.connect(TjkUrlType.STATISTICS.getUrl(tjkNo.toString())).timeout(JSOUP_TIMEOUT).get();
        }
        Elements table = doc.select("div.grid_10 table.tablesorter");
        for (Element row : table.select("tbody tr")) {
            HorseStatistic horseStatistic = new HorseStatistic();
            int index = 0;
            for (Element td : row.select("td")) {
                HorseInfoUtils.mapHorseStatistics(index, td.text(), horseStatistic);
                index++;
            }
            horseStatistics.add(horseStatistic);
        }
        return horseStatistics;
    }

    @Async
    public void saveHorseAllData(Long tjkNo) throws Exception {
        Horse current = horseRepository.findByTjkNo(tjkNo);
        if (Objects.isNull(current)) {
            return;
        }
        List<HorseSibling> horseSiblings = getSiblings(tjkNo, current);
        if (!CollectionUtils.isEmpty(current.getHorseSiblings())) {
            current.getHorseSiblings().clear();
            current.getHorseSiblings().addAll(horseSiblings);
        } else {
            current.setHorseSiblings(horseSiblings);
        }
        current.getHorseSiblings().forEach(s -> s.setHorse(current));
        if (current.getHorsePedigri() == null) {
            HorsePedigri horsePedigri = getPedigri(tjkNo);
            current.setHorsePedigri(horsePedigri);
        }
        horseRepository.save(current);
    }

    public List<HorseSibling> getSiblings(Long tjkNo, Horse horse) throws Exception {
        List<HorseSibling> horseSiblings = new ArrayList<>();
        Document doc = Jsoup.connect(TjkUrlType.FAMILY.getUrl(tjkNo.toString())).timeout(JSOUP_TIMEOUT).get();
        Elements table = doc.select("div.grid_24 table.tablesorter");
        for (Element row : table.select("tbody tr")) {
            HorseSibling horseSibling = new HorseSibling();
            int index = 0;
            for (Element td : row.select("td")) {
                HorseInfoUtils.mapHorseSibling(index, td.text(), horseSibling);
                index++;
            }
            if (horse != null) {
                horseSibling.setMotherName(horse.getMotherName());
                horseSibling
                        .setSiblingType(horse.getFatherName().equals(horseSibling.getFatherName()) ? SiblingType.FATHER
                                : SiblingType.MOTHER);
            }
            horseSiblings.add(horseSibling);
        }
        return horseSiblings;
    }

    public HorsePedigri getPedigri(Long tjkNo) throws Exception {
        Document doc = Jsoup.connect(TjkUrlType.PEDIGRI.getUrl(tjkNo.toString())).timeout(JSOUP_TIMEOUT).get();
        Elements table = doc.select("div.grid_24 table.tablesorter");
        HorsePedigri horsePedigri = new HorsePedigri();
        int i = 1, j = 3;
        List<Pedigri> pedigris = new ArrayList<>();
        for (int k = 0; k < 7; k++) {
            pedigris.add(new Pedigri());
        }
        for (Element row : table.select("tbody tr")) {
            for (Element td : row.select("td")) {
                if (td.toString().contains("rowspan=\"4\"")) {
                    setParentText(pedigris, 0, td);
                } else if (td.toString().contains("rowspan=\"2\"")) {
                    boolean isCompleted = setParentText(pedigris, i, td);
                    if (isCompleted) {
                        i++;
                    }
                } else {
                    boolean isCompleted = setParentText(pedigris, j, td);
                    if (isCompleted) {
                        j++;
                    }
                }
            }
        }
        horsePedigri.setPedigri(Utils.getJson(pedigris));
        return horsePedigri;
    }

    private boolean setParentText(List<Pedigri> pedigri, int index, Element td) {
        if (td.toString().contains("#dbdbdb;")) {
            pedigri.get(index).setFather(td.text());
            return false;
        } else {
            pedigri.get(index).setMother(td.text());
            return true;
        }
    }

    public List<HorseSummary> getHorsesSummary(Horse request) {
        Pageable pageable = PageRequest.of(0, 1000);

        return horseSummaryRepository
                .findAll(HorseSummaryRepository.QueryGeneration.search(request.getName()), pageable)
                .get()
                .collect(Collectors.toList());
    }

    @Transactional
    public void saveHorsesSummary(List<HorseSummary> horseSummaries) {
        if (!CollectionUtils.isEmpty(horseSummaries)) {
            horseSummaryRepository.saveAll(horseSummaries);
        }
    }

    @Retryable(value = SocketTimeoutException.class)
    public HorseSummaryDto getHorsesSummary(Integer pageNumber) throws Exception {
        String url = TjkUrlType.BULK_SEARCH.getUrl(pageNumber.toString());
        HorseSummaryDto horseSummaryDto = new HorseSummaryDto();
        try {
            Document doc = Jsoup.connect(url).timeout(HorseService.JSOUP_TIMEOUT).get();
            Element body = doc.body();
            horseSummaryDto.setHorseSummaries(parseHorseSummary(body));
            if (pageNumber == 0 && horseSummaryDto.getHorseCount() == 0) {
                horseSummaryDto.setHorseCount(parseHorseCount(body));
            }
        } catch (Exception e) {
            log.warn("page {} process failed and retried for {}, Exception = {}", pageNumber, url, e.getMessage());
            throw e;
        }
        return horseSummaryDto;
    }

    @Recover
    public HorseSummaryDto recover(SocketTimeoutException e, Integer pageNumber) {
        log.error("page {} process failed.", pageNumber, e);
        return new HorseSummaryDto();
    }

    private int parseHorseCount(Element body) {
        try {
            return body.select("div").stream()
                    .filter(e -> StringUtils.startsWith(e.text(), "Toplam"))
                    .map(element -> Integer.parseInt(element.text().split(" ")[1]))
                    .findAny()
                    .orElse(0);
        } catch (Exception e) {
            log.error("Horse count parsing error.", e);
            return 0;
        }
    }

    private List<HorseSummary> parseHorseSummary(Element body) {
        List<HorseSummary> horseSummaryList = new ArrayList<>();
        body.children()
                .forEach(element -> {
                    try {
                        if (StringUtils.contains(element.attr("href"), "QueryParameter_AtId")) {
                            HorseSummary horse = new HorseSummary();
                            horse.setName(element.text());
                            horseSummaryList.add(horse);
                            horse.setTjkNo((Long) Utils.toObject(Long.class, element.attr("href").split("AtId=")[1]));
                            horse.setRace(element.nextSibling().nextSibling().nextSibling().toString().trim());
                        } else if (StringUtils.contains(element.attr("href"), "QueryParameter_BabaAdi")) {
                            horseSummaryList.get(horseSummaryList.size() - 1).setFatherName(element.text());
                        } else if (StringUtils.contains(element.attr("href"), "QueryParameter_AnneAdi")) {
                            horseSummaryList.get(horseSummaryList.size() - 1).setMotherName(element.text());
                        }
                    } catch (Exception e) {
                        log.error("Parsing error for TjkNo : {}, name : {}",
                                horseSummaryList.get(horseSummaryList.size() - 1).getTjkNo(),
                                horseSummaryList.get(horseSummaryList.size() - 1).getName(),
                                e);
                    }
                });
        return horseSummaryList;
    }

    public static void main(String[] args) throws Exception {
        HorseService service = new HorseService(null, null);
        service.getHorsesSummary(1).getHorseSummaries()
                .forEach(h -> System.out.println(h.getTjkNo() + " - " + h.getName() + " - " + h.getFatherName() + " - " + h.getMotherName() + " - " + h.getRace()));
    }

}
