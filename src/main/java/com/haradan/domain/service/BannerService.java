package com.haradan.domain.service;

import com.haradan.common.exception.exceptions.MicroException;
import com.haradan.common.util.Utils;
import com.haradan.domain.dao.entity.Banner;
import com.haradan.domain.dao.repository.BannerRepository;
import com.haradan.domain.dto.BannerSearchDto;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class BannerService {
    private final BannerRepository bannerRepository;

    public Banner get(String id) {
        return bannerRepository.findById(id).orElseThrow(() -> MicroException.newDataNotFoundException(Banner.class.getSimpleName(), id));
    }

    @Transactional
    @CacheEvict(value = "banner", allEntries = true)
    public Banner put(String id, Banner banner) {
        Banner theReal = bannerRepository.findById(id).orElseThrow(() -> MicroException.newDataNotFoundException(Banner.class.getSimpleName(), id));
        banner.setIdentifier(theReal.getIdentifier());
        banner.setSearchText(Utils.normalizeCharacters(banner.getName()));
        return bannerRepository.save(banner);
    }

    @Transactional
    @CacheEvict(value = "banner", allEntries = true)
    public void delete(String id) {
        Banner theReal = bannerRepository.findById(id).orElseThrow(() -> MicroException.newDataNotFoundException(Banner.class.getSimpleName(), id));
        bannerRepository.delete(theReal);
    }

    @Transactional
    @CacheEvict(value = "banner", allEntries = true)
    public Banner save(Banner banner) {
        banner.setSearchText(Utils.normalizeCharacters(banner.getName()));
        return bannerRepository.save(banner);
    }

    @Cacheable(value = "banner")
    public Page<Banner> search(BannerSearchDto searchDto, Pageable pageable) {
        return bannerRepository.findAll(BannerRepository.QueryGeneration.search(searchDto), pageable);
    }
}
