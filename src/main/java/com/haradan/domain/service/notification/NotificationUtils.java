package com.haradan.domain.service.notification;

import com.haradan.common.enumeration.MailType;
import com.haradan.common.enumeration.SmsType;
import com.haradan.domain.dao.entity.Advert;
import com.haradan.domain.dao.entity.Contact;
import com.haradan.domain.dao.entity.User;
import com.haradan.domain.dto.MailDto;
import com.haradan.domain.dto.SmsDto;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class NotificationUtils {

    public static String baseUrl;
    public static String haradanEmail;

    public static SmsDto prepareSms(SmsType type, String phoneNumber) {
        SmsDto smsDto = new SmsDto();
        smsDto.setPhone(phoneNumber);
        smsDto.setText(type.getText());
        return smsDto;
    }

    public static SmsDto prepareSms(SmsType type, User user) {
        SmsDto smsDto = new SmsDto();
        smsDto.setPhone(user.getPhoneNumber());
        smsDto.setText(type.getText());
        smsDto.setProps(getModel(user));
        return smsDto;
    }

    public static SmsDto prepareSms(SmsType type, Advert advert) {
        SmsDto smsDto = new SmsDto();
        smsDto.setPhone(advert.getUser().getPhoneNumber());
        smsDto.setText(type.getText(advert.getAdvertNo().toString()));
        smsDto.setProps(getModel(advert.getUser()));
        return smsDto;
    }

    public static MailDto prepareMail(MailType type, User user) {
        MailDto mailDto = new MailDto();
        mailDto.setMailTo(user.getEmail());
        mailDto.setTemplate(type.getTemplate());
        mailDto.setSubject(type.getSubject());
        mailDto.setProps(getModel(user));
        return mailDto;
    }

    public static MailDto prepareMail(MailType type, Advert advert, Integer amount) {
        MailDto mailDto = new MailDto();
        mailDto.setMailTo(MailType.ADVERT_START.equals(type) ? haradanEmail : advert.getUser().getEmail());
        mailDto.setTemplate(type.getTemplate());
        mailDto.setSubject(type.getSubject(advert.getAdvertNo().toString()));
        Map<String, Object> model = getModel(advert);
        if (MailType.ADVERT_DOPING.equals(type)) {
            model.put("amount", amount);
        }
        mailDto.setProps(model);
        return mailDto;
    }

    public static MailDto prepareMail(MailType type, Contact contact) {
        MailDto mailDto = new MailDto();
        mailDto.setMailTo(haradanEmail);
        mailDto.setTemplate(type.getTemplate());
        mailDto.setSubject(type.getSubject());
        Map<String, Object> model = getModel();
        model.put("name", contact.getName());
        model.put("email", contact.getEmail());
        model.put("phone", contact.getPhoneNumber());
        model.put("message", contact.getMessage());
        mailDto.setProps(model);
        return mailDto;
    }

    private static Map<String, Object> getModel(Advert advert) {
        Map<String, Object> model = getModel(advert.getUser());
        model.put("advertId", advert.getIdentifier());
        model.put("advertNo", advert.getAdvertNo());
        model.put("advertTitle", advert.getTitle());
        model.put("advertUrl", advert.getUrl());
        return model;
    }

    private static Map<String, Object> getModel(User user) {
        Map<String, Object> model = getModel();
        model.put("id", user.getIdentifier());
        model.put("email", user.getEmail());
        model.put("firstName", user.getFirstName());
        model.put("lastName", user.getLastName());
        model.put("resetPasswordKey", user.getResetPasswordKey());
        return model;
    }

    private static Map<String, Object> getModel() {
        Map<String, Object> model = new HashMap<>();
        model.put("baseUrl", baseUrl);
        return model;
    }
}