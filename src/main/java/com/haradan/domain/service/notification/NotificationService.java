package com.haradan.domain.service.notification;

import com.haradan.common.enumeration.EntityStatus;
import com.haradan.common.enumeration.NotificationType;
import com.haradan.domain.dao.entity.Advert;
import com.haradan.domain.dao.entity.Contact;
import com.haradan.domain.dao.entity.SmsData;
import com.haradan.domain.dao.entity.User;
import com.haradan.domain.dao.repository.SmsDataRepository;
import com.haradan.domain.dto.NotificationDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Service
@Slf4j
@AllArgsConstructor
public class NotificationService {

    EmailSenderService emailSenderService;
    SmsDataRepository smsDataRepository;

    public void notify(NotificationDto context) {
        NotificationType type = context.getType();
        if (context.getEntity() instanceof User) {
            User user = (User) context.getEntity();
            if (type.getMailType() != null) {
                emailSenderService.sendEmail(NotificationUtils.prepareMail(type.getMailType(), user));
            }
            if (type.getSmsType() != null) {
                smsDataRepository.save(SmsData.builder()
                        .entity(user.getClass().getSimpleName())
                        .entityId(user.getIdentifier())
                        .status(EntityStatus.DEFAULT)
                        .phoneNumber(user.getPhoneNumber())
                        .type(type.getSmsType())
                        .build());
            }
        } else if (context.getEntity() instanceof Contact) {
            Contact contact = (Contact) context.getEntity();
            if (type.getMailType() != null) {
                emailSenderService.sendEmail(NotificationUtils.prepareMail(type.getMailType(), contact));
            }
        } else {
            Advert advert = (Advert) context.getEntity();
            type = context.getType() != null ? context.getType() :
                    NotificationType.of(context.getOldStatus(), advert.getStatus(), Advert.class);
            if (type != null) {
                if (type.getMailType() != null) {
                    emailSenderService.sendEmail(NotificationUtils.prepareMail(type.getMailType(), advert, context.getAmount()));
                }
                if (type.getSmsType() != null) {
                    smsDataRepository.save(SmsData.builder()
                            .entity(advert.getClass().getSimpleName())
                            .entityId(advert.getIdentifier())
                            .status(EntityStatus.DEFAULT)
                            .phoneNumber(advert.getUser().getPhoneNumber())
                            .type(type.getSmsType())
                            .build());
                }
            }
        }
    }
}