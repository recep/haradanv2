package com.haradan.domain.service.notification;

import com.haradan.common.enumeration.EntityStatus;
import com.haradan.domain.dao.entity.Advert;
import com.haradan.domain.dao.entity.SmsData;
import com.haradan.domain.dao.entity.User;
import com.haradan.domain.dao.repository.SmsDataRepository;
import com.haradan.domain.dto.SmsDto;
import com.haradan.domain.service.AdvertService;
import com.haradan.domain.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Service
@Slf4j
@AllArgsConstructor
public class SmsSenderService {

    SmsDataRepository smsDataRepository;
    UserService userService;
    AdvertService advertService;

    public void sendSms() {
        smsDataRepository.findByStatus(EntityStatus.DEFAULT).forEach(s -> sendSms(s));
    }

    private void sendSms(SmsData smsData) {
        SmsDto dto;
        if (smsData.getEntity().equals(User.class.getSimpleName())) {
            dto = NotificationUtils.prepareSms(smsData.getType(), smsData.getPhoneNumber());
        } else if (smsData.getEntity().equals(Advert.class.getSimpleName())) {
            dto = NotificationUtils.prepareSms(smsData.getType(), advertService.get(smsData.getEntityId()));
        }

        // call sms client
    }
}