package com.haradan.domain.service.notification;

import com.haradan.domain.dto.MailDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import java.util.Locale;


@Service
@Slf4j
@AllArgsConstructor
public class EmailSenderService {
    private JavaMailSender emailSender;
    private SpringTemplateEngine templateEngine;

    @Async
    public void sendEmail(MailDto mail) {
        try {
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
            helper.setFrom(mail.getFrom());
            helper.setTo(mail.getMailTo());
            helper.setBcc(mail.getFrom());
            helper.setSubject(mail.getSubject());
            String text = templateEngine.process(mail.getTemplate(), new Context(Locale.getDefault(), mail.getProps()));
            helper.setText(text, true);
            emailSender.send(message);
        } catch (Exception e) {
            log.error("An error occured on sending Email ", e);
        }
    }
}