package com.haradan.domain.service.file.upload;

import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

public interface IFileUploadService {

    void upload(String id, MultipartFile file, String fileName);

    void save(String id, String files);

    void changeDefaultImage(String id, String fileName);

    void deleteImage(String id, String fileName);

    Map<String, String> getFileMap(String id);
}
