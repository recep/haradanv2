package com.haradan.domain.service.file.resize.impl;

import com.haradan.common.enumeration.PhotoType;
import com.haradan.domain.service.file.resize.IPhotoResize;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;

@Slf4j
@Service
public class ImageMagickTool implements IPhotoResize {

    public static String root;
    public static boolean mergeWhite;
    public static String whiteImagePath;
    public static String imageMagickWindowsPath;

    @Override
    public void compress(String path) {
        // dont anything
    }

    @Override
    @Async
    public void resize(String path, String fileName) {
        Arrays.stream(PhotoType.values())
                .forEach(p -> resize(Paths.get(path).resolve(fileName).toString(),
                        Paths.get(path).resolve(p.of(fileName, p)).toString(), p));
    }

    private boolean resize(String inputImagePath, String outputImagePath, PhotoType photoType) {
        String maxWidthStr = "" + photoType.getWidth();
        String maxHeightStr = "x" + photoType.getHeight();
        String[] command = {getImageMagicPath(), "-resize", ">" + maxWidthStr + maxHeightStr, inputImagePath, outputImagePath};
        if (exec(command)) {
            return mergeWhite ? mergeWithWhite(outputImagePath, photoType) : true;
        }
        return false;
    }

    private boolean mergeWithWhite(String destinationFilePath, PhotoType photoType) {
        try {
            BufferedImage bimg = ImageIO.read(new File(destinationFilePath));
            File resizedFile = new File(destinationFilePath);
            if (resizedFile.exists()) {
                bimg = ImageIO.read(resizedFile);
                int resizedWidth = bimg.getWidth();
                int resizedHeight = bimg.getHeight();
                if (resizedWidth < photoType.getWidth() || resizedHeight < photoType.getHeight()) {
                    return mergeWithWhite(destinationFilePath, getWhiteImagePath(photoType), "center");
                } else {
                    return true;
                }
            }
        } catch (IOException e) {
            log.error("Error occured on merging white.", e);
        }
        return false;
    }

    private boolean mergeWithWhite(String destinationFilePath, String whiteFilePath, String location) {
        String[] command = new String[]{getImageMagicCompositePath(), "-dissolve", "100%", "-gravity", location, destinationFilePath, whiteFilePath, destinationFilePath};
        if (!exec(command)) {
            return false;
        }
        return true;
    }

    private String getWhiteImagePath(PhotoType photoType) throws IOException {
        return ResourceUtils.getFile(whiteImagePath  + photoType.getWhiteImage()).getPath();
    }

    private String getImageMagicPath() {
        String imageMagickPath = "convert";
        if (System.getProperty("os.name").indexOf("Windows") > -1) {
            imageMagickPath = imageMagickWindowsPath + "convert.exe";
        }
        return imageMagickPath;
    }

    private String getImageMagicCompositePath() {
        String imageMagickPath = "composite";
        if (System.getProperty("os.name").indexOf("Windows") > -1) {
            imageMagickPath = imageMagickWindowsPath + "composite.exe";
        }
        return imageMagickPath;
    }

    private static boolean exec(String[] command) {
        return exec(command, 60000);
    }

    private static boolean exec(String[] command, long timeoutInMillis) {
        try {
            Runtime runtime = Runtime.getRuntime();
            Process p;
            p = runtime.exec(command);
            long finish = System.currentTimeMillis() + timeoutInMillis;
            while (isAlive(p)) {
                Thread.sleep(10);
                if (System.currentTimeMillis() > finish) {
                    p.destroy();
                    return false;
                }
            }
            return p.exitValue() == 0;
        } catch (Exception e) {
            return false;
        }
    }

    private static boolean isAlive(Process p) {
        try {
            p.exitValue();
            return false;
        } catch (IllegalThreadStateException e) {
            return true;
        }
    }
}
