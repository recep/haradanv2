package com.haradan.domain.service.file.resize.impl;

import com.haradan.common.enumeration.PhotoType;
import com.haradan.domain.service.file.resize.IPhotoResize;
import com.tinify.Options;
import com.tinify.Source;
import com.tinify.Tinify;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;

@Slf4j
@Service
public class TinyPngResizeTool implements IPhotoResize {

    @Override
    public void compress(String filePath) throws IOException {
        Tinify.fromFile(filePath).toFile(filePath);
    }

    @Async
    @Override
    public void resize(String path, String fileName) {
        try {
            final String filePath = Paths.get(path).resolve(fileName).toString();
            final Source source = Tinify.fromFile(filePath);

            Arrays.stream(PhotoType.values())
                    .forEach(p -> {
                        String resizedPath = Paths.get(path).resolve(p.of(fileName, p)).toString();
                        createResized(source, resizedPath, p.getWidth(), p.getHeight());
                    });
        } catch (Exception e) {
            log.error("resize edilirken bir hata oluştu. Hata: ", e);
        }
    }

    private void createResized(Source source, String resizedPath, Integer width, Integer height) {
        try {
            Options options = new Options()
                    .with("method", "fit")
                    .with("width", width)
                    .with("height", height);
            Source resized = source.resize(options);
            resized.toFile(resizedPath);
        } catch (Exception e) {
            log.error("Yeni dosya yazılırken bir hata oluştu. Hata: ", e);
        }
    }

}
