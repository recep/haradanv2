package com.haradan.domain.service.file.upload;

import org.springframework.stereotype.Component;

import com.haradan.domain.service.file.upload.impl.AdvertFileService;
import com.haradan.domain.service.file.upload.impl.ArticleFileService;
import com.haradan.domain.service.file.upload.impl.BannerFileService;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class FileUploadFactory {

    private final AdvertFileService advertFileService;
    private final ArticleFileService articleFileService;
    private final BannerFileService bannerFileService;

    public IFileUploadService of(String criteria) {
        if ("article".equals(criteria)) {
            return articleFileService;
        } else if ("banner".equals(criteria)) {
            return bannerFileService;
        }
        return advertFileService;
    }
}
