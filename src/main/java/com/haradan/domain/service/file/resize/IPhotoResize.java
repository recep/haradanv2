package com.haradan.domain.service.file.resize;

public interface IPhotoResize {
    void resize(String path, String media) throws Exception;

    void compress(String path) throws Exception;
}
