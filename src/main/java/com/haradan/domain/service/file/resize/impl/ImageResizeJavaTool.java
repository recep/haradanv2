package com.haradan.domain.service.file.resize.impl;

import com.haradan.common.enumeration.PhotoType;
import com.haradan.common.util.Constants;
import com.haradan.domain.service.file.resize.IPhotoResize;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;

@Slf4j
@Service
public class ImageResizeJavaTool implements IPhotoResize {

    @Override
    public void compress(String path) {
        // dont anything
    }

    @Async
    @Override
    public void resize(String path, String fileName) {
        try {
            String format = fileName.substring(fileName.lastIndexOf(Constants.dat) + 1);
            BufferedImage originalImage = ImageIO.read(Paths.get(path).resolve(fileName).toFile());
            int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
            Arrays.stream(PhotoType.values())
                    .forEach(p -> {
                        Dimension dimension = getScaledDimension(new Dimension(originalImage.getWidth(), originalImage.getHeight()), new Dimension(p.getWidth(), p.getHeight()));
                        BufferedImage resizeImageHintJpg = resizeImageWithHint(originalImage, type, dimension);
                        createResized(resizeImageHintJpg, format, Paths.get(path).resolve(p.of(fileName, p)).toFile());
                    });
        } catch (Exception e) {
            log.error("resize edilirken bir hata oluştu. Hata: ", e);
        }
    }

    private void createResized(BufferedImage resizeImageHintJpg, String format, File file) {
        try {
            ImageIO.write(resizeImageHintJpg, format, file);
        } catch (IOException e) {
            log.error("resize edilirken bir hata oluştu. Hata: ", e);
        }
    }

    private static BufferedImage resizeImageWithHint(BufferedImage originalImage, int type, Dimension dimension) {
        BufferedImage resizedImage = new BufferedImage(dimension.width, dimension.height, type);
        Graphics2D graphic = resizedImage.createGraphics();
        graphic.drawImage(originalImage, 0, 0, dimension.width, dimension.height, null);
        graphic.dispose();
        graphic.setComposite(AlphaComposite.Src);
        graphic.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphic.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        graphic.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        return resizedImage;
    }

    public static Dimension getScaledDimension(Dimension originalSize, Dimension boundary) {
        int original_width = originalSize.width;
        int original_height = originalSize.height;
        int bound_width = boundary.width;
        int bound_height = boundary.height;
        int new_width = original_width;
        int new_height = original_height;
        // first check if we need to scale width
        if (original_width > bound_width) {
            // scale width to fit
            new_width = bound_width;
            // scale height to maintain aspect ratio
            new_height = (new_width * original_height) / original_width;
        }
        // then check if we need to scale even with the new height
        if (new_height > bound_height) {
            // scale height to fit instead
            new_height = bound_height;
            // scale width to maintain aspect ratio
            new_width = (new_height * original_width) / original_height;
        }
        return new Dimension(new_width, new_height);
    }
}
