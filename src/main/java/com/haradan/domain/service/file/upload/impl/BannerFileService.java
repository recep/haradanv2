package com.haradan.domain.service.file.upload.impl;

import com.haradan.common.util.Constants;
import com.haradan.domain.dao.entity.Banner;
import com.haradan.domain.service.BannerService;
import com.haradan.domain.service.file.upload.FileOperationService;
import com.haradan.domain.service.file.upload.IFileUploadService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@Slf4j
@Service
@RequiredArgsConstructor
public class BannerFileService implements IFileUploadService {

    private final BannerService bannerService;
    private final FileOperationService fileOperationService;

    @Override
    public void upload(String id, MultipartFile file, String fileName) {
        Banner banner = bannerService.get(id);
        fileOperationService.upload(banner.getMediaPath(), file, fileName);
        fileOperationService.compress(banner.getMediaPath(), fileName);
    }

    @Override
    public void save(String id, String fileName) {
        Banner banner = bannerService.get(id);
        banner.setMedia(banner.getMediaPath() + Constants.slash + fileName);
        bannerService.put(id, banner);
    }

    @Override
    public void changeDefaultImage(String id, String fileName) {
        //NA
    }

    @Override
    public void deleteImage(String id, String fileName) {
        Banner banner = bannerService.get(id);
        fileOperationService.deleteFile(banner.getMediaPath(), fileName);
        banner.setMedia(null);
        bannerService.put(id, banner);
    }

    @Override
    public Map<String, String> getFileMap(String id) {
        return null;
    }
}