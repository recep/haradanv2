package com.haradan.domain.service.file.upload.impl;

import com.haradan.common.enumeration.PhotoType;
import com.haradan.common.util.Constants;
import com.haradan.domain.dao.entity.Advert;
import com.haradan.domain.dao.entity.Media;
import com.haradan.domain.dao.repository.AdvertRepository;
import com.haradan.domain.dao.repository.MediaRepository;
import com.haradan.domain.service.AdvertService;
import com.haradan.domain.service.file.upload.FileOperationService;
import com.haradan.domain.service.file.upload.IFileUploadService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityNotFoundException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.haradan.common.util.Utils.changeOrder;

@Slf4j
@Service
@RequiredArgsConstructor
public class AdvertFileService implements IFileUploadService {

    private final AdvertService advertService;
    private final MediaRepository mediaRepository;
    private final AdvertRepository advertRepository;
    private final FileOperationService fileOperationService;

    @Override
    public void upload(String id, MultipartFile file, String fileName) {
        Advert advert = advertService.get(id);
        fileOperationService.upload(advert.getMediaPath(), file, fileName);
        fileOperationService.compress(advert.getMediaPath(), fileName);
        fileOperationService.resizeAndCompress(advert.getMediaPath(), fileName);
    }

    @Override
    @Transactional
    public void save(String advertId, String images) {
        Advert advert = advertService.get(advertId);

        if (advert.getMedia() != null && !StringUtils.isEmpty(advert.getMedia().getMedia())) {
            advert.getMedia().setMedia(advert.getMedia().getMedia() + Constants.comma + images);
        } else {
            String mainFileName = PhotoType.getMainPageVersion(images.split(Constants.comma)[0]);
            Media media = advert.getMedia();
            media.setImageUrl(getImageUrl(advert, mainFileName));
            media.setMedia(images);
            advert.setMedia(media);
        }
        advertRepository.save(advert);
    }

    @Override
    @Transactional
    public void changeDefaultImage(String advertId, String fileName) {
        Advert advert = advertService.get(advertId);
        String orderedImages = changeOrder(advert.getMedia().getMedia(), fileName);
        String mainFileName = PhotoType.getMainPageVersion(fileName);
        advert.getMedia().setMedia(orderedImages);
        advert.getMedia().setImageUrl(getImageUrl(advert, mainFileName));
        advertRepository.save(advert);
    }

    @Override
    @Transactional
    public void deleteImage(String advertId, String fileName) {
        Advert advert = advertService.get(advertId);
        fileOperationService.deleteFile(advert.getMediaPath(), fileName);
        String images = advert.getMedia()
                .getMedia()
                .replace(fileName.concat(Constants.comma), Constants.empty)
                .replace(Constants.comma.concat(fileName), Constants.empty)
                .replace(fileName, Constants.empty);
        if (StringUtils.isEmpty(images)) {
            advert.getMedia().setMedia(null);
            advert.getMedia().setImageUrl(null);
        } else {
            String mainFileName = PhotoType.getMainPageVersion(images.split(Constants.comma)[0]);
            advert.getMedia().setMedia(images);
            advert.getMedia().setImageUrl(getImageUrl(advert, mainFileName));
        }
        advertRepository.save(advert);
    }

    @Override
    @Transactional
    public Map<String, String> getFileMap(String advertId) {
        Advert advert = advertService.get(advertId);
        List<String> list = Arrays.stream(
                Optional.ofNullable(advert.getMedia())
                        .filter(m -> !StringUtils.isEmpty(m.getMedia()))
                        .map(m -> m.getMedia().split(Constants.comma))
                        .orElseGet(() -> new String[0]))
                .collect(Collectors.toList());
        Map<String, String> map = new LinkedHashMap<>();
        list.stream().forEach(f -> map.put(f, getImageUrl(advert, PhotoType.getMainPageVersion(f))));
        return map;
    }

    private String getImageUrl(Advert advert, String fileName) {
        return Constants.slash + advert.getCategory().getSearchText() + advert.getMediaPath() + Constants.slash + fileName;
    }

    public Map<String, List<String>> createAdvertMediaMap(Advert advert) {
        Map<String, List<String>> imageMap = new HashMap<>();
        if (advert.getMedia() != null && !StringUtils.isEmpty(advert.getMedia().getMedia())) {
            imageMap.put("images",
                    Arrays.stream(advert.getMedia().getMedia().split(Constants.comma))
                            .map(f -> getImageUrl(advert, f))
                            .collect(Collectors.toList()));
        }
        return imageMap;
    }
}