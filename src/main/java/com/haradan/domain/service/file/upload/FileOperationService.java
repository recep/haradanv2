package com.haradan.domain.service.file.upload;

import com.haradan.common.enumeration.PhotoType;
import com.haradan.common.exception.exceptions.MicroException;
import com.haradan.common.util.Constants;
import com.haradan.domain.service.file.resize.IPhotoResize;
import com.tinify.Tinify;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import static com.haradan.common.util.Utils.createPath;

@Slf4j
@Service
@RequiredArgsConstructor
public class FileOperationService {

    public static IPhotoResize photoResize;

    @Value("${system.parameter.file.root}")
    String fileRoot;

    @Value("${system.parameter.file.compress.key:RmX3SR6CqVZSpk9jD9JSWDjgpw29sFFQ}")
    String compressKey;

    @PostConstruct
    private void init() {
        Tinify.setKey(compressKey);
    }

    private Path getRoot(String path) {
        return Paths.get(fileRoot + path);
    }

    public void upload(Path inFile, String path, String fileName) {
        try {
            Path root = getRoot(path);
            createPath(root);
            Files.copy(inFile, root.resolve(fileName));
        } catch (Exception e) {
            throw new MicroException(101L, "Could not store file. Error: " + e.toString());
        }
    }

    public void upload(String path, MultipartFile file, String fileName) {
        try {
            Path root = getRoot(path);
            createPath(root);
            Files.copy(file.getInputStream(), root.resolve(fileName));
        } catch (Exception e) {
            throw new MicroException(101L, "Could not store file. Error: " + e);
        }
    }

    public void compress(String path, String fileName) {
        try {
            String filePath = getRoot(path).resolve(fileName).toString();
            photoResize.compress(filePath);
        } catch (Exception e) {
            log.error("Could not copress file. Error: " + e);
        }
    }

    @Async
    public void resizeAndCompress(String path, String fileName) {
        try {
            photoResize.resize(getRoot(path).toString(), fileName);
            Arrays.stream(PhotoType.values()).forEach(p -> compress(path, p.of(fileName, p)));
        } catch (Exception e) {
            log.error("Could not resize file. path: {}, file: {}, Error: {} ", path, fileName, e.toString());
        }
    }

    @Async
    public void deleteFile(String path, String fileName) {
        try {
            Path root = getRoot(path);
            Files.walk(root, 1)
                    .filter(p -> !p.equals(root))
                    .filter(p -> p.getFileName().toString().contains(fileName.substring(0, fileName.lastIndexOf(Constants.dat))))
                    .forEach(p -> {
                        try {
                            Files.deleteIfExists(p);
                        } catch (IOException e) {
                            log.error("An exception occured on deleting file and ignored. ", e);
                        }
                    });
        } catch (IOException e) {
            log.error("An exception occured when walking on files and ignored. ", e);
        }
    }

    @Async
    public void deleteAllFiles(String path) {
        FileSystemUtils.deleteRecursively(getRoot(path).toFile());
    }
}