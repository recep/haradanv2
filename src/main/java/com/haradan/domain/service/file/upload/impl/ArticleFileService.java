package com.haradan.domain.service.file.upload.impl;

import com.haradan.common.util.Constants;
import com.haradan.domain.dao.entity.Article;
import com.haradan.domain.service.ArticleService;
import com.haradan.domain.service.file.upload.FileOperationService;
import com.haradan.domain.service.file.upload.IFileUploadService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@Slf4j
@Service
@RequiredArgsConstructor
public class ArticleFileService implements IFileUploadService {

    private final ArticleService articleService;
    private final FileOperationService fileOperationService;

    @Override
    public void upload(String id, MultipartFile file, String fileName) {
        Article article = articleService.get(id);
        fileOperationService.upload(article.getMediaPath(), file, fileName);
        fileOperationService.compress(article.getMediaPath(), fileName);
    }

    @Override
    public void save(String id, String fileName) {
        Article article = articleService.get(id);
        article.setMedia(article.getMediaPath() + Constants.slash + fileName);
        articleService.put(id, article);
    }

    @Override
    public void changeDefaultImage(String id, String fileName) {
        //NA
    }

    @Override
    public void deleteImage(String id, String fileName) {
        Article article = articleService.get(id);
        fileOperationService.deleteFile(article.getMediaPath(), fileName);
        article.setMedia(null);
        articleService.put(id, article);
    }

    @Override
    public Map<String, String> getFileMap(String id) {
        return null;
    }
}