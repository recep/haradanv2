package com.haradan.domain.service;

import com.haradan.common.enumeration.EntityStatus;
import com.haradan.common.exception.exceptions.MicroException;
import com.haradan.domain.dao.entity.Stable;
import com.haradan.domain.dao.entity.User;
import com.haradan.domain.dao.repository.StableRepository;
import com.haradan.domain.dao.repository.UserRepository;
import com.haradan.domain.dto.StableSearchDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class StableService {
    private final StableRepository stableRepository;

    public Stable get(String id) {
        return stableRepository.findById(id).orElseThrow(() -> MicroException.newDataNotFoundException(Stable.class.getSimpleName(), id));
    }

    @Transactional
    public void delete(String id) {
        Stable theReal = get(id);
        stableRepository.delete(theReal);
    }

    @Transactional
    public Stable save(Stable stable){
        return stableRepository.save(stable);
    }

    @Transactional
    public Stable put(String id, Stable stable) {
        Stable theReal = get(id);
        theReal.setName(stable.getName());
        theReal.setContactName(stable.getContactName());
        theReal.setPhoneNumber(stable.getPhoneNumber());
        theReal.setEmail(stable.getEmail());
        theReal.setWebpage(stable.getWebpage());
        theReal.setAddress(stable.getAddress());
        theReal.setNote(stable.getNote());
        return stableRepository.save(theReal);
    }

    public Page<Stable> search(StableSearchDto searchDto, Pageable pageable) {
        return stableRepository.findAll(StableRepository.QueryGeneration.search(searchDto), pageable);
    }

}
