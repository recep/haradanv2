package com.haradan.domain.service;

import com.haradan.common.exception.exceptions.MicroException;
import com.haradan.domain.dao.entity.City;
import com.haradan.domain.dao.repository.CityRepository;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class CityService {
    private final CityRepository cityRepository;

    @Cacheable(value = "city", key = "#id")
    public City get(String id) {
        return cityRepository.findById(id).orElseThrow(() -> MicroException.newDataNotFoundException(City.class.getSimpleName(), id));
    }

    @Transactional
    @CacheEvict(value = "city", allEntries = true)
    public City put(String id, City city) {
        City theReal = cityRepository.findById(id).orElseThrow(() -> MicroException.newDataNotFoundException(City.class.getSimpleName(), id));
        city.setIdentifier(theReal.getIdentifier());
        return cityRepository.save(city);
    }

    @Transactional
    @CacheEvict(value = "city", allEntries = true)
    public void delete(String id) {
        City theReal = cityRepository.findById(id).orElseThrow(() -> MicroException.newDataNotFoundException(City.class.getSimpleName(), id));
        cityRepository.delete(theReal);
    }

    @Transactional
    @CacheEvict(value = "city", allEntries = true)
    public City save(City city) {
        return cityRepository.save(city);
    }

    @Cacheable(value = "city")
    public Page<City> search(String countryCode, String name, String code, Pageable pageable) {
        return cityRepository.findAll(CityRepository.QueryGeneration.search(countryCode, name, code), pageable);
    }
}
