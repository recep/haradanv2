package com.haradan.domain.service;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haradan.common.enumeration.ArticleType;
import com.haradan.common.exception.exceptions.MicroException;
import com.haradan.common.util.Utils;
import com.haradan.domain.dao.entity.Article;
import com.haradan.domain.dao.repository.ArticleRepository;
import com.haradan.domain.dto.ArticleSearchDto;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class ArticleService {
    private final ArticleRepository articleRepository;

    public Article get(String id) {
        return articleRepository.findById(id).orElseThrow(() -> MicroException.newDataNotFoundException(Article.class.getSimpleName(), id));
    }

    @Transactional
    public Article put(String id, Article article) {
        Article theReal = articleRepository.findById(id).orElseThrow(() -> MicroException.newDataNotFoundException(Article.class.getSimpleName(), id));
        article.setIdentifier(theReal.getIdentifier());
        article.setSearchText(Utils.normalizeCharacters(article.getTitle()));
        return articleRepository.save(article);
    }

    @Transactional
    public void delete(String id) {
        Article theReal = articleRepository.findById(id).orElseThrow(() -> MicroException.newDataNotFoundException(Article.class.getSimpleName(), id));
        articleRepository.delete(theReal);
    }

    @Transactional
    public Article save(Article article) {
        article.setSearchText(Utils.normalizeCharacters(article.getTitle()));
        return articleRepository.save(article);
    }

    public Page<Article> search(ArticleType type, Pageable pageable) {
        return articleRepository.findAll(ArticleRepository.QueryGeneration.search(type), pageable);
    }
    
    public Page<Article> search(ArticleSearchDto searchDto, Pageable pageable) {
        return articleRepository.findAll(ArticleRepository.QueryGeneration.search(searchDto), pageable);
    }
}
