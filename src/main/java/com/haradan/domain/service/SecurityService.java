package com.haradan.domain.service;

import com.haradan.common.enumeration.AuthorityType;
import com.haradan.common.exception.Exceptions;
import com.haradan.common.exception.exceptions.MicroException;
import com.haradan.domain.dao.entity.User;
import com.haradan.domain.dao.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Component
@AllArgsConstructor
public class SecurityService {
    private UserRepository userRepository;
    private TokenStore tokenStore;

    public Map<String, Object> getCurrentUserAsMap() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getDetails() instanceof OAuth2AuthenticationDetails) {
            OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) authentication.getDetails();
            OAuth2AccessToken accessToken = tokenStore.readAccessToken(details.getTokenValue());
            return accessToken.getAdditionalInformation();
        } else {
            return null;
        }

    }

    /**
     * Get the login of the current user.
     *
     * @return the login of the current user
     */
    public String getCurrentUserLogin() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        String userName = null;
        if (authentication != null) {
            if (authentication.getPrincipal() instanceof UserDetails) {
                UserDetails springSecurityUser = (UserDetails) authentication.getPrincipal();
                userName = springSecurityUser.getUsername();
            } else if (authentication.getPrincipal() instanceof String) {
                userName = (String) authentication.getPrincipal();
            }
        }
        return userName;
    }

    public User getLoginUser() {
        return userRepository.findByEmail(getCurrentUserLogin()).orElseThrow(() -> new MicroException(Exceptions.USER_NOT_FOUND));
    }

    /**
     * Check if a user is authenticated.
     *
     * @return true if the user is authenticated, false otherwise
     */
    public boolean isAuthenticated() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        if (authentication != null) {
            Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
            if (authorities != null) {
                for (GrantedAuthority authority : authorities) {
                    if (authority.getAuthority().equals("ROLE_ANONYMOUS")) {
                        return false;
                    }
                }
            }
            return true;
        }
        return false;
    }

    public boolean hasAnyAuthorities(AuthorityType... authorityTypes) {
        final Set<String> givenAuthorities = Arrays.stream(authorityTypes).map(AuthorityType::getName).collect(Collectors.toSet());
        return Optional.ofNullable(getCurrentUserAuthorities())
                .map(currentUserAuthorities -> givenAuthorities.stream().anyMatch(currentUserAuthorities::contains))
                .orElse(false);
    }

    public boolean isAdmin() {
        return hasAuthorities(AuthorityType.ROLE_ADMIN);
    }

    public boolean hasAuthorities(AuthorityType... authorityTypes) {
        final Set<String> givenAuthorities = Arrays.stream(authorityTypes).map(AuthorityType::getName).collect(Collectors.toSet());
        return Optional.ofNullable(getCurrentUserAuthorities())
                .map(currentUserAuthorities -> currentUserAuthorities.containsAll(givenAuthorities))
                .orElse(false);
    }

    public Set<String> getCurrentUserAuthorities() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        if (authentication != null) {
            if (authentication instanceof OAuth2Authentication) {
                return authentication.getAuthorities().stream()
                        .map(GrantedAuthority::getAuthority)
                        .collect(Collectors.toSet());
            } else if (authentication.getPrincipal() instanceof UserDetails) {
                UserDetails springSecurityUser = (UserDetails) authentication.getPrincipal();
                return springSecurityUser.getAuthorities()
                        .stream()
                        .map(GrantedAuthority::getAuthority)
                        .collect(Collectors.toSet());
            }
        }
        return Collections.EMPTY_SET;
    }
}