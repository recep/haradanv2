package com.haradan.domain.service;

import com.haradan.common.controller.response.LookupDto;
import com.haradan.common.enumeration.SpecialPathType;
import com.haradan.common.exception.exceptions.MicroException;
import com.haradan.common.util.Utils;
import com.haradan.domain.dao.entity.Category;
import com.haradan.domain.dao.repository.CategoryRepository;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CategoryService {

    private final CategoryRepository categoryRepository;

    @Cacheable(value = "parent-category")
    public List<Category> getParentCategories() {
        return categoryRepository.findByParent(null).orElseThrow(() -> MicroException.newDataNotFoundException(Category.class.getSimpleName(), "parent"));
    }

    @Cacheable(value = "category")
    public Category get(String id) {
        return categoryRepository.findById(id).orElseThrow(() -> MicroException.newDataNotFoundException(Category.class.getSimpleName(), id));
    }

    public Set<String> getCategoryIds(String searchText) {
        Category category = getBySearchText(searchText);
        Set<String> categoryIds = new HashSet<>();
        categoryIds.add(category.getIdentifier());
        category.getChildren().stream().forEach(c -> categoryIds.add(c.getIdentifier()));
        return categoryIds;
    }

    @Cacheable(value = "category")
    public Category getBySearchText(String searchText) {
        return categoryRepository.findBySearchText(searchText).orElseThrow(() -> MicroException.newDataNotFoundException(Category.class.getSimpleName(), searchText));
    }

    @Transactional
    @CacheEvict(value = "category", allEntries = true)
    public Category put(String id, Category category) {
        Category theReal = categoryRepository.findById(id).orElseThrow(() -> MicroException.newDataNotFoundException(Category.class.getSimpleName(), id));
        category.setIdentifier(theReal.getIdentifier());
        category.setSearchText(Utils.normalizeCharacters(category.getName()));
        category.setParent(Utils.checkEntity(category.getParent()));
        return categoryRepository.save(category);
    }

    @Transactional
    @CacheEvict(value = "category", allEntries = true)
    public void delete(String id) {
        Category theReal = categoryRepository.findById(id).orElseThrow(() -> MicroException.newDataNotFoundException(Category.class.getSimpleName(), id));
        categoryRepository.delete(theReal);
    }

    @Transactional
    @CacheEvict(value = "category", allEntries = true)
    public Category save(Category category) {
        category.setSearchText(Utils.normalizeCharacters(category.getName()));
        category.setParent(Utils.checkEntity(category.getParent()));
        return categoryRepository.save(category);
    }

    @Cacheable(value = "lookup-category")
    public List<LookupDto> getMainpageCategories() {
        List<Category> categories = categoryRepository.findByParent(null).orElseThrow(() -> MicroException.newDataNotFoundException(Category.class.getSimpleName(), "parent"));
        List<LookupDto> lookupDtos = categories.get(0)
                .getChildren()
                .stream()
                .map(c -> new LookupDto(c.getSearchText(), c.getName().substring(c.getName().indexOf(" ") + 1)))
                .collect(Collectors.toList());

        categories.get(1).getChildren().forEach(c -> {
            String name;
            if ("pansiyon-haralar".equals(c.getSearchText())) {
                name = "Pansiyon";
            } else if ("at-nakliyesi".equals(c.getSearchText())) {
                name = "Nakliye";
            } else {
                name = "Nalbant";
            }
            lookupDtos.add(new LookupDto(c.getSearchText(), name));
        });

        lookupDtos.addAll(SpecialPathType.mainpageCategories());
        return lookupDtos;
    }


}
