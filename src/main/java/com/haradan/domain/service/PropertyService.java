package com.haradan.domain.service;

import com.haradan.common.enumeration.PropertyType;
import com.haradan.common.enumeration.property.AgeType;
import com.haradan.common.enumeration.property.DonuType;
import com.haradan.common.enumeration.property.GenderType;
import com.haradan.common.enumeration.property.RaceType;
import com.haradan.common.exception.exceptions.MicroException;
import com.haradan.common.util.Utils;
import com.haradan.domain.controller.response.PropertyResponse;
import com.haradan.domain.dao.entity.Category;
import com.haradan.domain.dao.entity.Property;
import com.haradan.domain.dao.repository.PropertyRepository;
import com.haradan.domain.mapper.PropertyMapper;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class PropertyService {
    private final PropertyRepository propertyRepository;
    private final CategoryService categoryService;
    private final PropertyMapper propertyMapper;

    @Cacheable(value = "property")
    public Property get(String id) {
        return propertyRepository.findById(id).orElseThrow(() -> MicroException.newDataNotFoundException(Property.class.getSimpleName(), id));
    }

    @Cacheable(value = "property")
    public List<Property> getByCategoryId(String categoryId) {
        Category category = categoryService.get(categoryId);
        return propertyRepository.findByCategoryOrderByOrderId(category).orElse(new ArrayList<>());
    }

    @Transactional
    @CacheEvict(value = "property", allEntries = true)
    public Property put(String id, Property property) {
        Property theReal = propertyRepository.findById(id).orElseThrow(() -> MicroException.newDataNotFoundException(Property.class.getSimpleName(), id));
        property.setIdentifier(theReal.getIdentifier());
        property.setSearchText(Utils.normalizeCharacters(property.getName()));
        property.setParent(Utils.checkEntity(property.getParent()));
        return propertyRepository.save(property);
    }

    @Transactional
    @CacheEvict(value = "property", allEntries = true)
    public void delete(String id) {
        Property theReal = propertyRepository.findById(id).orElseThrow(() -> MicroException.newDataNotFoundException(Property.class.getSimpleName(), id));
        propertyRepository.delete(theReal);
    }

    @Transactional
    @CacheEvict(value = "property", allEntries = true)
    public Property save(Property property) {
        property.setSearchText(Utils.normalizeCharacters(property.getName()));
        property.setParent(Utils.checkEntity(property.getParent()));
        return propertyRepository.save(property);
    }

    public List<PropertyResponse> getByCategoryIdWithLookup(String categoryId) {
        List<PropertyResponse> properties = propertyMapper.toResponse(getByCategoryId(categoryId));
        properties.stream()
                .filter(p -> PropertyType.select.equals(p.getType()))
                .forEach(p -> {
                    if ("yas".equals(p.getSearchText())) {
                        p.setLookupData(AgeType.getLookup());
                    } else if ("at-irki".equals(p.getSearchText())) {
                        p.setLookupData(RaceType.getLookup());
                    } else if (p.getSearchText().startsWith("donu")) {
                        p.setLookupData(DonuType.getLookup());
                    } else if ("cinsiyet".equals(p.getSearchText())) {
                        p.setLookupData(GenderType.getLookup());
                    }
                });
        return properties;
    }
}
