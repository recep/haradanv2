package com.haradan.domain.service;

import com.haradan.common.enumeration.DopingType;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.common.exception.Exceptions;
import com.haradan.common.exception.exceptions.MicroException;
import com.haradan.common.util.Constants;
import com.haradan.common.util.Utils;
import com.haradan.domain.controller.response.AdvertResponse;
import com.haradan.domain.dao.entity.Advert;
import com.haradan.domain.dao.entity.AdvertProperty;
import com.haradan.domain.dao.entity.Doping;
import com.haradan.domain.dao.entity.Favourite;
import com.haradan.domain.dao.entity.Media;
import com.haradan.domain.dao.entity.User;
import com.haradan.domain.dao.repository.AdvertPropertyRepository;
import com.haradan.domain.dao.repository.AdvertRepository;
import com.haradan.domain.dao.repository.DopingRepository;
import com.haradan.domain.dao.repository.FavouriteRepository;
import com.haradan.domain.dto.AdDopingSearchDto;
import com.haradan.domain.dto.NotificationDto;
import com.haradan.domain.dto.SearchDto;
import com.haradan.domain.dto.SearchResultDto;
import com.haradan.domain.mapper.AdvertMapper;
import com.haradan.domain.service.notification.NotificationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import static com.haradan.common.enumeration.EntityStatus.ACTIVE;
import static com.haradan.common.enumeration.EntityStatus.NOT_COMPLETED;
import static com.haradan.common.enumeration.EntityStatus.WAITING_APPROVAL;
import static com.haradan.common.enumeration.NotificationType.ADVERT_DOPING;
import static com.haradan.common.enumeration.NotificationType.ADVERT_DOPING_EXPIRE;
import static com.haradan.common.util.Constants.PREGNANT;
import static com.haradan.common.util.Constants.RACE;
import static com.haradan.common.util.Constants.SEX;
import static com.haradan.common.util.Utils.toShuffledList;

@Slf4j
@Service
@AllArgsConstructor
public class AdvertService {
    private final AdvertRepository advertRepository;
    private final AdvertPropertyRepository advertPropertyRepository;
    private final DopingRepository dopingRepository;
    private final SecurityService securityService;
    private final NotificationService notificationService;
    private final CategoryService categoryService;
    private final UserService userService;
    private final FavouriteRepository favouriteRepository;
    private final AdvertMapper advertMapper;

    static AtomicLong advertNo;

    @PostConstruct
    private void init() {
        advertNo = new AtomicLong(advertRepository.findMaxAdvertNo().orElseGet(() -> 1L));
    }

    public Advert get(String id) {
        return advertRepository.findById(id)
                .orElseThrow(() -> MicroException.newDataNotFoundException(Advert.class.getSimpleName(), id));
    }

    public Advert get(Long advertNo) {
        return advertRepository.findByAdvertNo(advertNo).orElseThrow(
                () -> MicroException.newDataNotFoundException(Advert.class.getSimpleName(), advertNo.toString()));
    }

    @Transactional
    @CacheEvict(value = "advert", allEntries = true)
    public Advert put(String id, Advert advert) {
        Advert theReal = advertRepository.findById(id)
                .orElseThrow(() -> MicroException.newDataNotFoundException(Advert.class.getSimpleName(), id));
        EntityStatus status = theReal.getStatus();
        theReal.setStatus(determineNextStatus(theReal, advert));
        theReal.setTitle(advert.getTitle());
        theReal.setDescription(advert.getDescription());
        theReal.setPrice(advert.getPrice());
        theReal.setCity(advert.getCity());
        theReal.setDistrict(advert.getDistrict());
        theReal.setSearchText(Utils.normalizeCharacters(theReal.getTitle()));
        if (advert.getCategory() != null) {
            theReal.setCategory(advert.getCategory());
        }
        if (ACTIVE.equals(theReal.getStatus())) {
            theReal.setCharged(true);
        }
        theReal.setTjkNo(advert.getTjkNo());
        Advert updatedAdvert = advertRepository.save(theReal);
        notificationService.notify(NotificationDto.builder().oldStatus(status).entity(updatedAdvert).build());
        return updatedAdvert;
    }

    private EntityStatus determineNextStatus(Advert theReal, Advert advert) {

        if (securityService.isAdmin()) {
            return ACTIVE;
        }

        if (StringUtils.equals(theReal.getTitle(), advert.getTitle())
                && StringUtils.equals(theReal.getDescription(), advert.getDescription())) {
            return theReal.getStatus();
        }

        return WAITING_APPROVAL;
    }

    @Transactional
    @CacheEvict(value = "advert", allEntries = true)
    public void delete(String id) {
        Advert theReal = advertRepository.findById(id)
                .orElseThrow(() -> MicroException.newDataNotFoundException(Advert.class.getSimpleName(), id));
        advertRepository.delete(theReal);
    }

    @Transactional
    @CacheEvict(value = "advert", allEntries = true)
    public Advert save(Advert advert) {
        advert.setUser(securityService.getLoginUser());
        advert.setSearchText(Utils.normalizeCharacters(advert.getTitle()));
        advert.setStatus(NOT_COMPLETED);
        advert.setAdvertNo(advertNo.incrementAndGet());
        advert.setMedia(Media.builder().build());
        Advert savedAdvert = advertRepository.save(advert);
        notificationService.notify(NotificationDto.builder().oldStatus(NOT_COMPLETED).entity(savedAdvert).build());
        return savedAdvert;
    }

    public Map<String, List<SearchResultDto>> searchAdvertForCategoryGroup(SearchDto searchDto) {
        Map<String, SearchResultDto> categoriesMap = new HashMap<>();
        categoryService.getParentCategories().stream()
                .forEach(c -> c.getChildren()
                        .forEach(child -> categoriesMap.put(child.getSearchText(),
                                SearchResultDto.builder()
                                        .parentName(c.getName())
                                        .categorySearchText(child.getSearchText())
                                        .categoryName(child.getName())
                                        .build())));

        search(searchDto, PageRequest.of(0, 1000)).get()
                .forEach(a -> {
                    String cathSearchText = a.getCategory().getSearchText();
                    categoriesMap.get(cathSearchText).setAdCount(categoriesMap.get(cathSearchText).getAdCount() + 1L);
                });

        Map<String, List<SearchResultDto>> searchResultList = new HashMap<>();
        categoriesMap.entrySet().stream().filter(e -> e.getValue().getAdCount() > 0).forEach(e -> {
            List<SearchResultDto> list = searchResultList.getOrDefault(e.getValue().getParentName(), new ArrayList<>());
            list.add(e.getValue());
            searchResultList.put(e.getValue().getParentName(), list);
        });
        return searchResultList;
    }

    @Cacheable(value = "advert")
    public Page<Advert> search(SearchDto searchDto, Pageable pageable) {
        if (StringUtils.isNoneEmpty(searchDto.getCategorySearchText())) {
            searchDto.setCategoryIds(categoryService.getCategoryIds(searchDto.getCategorySearchText()));
        }
        return advertRepository.findAll(AdvertRepository.QueryGeneration.search(searchDto), pageable);
    }

    @Cacheable(value = "advert")
    public Page<Advert> search(AdDopingSearchDto searchDto, Pageable pageable) {
        return advertRepository.findAll(AdvertRepository.QueryGeneration.search(searchDto), pageable);
    }

    @Cacheable(value = "advert")
    public Page<Advert> searchDoping(DopingType doping, Pageable pageable) {
        return advertRepository.findAll(AdvertRepository.QueryGeneration.searchDoping(doping), pageable);
    }

    public List<AdvertResponse> searchDoping(DopingType doping, int size, String... properties) {
        Pageable pageable = PageRequest.of(0, size, Sort.Direction.DESC, properties);
        return searchDoping(doping, pageable)
                .map(advertMapper::toResponse)
                .get()
                .collect(toShuffledList());
    }

    @Transactional
    public void saveProperty(String id, List<AdvertProperty> properties) {
        Advert advert = get(id);
        List<AdvertProperty> existProperties = getAdvertProperties(advert);
        properties.forEach(p -> {
            existProperties.forEach(e -> {
                if (e.getProperty().getIdentifier().equals(p.getProperty().getIdentifier())) {
                    p.setIdentifier(e.getIdentifier());
                }
            });
            p.setAdvert(advert);
        });
        advertPropertyRepository.saveAll(properties);
    }

    @Async
    @Transactional
    public void updateDopingStatusAsync(String id, List<Doping> dopings) {
        Advert advert = get(id);
        int amount = dopings
                .stream()
                .filter(Doping::isActive)
                .mapToInt(d -> d.getDoping().getPrice())
                .sum();

        dopings.stream()
                .filter(Doping::isActive)
                .forEach(d -> d.setApproveDate(LocalDate.now()));

        sendNotification(advert, amount);
        saveDoping(id, dopings);
        updateAdvert(advert, dopings);
    }

    private void updateAdvert(Advert advert, List<Doping> dopings) {
        boolean isUpToDate = dopings.stream()
                .anyMatch(d -> d.getDoping().equals(DopingType.UP_TO_DATE));

        if (isUpToDate) {
            advert.setCreatedDate(ZonedDateTime.now());
            advertRepository.save(advert);
        }
    }

    private void sendNotification(Advert advert, int amount) {
        if (amount > 0) {
            notificationService.notify(NotificationDto.builder().type(ADVERT_DOPING).entity(advert).amount(amount).build());
        } else {
            notificationService.notify(NotificationDto.builder().type(ADVERT_DOPING_EXPIRE).entity(advert).amount(amount).build());
        }
    }

    @Transactional
    public void saveDoping(String id, List<Doping> dopings) {
        Advert advert = get(id);
        List<Doping> existDopings = getDopings(id);
        dopings.forEach(p -> {
            existDopings.forEach(e -> {
                if (e.getDoping().equals(p.getDoping())) {
                    p.setIdentifier(e.getIdentifier());
                    p.setCreatedDate(e.getCreatedDate());
                }
            });
            p.setAdvert(advert);
        });
        dopingRepository.saveAll(dopings);
    }

    public List<AdvertProperty> getAdvertProperties(Advert advert) {
        return advertPropertyRepository.findByAdvert(advert).orElseGet(ArrayList::new).stream()
                .sorted(Comparator.comparingLong(p -> p.getProperty().getOrderId())).collect(Collectors.toList());
    }

    public List<Doping> getDopings() {
        return Arrays.stream(DopingType.values()).map(d -> Doping.builder().doping(d).build())
                .collect(Collectors.toList());
    }

    public List<Doping> getDopings(String id) {
        return dopingRepository.findByAdvert(get(id)).orElseGet(ArrayList::new);
    }

    public List<Doping> getDopingsToActivate(String id) {
        return getDopings(id)
                .stream()
                .filter(d -> EntityStatus.WAITING_APPROVAL.equals(d.getStatus()))
                .map(d -> {
                    d.setStatus(ACTIVE);
                    return d;
                })
                .collect(Collectors.toList());
    }

    @Transactional
    @CacheEvict(value = "advert", allEntries = true)
    public void updateStatus(String id, EntityStatus status) {
        Advert theReal = advertRepository.findById(id)
                .orElseThrow(() -> MicroException.newDataNotFoundException(Advert.class.getSimpleName(), id));
        EntityStatus oldStatus = theReal.getStatus();
        theReal.setStatus(status);
        theReal.setCharged(ACTIVE.equals(status));
        Advert advert = advertRepository.save(theReal);
        if (securityService.isAdmin()) {
            notificationService.notify(NotificationDto.builder().oldStatus(oldStatus).entity(advert).build());
        }
    }

    @Transactional
    @CacheEvict(value = "advert", allEntries = true)
    public void updateCategory(String id, String categoryId) {
        Advert theReal = advertRepository.findById(id)
                .orElseThrow(() -> MicroException.newDataNotFoundException(Advert.class.getSimpleName(), id));
        theReal.setCategory(categoryService.get(categoryId));
        advertRepository.save(theReal);
    }

    @Transactional
    public void addToFavourites(String advertId, String userId) {
        Advert advert = get(advertId);
        User user = userService.get(userId);
        favouriteRepository.findByAdvertAndUser(advert, user).ifPresent(u -> {
            throw new MicroException(Exceptions.FAVOURITE_ALREADY_EXIST);
        });
        Favourite favourite = new Favourite();
        favourite.setAdvert(advert);
        favourite.setUser(user);
        favouriteRepository.save(favourite);
    }

    @Transactional
    public void removeFromFavourites(String advertId, String userId) {
        Advert advert = get(advertId);
        User user = userService.get(userId);
        Favourite favourite = favouriteRepository.findByAdvertAndUser(advert, user)
                .orElseThrow(() -> new MicroException(Exceptions.FAVOURITE_NOT_FOUND));
        favouriteRepository.deleteById(favourite.getIdentifier());
    }

    public int getFavouriteCount(String advertId) {
        Advert advert = get(advertId);
        List<Favourite> favourite = favouriteRepository.findByAdvert(advert).orElseGet(ArrayList::new);
        return favourite.size();
    }

    public Page<Advert> searchFavouriteAds(String userId, Pageable pageable) {
        User user = userService.get(userId);
        return advertRepository.findAll(AdvertRepository.QueryGeneration.searchFavouriteAds(user), pageable);
    }

    public int getAmount(Advert advert) {
        int dopingPrice = getDopingAmount(advert);
        if (dopingPrice == 0) {
            try {
                Thread.sleep(100);
                log.warn("Recalculate doping amount.");
            } catch (InterruptedException e) {
                log.warn("Thread does not sleep...");
            }
            dopingPrice = getDopingAmount(advert);
        }
        return advert.getChargePrice() + dopingPrice;
    }

    private int getDopingAmount(Advert advert) {
        return getDopings(advert.getIdentifier())
                .stream()
                .filter(d -> WAITING_APPROVAL.equals(d.getStatus()))
                .mapToInt(d -> d.getDoping().getPrice()).sum();
    }

    public List<Advert> getSimilarAdverts(Advert advert) {
        Pageable pageable = PageRequest.of(0, 10, Sort.Direction.DESC, Constants.CREATED_DATE);

        Map<String, String> params = getAdvertProperties(advert).stream()
                .filter(p -> p.getSearchText().equalsIgnoreCase(RACE)
                        || p.getSearchText().equalsIgnoreCase(PREGNANT)
                        || p.getSearchText().equalsIgnoreCase(SEX))
                .collect(Collectors.toMap(AdvertProperty::getSearchText, AdvertProperty::getValue));

        SearchDto searchDto = SearchDto.builder()
                .status(ACTIVE)
                .categorySearchText(advert.getCategory().getSearchText())
                .properties(params)
                .build();

        return search(searchDto, pageable).get()
                .filter(a -> !a.getIdentifier().equals(advert.getIdentifier()))
                .collect(Collectors.toList());
    }

    @Async
    @Transactional
    public void updateAdvertCharged(String id, boolean charged) {
        Advert advert = get(id);
        advert.setCharged(charged);
        advertRepository.save(advert);
    }

    public List<String> findPedigree(String searchText) {
        return advertRepository.findPedigree(searchText).orElseGet(ArrayList::new);
    }

    @CacheEvict(value = "advert", allEntries = true)
    public void expireAdverts(int days, EntityStatus status) {
        Pageable pageable = PageRequest.of(0, 100);
        SearchDto searchDto = SearchDto.builder()
                .dayBefore(days)
                .status(status)
                .build();
        search(searchDto, pageable)
                .get()
                .forEach(a -> {
                    EntityStatus newStatus = ACTIVE.equals(a.getStatus()) ? EntityStatus.PASSIVE : EntityStatus.REJECTED;
                    updateStatus(a.getIdentifier(), newStatus);
                    log.info("{} advert {} status changed to {} after {} days.", status, a.getAdvertNo(), newStatus, days);
                });
    }
}
