package com.haradan.domain.service;

import com.haradan.common.exception.exceptions.MicroException;
import com.haradan.domain.dao.entity.Contact;
import com.haradan.domain.dao.repository.ContactRepository;
import com.haradan.domain.dto.NotificationDto;
import com.haradan.domain.service.notification.NotificationService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.haradan.common.enumeration.NotificationType.CONTACT;

@Service
@AllArgsConstructor
public class ContactService {
    private final ContactRepository contactRepository;
    private NotificationService notificationService;

    public Contact get(String id) {
        return contactRepository.findById(id).orElseThrow(() -> MicroException.newDataNotFoundException(Contact.class.getSimpleName(), id));
    }

    @Transactional
    public void delete(String id) {
        Contact theReal = contactRepository.findById(id).orElseThrow(() -> MicroException.newDataNotFoundException(Contact.class.getSimpleName(), id));
        contactRepository.delete(theReal);
    }

    @Transactional
    public Contact save(Contact contact) {
        contact = contactRepository.save(contact);
        notificationService.notify(NotificationDto.builder().type(CONTACT).entity(contact).build());
        return contact;
    }
}
