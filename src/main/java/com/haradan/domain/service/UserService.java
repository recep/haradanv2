package com.haradan.domain.service;

import com.haradan.common.enumeration.AuthorityType;
import com.haradan.common.enumeration.ChannelType;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.common.exception.Exceptions;
import com.haradan.common.exception.dto.ExceptionData;
import com.haradan.common.exception.exceptions.MicroException;
import com.haradan.common.util.Constants;
import com.haradan.common.util.Utils;
import com.haradan.domain.controller.request.CreateNewPasswordRequest;
import com.haradan.domain.controller.request.PasswordRequest;
import com.haradan.domain.dao.entity.Setting;
import com.haradan.domain.dao.entity.User;
import com.haradan.domain.dao.repository.UserRepository;
import com.haradan.domain.dto.NotificationDto;
import com.haradan.domain.service.notification.NotificationService;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static com.haradan.common.enumeration.NotificationType.CREATE_NEW_PASSWORD;
import static com.haradan.common.enumeration.NotificationType.PASSWORD_CHANGE;
import static com.haradan.common.enumeration.NotificationType.PASSWORD_FORGOT;
import static com.haradan.common.enumeration.NotificationType.REGISTRATION;
import static com.haradan.common.exception.Exceptions.PASSWORD_NOT_MATCH;
import static com.haradan.common.exception.Exceptions.RESET_PASSWORD_KEY_NOT_MATCH;
import static com.haradan.common.exception.Exceptions.USER_ID_EMAIL_NOT_MATCH;
import static com.haradan.common.exception.Exceptions.USER_REGISTERED_WITH_EMAIL;
import static com.haradan.common.util.Utils.hashMD5;

@Service
@AllArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private NotificationService notificationService;

    public Optional<User> findByUsername(String username) {
        return userRepository.findByEmail(username);
    }

    public User get(String id) {
        return userRepository.findById(id).orElseThrow(() -> MicroException.newDataNotFoundException(User.class.getSimpleName(), id));
    }

    @Transactional
    @CacheEvict(value = "user", allEntries = true)
    public User save(User user) {

        if(user.getChannel()  ==  ChannelType.FORM){
            validateUserForm(user);
        }

        findByUsername(user.getUsername()).ifPresent(u -> {
            if (ChannelType.GOOGLE.equals(u.getChannel()))
                throw new MicroException(Exceptions.USER_REGISTERED_WITH_GOOGLE);
            else if (ChannelType.FACEBOOK.equals(u.getChannel()))
                throw new MicroException(Exceptions.USER_REGISTERED_WITH_FACEBOOK);
            throw new MicroException(Exceptions.USER_ALREADY_EXIST);
        });

        user.setPassword(passwordEncoder().encode(user.getPassword()));
        user.setStatus(EntityStatus.ACTIVE);
        user.setAuthorities(Collections.singletonList(AuthorityType.ROLE_USER));
        user.setSetting(Setting.buildDefault());
        user = userRepository.save(user);
        return user;
    }

    @Transactional
    @CacheEvict(value = "user", allEntries = true)
    public User put(String id, User user, boolean setAsAdmin) {

        if(user.getChannel()  ==  ChannelType.FORM){
            validateUserForm(user);
        }

        User theReal = get(id);
        theReal.setFirstName(user.getFirstName());
        theReal.setLastName(user.getLastName());
        theReal.setPhoneNumber(user.getPhoneNumber());
        if (setAsAdmin && !theReal.hasAdminAuthority()) {
            theReal.addAuthorities(Arrays.asList(AuthorityType.ROLE_ADMIN));
        }
        if (user.getStatus() != null) {
            theReal.setStatus(user.getStatus());
        }
        return userRepository.save(theReal);
    }

    private void validateUserForm(User user) {
        if(user.getPhoneNumber() == null || user.getPhoneNumber().isEmpty()  || user.getPhoneNumber().length() != 10){
            throw new MicroException(Exceptions.INVALID_PHONE_NUMBER);
        }
    }

    @Transactional
    @CacheEvict(value = "user", allEntries = true)
    public User putSetting(String id, Setting setting) {
        User theReal = get(id);
        theReal.setSetting(setting);
        return userRepository.save(theReal);
    }

    @CacheEvict(value = "user", allEntries = true)
    public User changePassword(String id, PasswordRequest passwordRequest) {
        User user = get(id);
        if (!user.getEmail().equalsIgnoreCase(passwordRequest.getEmail())) {
            throw new MicroException(USER_ID_EMAIL_NOT_MATCH);
        }
        user.setPassword(passwordEncoder().encode(passwordRequest.getNewPassword()));
        user = userRepository.save(user);
        notificationService.notify(NotificationDto.builder().type(PASSWORD_CHANGE).entity(user).build());
        return user;
    }

    @CacheEvict(value = "user", allEntries = true)
    public User createNewPassword(String id, CreateNewPasswordRequest passwordRequest) {
        User user = get(id);
        if (!user.getResetPasswordKey().equalsIgnoreCase(passwordRequest.getResetPasswordKey())) {
            throw new MicroException(RESET_PASSWORD_KEY_NOT_MATCH);
        }
        user.setPassword(passwordEncoder().encode(passwordRequest.getNewPassword()));
        user.setResetPasswordKey("");
        user = userRepository.save(user);
        notificationService.notify(NotificationDto.builder().type(CREATE_NEW_PASSWORD).entity(user).build());
        return user;
    }

    @Autowired
    private PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @CacheEvict(value = "user", allEntries = true)
    public void sendForgotPasswordLink(String username) {
        ExceptionData exceptionData = Exceptions.USER_NOT_FOUND;
        exceptionData.addParameter("email", username);

        User user = findByUsername(username).orElseThrow(() -> new MicroException(exceptionData));
        user.setResetPasswordKey(Utils.generateRandomText(36));
        user = userRepository.save(user);
        notificationService.notify(NotificationDto.builder().type(PASSWORD_FORGOT).entity(user).build());
    }

    public Page<User> search(String id, String email, String firstName, String lastName, String phoneNumber, EntityStatus status, Pageable pageable) {
        return userRepository.findAll(UserRepository.QueryGeneration.search(id, email, firstName, lastName, phoneNumber, status), pageable);
    }

    @Transactional
    @CacheEvict(value = "user", allEntries = true)
    public void updateStatus(String id, EntityStatus status) {
        User user = get(id);
        user.setStatus(status);
        userRepository.save(user);
    }

    @Transactional
    @CacheEvict(value = "user", allEntries = true)
    public void updatePhoneNumber(String id, String phoneNumber) {
        User user = get(id);
        user.setPhoneNumber(phoneNumber);
        userRepository.save(user);
    }

    @Transactional
    @CacheEvict(value = "user", allEntries = true)
    public String socialLogin(String email, String fullName, ChannelType channel) {
        String firstName = fullName;
        String lastName = null;
        if (StringUtils.isNotEmpty(fullName)) {
            String[] fullNameParts = fullName.split(" ");
            firstName = fullNameParts[0];
            lastName = fullNameParts[fullNameParts.length - 1];
        }

        String password = hashMD5(email, Constants.DEFAULT_PASSWORD);
        User user = findByUsername(email).orElse(null);
        if (user == null) {
            user = save(User.builder()
                    .email(email)
                    .firstName(firstName)
                    .lastName(lastName)
                    .channel(channel)
                    .password(password)
                    .build());

            notificationService.notify(NotificationDto.builder().type(REGISTRATION).entity(user).build());
        } else if (ChannelType.FORM.equals(user.getChannel()) || ChannelType.ADMIN_FORM.equals(user.getChannel())) {
            throw new MicroException(USER_REGISTERED_WITH_EMAIL);
        } else if (!user.getPassword().startsWith("{")) {
            user.setPassword(passwordEncoder().encode(password));
            userRepository.save(user);
        }

        return password;
    }

    // can be deleted after all user password replaced to bycrpt
    @CacheEvict(value = "user", allEntries = true)
    public void encryptPassword(PasswordRequest passwordRequest) {
        User user = findByUsername(passwordRequest.getEmail()).orElseThrow(() -> new MicroException(USER_ID_EMAIL_NOT_MATCH));
        if (user.getPassword().startsWith("{")) {
            return;
        }
        if (!user.getPassword().equals(hashMD5(passwordRequest.getEmail(), passwordRequest.getNewPassword()))) {
            throw new MicroException(PASSWORD_NOT_MATCH);
        }
        user.setPassword(passwordEncoder().encode(passwordRequest.getNewPassword()));
        userRepository.save(user);
    }
}

