package com.haradan.domain.service;

import com.haradan.common.enumeration.AuthorityType;
import com.haradan.common.enumeration.ChannelType;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.domain.dao.entity.Setting;
import com.haradan.domain.dao.entity.User;
import com.haradan.domain.dao.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;

@Slf4j
@Service
@RequiredArgsConstructor
public class InitService implements ApplicationRunner {

    private final UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @Override
    @Async
    @Transactional
    public void run(ApplicationArguments args) {
        this.addUser();
    }

    private void addUser() {
        User user = getUser("info@haradan.com", "haraa", "haradan", "admin","5321570810");
        User user2 = getUser("huseyinkizilbulak76@hotmail.com", "haraa", "Hüseyin", "Kızılbulak","5070603353");
        userRepository.findByEmail(user.getUsername()).orElseGet(() -> userRepository.save(user));
        userRepository.findByEmail(user2.getUsername()).orElseGet(() -> userRepository.save(user2));
        log.info("admin users added.");
    }

    private User getUser(String email, String password, String firstName, String lastName, String phoneNumber) {
        return User.builder()
                .status(EntityStatus.ACTIVE)
                .channel(ChannelType.ADMIN_FORM)
                .email(email)
                .password(passwordEncoder().encode(password))
                .firstName(firstName)
                .lastName(lastName)
                .phoneNumber(phoneNumber)
                .authorities(Arrays.asList(AuthorityType.ROLE_USER, AuthorityType.ROLE_ADMIN))
                .setting(Setting.buildDefault())
                .build();
    }
}
