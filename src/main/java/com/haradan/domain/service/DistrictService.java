package com.haradan.domain.service;

import com.haradan.common.exception.exceptions.MicroException;
import com.haradan.domain.dao.entity.District;
import com.haradan.domain.dao.repository.DistrictRepository;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class DistrictService {
    private final DistrictRepository districtRepository;

    @Cacheable(value = "district")
    public District get(String id) {
        return districtRepository.findById(id).orElseThrow(() -> MicroException.newDataNotFoundException(District.class.getSimpleName(), id));
    }

    @Transactional
    @CacheEvict(value = "district", allEntries = true)
    public District put(String id, District district) {
        District theReal = districtRepository.findById(id).orElseThrow(() -> MicroException.newDataNotFoundException(District.class.getSimpleName(), id));
        district.setIdentifier(theReal.getIdentifier());
        return districtRepository.save(district);
    }

    @Transactional
    @CacheEvict(value = "district", allEntries = true)
    public void delete(String id) {
        District theReal = districtRepository.findById(id).orElseThrow(() -> MicroException.newDataNotFoundException(District.class.getSimpleName(), id));
        districtRepository.delete(theReal);
    }

    @Transactional
    @CacheEvict(value = "district", allEntries = true)
    public District save(District district) {
        return districtRepository.save(district);
    }

    @Cacheable(value = "district")
    public Page<District> search(String cityCode, String name, String code, Pageable pageable) {
        return districtRepository.findAll(DistrictRepository.QueryGeneration.search(cityCode, name, code), pageable);
    }
}
