package com.haradan.domain.dao.entity.horse;

import com.haradan.common.dao.entity.BaseEntity;
import com.haradan.common.enumeration.horse.SiblingType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "horse_sibling")
@Getter
@Setter
public class HorseSibling extends BaseEntity {
	private static final long serialVersionUID = 134534545L;
	@ManyToOne(fetch = FetchType.LAZY)
	private Horse horse;
	@Enumerated(EnumType.STRING)
	private SiblingType siblingType;
	private String siblingName;
	private String fatherName;
	private String motherName;
	private Integer raceCount;
	private Integer first;
	private Integer second;
	private Integer third;
	private Integer fourth;
	private Long earning;
}
