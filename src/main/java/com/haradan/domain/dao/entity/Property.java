package com.haradan.domain.dao.entity;

import com.haradan.common.dao.entity.IdEntity;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.common.enumeration.PropertyType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
public class Property extends IdEntity {
    private static final long serialVersionUID = 3486887444513273933L;
    @Enumerated(EnumType.STRING)
    private EntityStatus status;
    private PropertyType type;
    private String name;
    private String searchText;
    private Boolean searchParam;
    private Boolean mandatory;
    private Long orderId;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id")
    private Property parent;
    @OrderBy("orderId")
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parent", cascade = CascadeType.ALL)
    private List<Property> children = new ArrayList<Property>();
    @ManyToOne(fetch = FetchType.LAZY)
    private Category category;
}