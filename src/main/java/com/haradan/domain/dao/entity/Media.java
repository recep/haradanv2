package com.haradan.domain.dao.entity;

import com.haradan.common.dao.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "media")
public class Media extends BaseEntity {
    private String imageUrl;
    @Type(type = "text")
    private String media;
}