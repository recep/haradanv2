package com.haradan.domain.dao.entity.horse;

import com.haradan.common.dao.entity.BaseAuditEntity;
import com.haradan.common.enumeration.EntityStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "horse")
@Getter
@Setter
public class Horse extends BaseAuditEntity {
	private static final long serialVersionUID = 20120824L;
	private EntityStatus status;
	private Long tjkNo;
	private Long identityNo;
	private String name;
	private String race;
	private Integer age;
	private String donu;
	private String gender;
	private String fatherName;
	private String motherName;
	private String maidenFatherName;
	private Date birthdate;
	private Integer hPoint;
	private String owner;
	private String grower;
	private String coach;
	private String last6RaceResult;
	private Date lastRaceDate;
	private Long earning;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "horse", orphanRemoval = true, cascade = CascadeType.ALL)
	@OrderBy(value = "year DESC, pistType DESC")
	private List<HorseStatistic> horseStatistics;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "horse", orphanRemoval = true, cascade = CascadeType.ALL)
	@OrderBy(value = "earning DESC")
	private List<HorseSibling> horseSiblings;
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "pedigri_id")
	private HorsePedigri horsePedigri;
}
