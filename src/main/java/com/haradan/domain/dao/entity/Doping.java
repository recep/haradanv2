package com.haradan.domain.dao.entity;

import com.haradan.common.dao.entity.BaseAuditEntity;
import com.haradan.common.enumeration.DopingType;
import com.haradan.common.enumeration.EntityStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "doping")
public class Doping extends BaseAuditEntity {
    @Enumerated(EnumType.STRING)
    private EntityStatus status;
    @Enumerated(EnumType.STRING)
    private DopingType doping;
    private LocalDate approveDate;
    @ManyToOne(fetch = FetchType.LAZY)
    private Advert advert;

    public LocalDate getExpireDate() {
        return getApproveDate().plusDays(getDoping().getDayOfDuration());
    }

    public LocalDate getApproveDate() {
        return approveDate == null ? getCreatedDate().toLocalDate() : approveDate;
    }

    public boolean isActive() {
        return EntityStatus.ACTIVE.equals(status);
    }
}