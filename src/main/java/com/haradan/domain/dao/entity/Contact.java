package com.haradan.domain.dao.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.haradan.common.dao.entity.BaseEntity;
import com.haradan.common.enumeration.EntityStatus;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
public class Contact extends BaseEntity {
    private static final long serialVersionUID = 3486887444513273933L;
    @Enumerated(EnumType.STRING)
    private EntityStatus status;
    private String name;
    private String email;
    private String phoneNumber;
    private String message;
}