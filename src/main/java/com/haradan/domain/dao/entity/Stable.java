package com.haradan.domain.dao.entity;

import com.haradan.common.dao.entity.IdEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
public class Stable extends IdEntity {
    private static final long serialVersionUID = 234234123L;
    private String name;
    private String phoneNumber;
    private String email;
    private String webpage;
    private String contactName;
    private String address;
    private String note;
}
