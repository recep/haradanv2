package com.haradan.domain.dao.entity;

import com.haradan.common.dao.entity.IdEntity;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity(name = "district")
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"city_id", "district_code"}),
        @UniqueConstraint(columnNames = {"city_id", "name"})
})
public class District extends IdEntity {
    private static final long serialVersionUID = 1524366L;

    @Column(nullable = false, name = "district_code", length = CODE_LENGTH)
    @NotNull
    private String districtCode;

    @Column(nullable = false, length = NAME_LENGTH)
    @NotNull
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @NotNull
    private City city;
}