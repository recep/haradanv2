package com.haradan.domain.dao.entity;

import com.haradan.common.dao.entity.IdEntity;
import com.haradan.common.enumeration.EntityStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
public class Category extends IdEntity {
    private static final long serialVersionUID = 234234123L;
    @Enumerated(EnumType.STRING)
    private EntityStatus status;
    private String name;
    private String searchText;
    private Integer price;
    private Long orderId;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id")
    private Category parent;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parent")
    private List<Category> children = new ArrayList<Category>();
}