package com.haradan.domain.dao.entity.horse;

import com.haradan.common.dao.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "horse_pedigri")
@Getter
@Setter
public class HorsePedigri extends BaseEntity {
	private static final long serialVersionUID = 134534546L;
	@Type(type = "text")
	private String pedigri;
}