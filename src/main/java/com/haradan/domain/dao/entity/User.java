package com.haradan.domain.dao.entity;

import com.haradan.common.dao.entity.BaseEntity;
import com.haradan.common.enumeration.AuthorityType;
import com.haradan.common.enumeration.ChannelType;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.common.util.Constants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.CollectionUtils;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User extends BaseEntity implements UserDetails {
    private static final long serialVersionUID = -6208248864071830402L;
    @Enumerated(EnumType.STRING)
    private EntityStatus status;
    @Enumerated(EnumType.STRING)
    private ChannelType channel;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "ADMIN_USER_ROLES", joinColumns = @JoinColumn(name = "USER_ID"))
    private List<AuthorityType> authorities = new ArrayList<>();
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "setting_id")
    private Setting setting;
    private String resetPasswordKey;

    public void addAuthorities(List<AuthorityType> authorities) {
        this.authorities.addAll(authorities);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> authorityCollection = new ArrayList<>();
        List<AuthorityType> userRoles = authorities;
        if (!CollectionUtils.isEmpty(userRoles)) {
            userRoles.forEach(role -> {
                SimpleGrantedAuthority authority = new SimpleGrantedAuthority(role.name());
                authorityCollection.add(authority);
            });
        }
        return authorityCollection;
    }

    public boolean hasAdminAuthority() {
        return this.authorities.stream().anyMatch(a -> a.getName().equals(AuthorityType.ROLE_ADMIN.getName()));
    }

    @Override
    public String getUsername() {
        return getEmail();
    }

    public String getFullName() {
        return this.getFirstName() + Constants.space + this.getLastName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
