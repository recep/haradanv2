package com.haradan.domain.dao.entity;

import com.haradan.common.dao.entity.BaseEntity;
import com.haradan.common.enumeration.ArticleType;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.common.util.Constants;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
public class Article extends BaseEntity {
    private static final long serialVersionUID = 3486887444513273933L;
    @Enumerated(EnumType.STRING)
    private EntityStatus status;
    @Enumerated(EnumType.STRING)
    private ArticleType type;
    private String title;
    @Type(type = "text")
    private String summary;
    @Type(type = "text")
    private String article;
    private String searchText;
    private String media;
    private Long orderId;

    public String getMediaPath() {
        return Constants.slash + type.getName() + Constants.slash + type.getName() + Constants.slash + this.identifier;
    }
}