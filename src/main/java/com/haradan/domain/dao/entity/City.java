package com.haradan.domain.dao.entity;

import com.haradan.common.dao.entity.IdEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

@Data
@Entity(name = "city")
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"country_code", "city_code"}),
        @UniqueConstraint(columnNames = {"country_code", "name"})
})
public class City extends IdEntity {

    private static final long serialVersionUID = 198767454L;

    @Column(name = "country_code")
    private String countryCode;

    @Column(nullable = false, name = "city_code", length = CODE_LENGTH)
    @NotNull
    private String cityCode;

    @Column(nullable = false, length = NAME_LENGTH)
    @NotNull
    private String name;
}