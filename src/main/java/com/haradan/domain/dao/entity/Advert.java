package com.haradan.domain.dao.entity;

import com.haradan.common.dao.entity.BaseAuditEntity;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.common.util.Constants;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Type;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
public class Advert extends BaseAuditEntity {
    private static final long serialVersionUID = -6208248864071830402L;

    private Long advertNo;
    @Enumerated(EnumType.STRING)
    private EntityStatus status = EntityStatus.NOT_COMPLETED;
    private String title;
    @Type(type = "text")
    private String description;
    private String searchText;
    private Integer viewCount = 0;
    private BigDecimal price;
    private String city;
    private String district;
    private Long tjkNo;
    private boolean charged = false;
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "media_id")
    private Media media;
    @ManyToOne(fetch = FetchType.LAZY)
    private Category category;
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    public String getUrl() {
        return this.getCategory().getSearchText() + Constants.slash + this.getSearchText() + Constants.slash + this.getIdentifier();
    }

    public String getLocation() {
        return this.getCity() + Constants.slash + this.getDistrict();
    }

    public String getMediaPath() {
        return Constants.slash + LocalDate.from(this.media.getCreatedDate()).getYear() +
                Constants.slash + LocalDate.from(this.media.getCreatedDate()).getMonthValue() +
                Constants.slash + this.identifier;
    }

    public int getChargePrice() {
        return this.category == null && this.isCharged() ? 0 : this.category.getPrice();
    }
}
