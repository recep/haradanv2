package com.haradan.domain.dao.entity;

import com.haradan.common.dao.entity.IdEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "setting")
public class Setting extends IdEntity {
    private boolean showPhone;
    private boolean showEmail;
    private boolean notification;
    private boolean campaignNotification;

    public static Setting buildDefault() {
        return Setting.builder()
                .showPhone(true)
                .showEmail(false)
                .notification(false)
                .campaignNotification(true)
                .build();
    }
}