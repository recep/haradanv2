package com.haradan.domain.dao.entity.horse;

import com.haradan.common.dao.entity.BaseEntity;
import com.haradan.common.enumeration.horse.PistType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "horse_statistic")
@Getter
@Setter
public class HorseStatistic extends BaseEntity {
	private static final long serialVersionUID = 134534545L;
	@ManyToOne(fetch = FetchType.LAZY)
	private Horse horse;
	private PistType pistType;
	private Integer year;
	private Integer raceCount;
	private Integer first;
	private Integer second;
	private Integer third;
	private Integer fourth;
	private Integer fifth;
	private Long earning;
}
