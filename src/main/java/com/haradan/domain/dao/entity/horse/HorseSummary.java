package com.haradan.domain.dao.entity.horse;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.ZonedDateTime;
import java.util.Objects;

@Entity
@Table(name = "horse_summary")
@Getter
@Setter
public class HorseSummary {
    private static final long serialVersionUID = 20120234824L;

    @CreatedDate
    @Column(name = "created_date", nullable = false)
    private ZonedDateTime createdDate = ZonedDateTime.now();

    @Id
    private Long tjkNo;
    private Long identityNo;
    private String name;
    private String fatherName;
    private String motherName;
    private String race;

    public int hashCode() {
        return Objects.hash(this.tjkNo);
    }

    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        } else {
            return obj != null && this.getClass() == obj.getClass() && Objects.equals(this.tjkNo, ((HorseSummary) obj).tjkNo);
        }
    }
}
