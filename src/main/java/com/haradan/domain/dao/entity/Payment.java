package com.haradan.domain.dao.entity;

import com.haradan.common.dao.entity.BaseAuditEntity;
import com.haradan.common.enumeration.EntityStatus;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
@Entity
@NoArgsConstructor
public class Payment extends BaseAuditEntity {
    private static final long serialVersionUID = 2124123134234111L;
    @Enumerated(EnumType.STRING)
    private EntityStatus status = EntityStatus.NOT_COMPLETED;
    private String merchantId;
    private String productId;
    private String productName;
    private String paymentAmount;
    private String userName;
    private String userEmail;
    private String userAddress;
    private String userPhone;
    private String ip;
    @Type(type = "text")
    private String tokenRequest;
    @Type(type = "text")
    private String tokenResponse;
    @Type(type = "text")
    private String notifyRequest;
    @Type(type = "text")
    private String notifyResponse;

}