package com.haradan.domain.dao.entity;

import com.haradan.common.dao.entity.IdEntity;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.common.enumeration.SmsType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@Entity(name = "SMS_DATA")
@NoArgsConstructor
@AllArgsConstructor
public class SmsData extends IdEntity {
    private static final long serialVersionUID = 234234457123L;
    @Enumerated(EnumType.STRING)
    private EntityStatus status = EntityStatus.DEFAULT;
    @Enumerated(EnumType.STRING)
    private SmsType type;
    private String entity;
    private String entityId;
    private String phoneNumber;
}