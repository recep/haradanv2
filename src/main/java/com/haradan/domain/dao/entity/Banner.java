package com.haradan.domain.dao.entity;

import com.haradan.common.dao.entity.BaseEntity;
import com.haradan.common.enumeration.BannerType;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.common.util.Constants;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDate;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
public class Banner extends BaseEntity {
    private static final long serialVersionUID = 3486887444513273933L;
    @Enumerated(EnumType.STRING)
    private EntityStatus status;
    @Enumerated(EnumType.STRING)
    private BannerType type;
    private String name;
    private String searchText;
    private Long price;
    private Long orderId;
    private String url;
    private String media;
    private String description;
    private LocalDate startDate;
    private LocalDate endDate;

    public String getMediaPath() {
        return Constants.slash + type.getName() + Constants.slash + type.getName() + Constants.slash + this.identifier;
    }
}