package com.haradan.domain.dao.entity;

import com.haradan.common.dao.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Data
@Entity(name = "ADVERT_PROPERTY")
public class AdvertProperty extends BaseEntity {
    private String searchText;
    private String value;
    @ManyToOne
    private Property property;
    @ManyToOne
    private Advert advert;
}