package com.haradan.domain.dao.repository;

import com.haradan.common.dao.repository.BaseRepository;
import com.haradan.domain.dao.entity.Contact;

public interface ContactRepository extends BaseRepository<Contact, String> {
}
