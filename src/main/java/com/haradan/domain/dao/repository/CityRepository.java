package com.haradan.domain.dao.repository;

import com.haradan.common.dao.repository.BaseRepository;
import com.haradan.domain.dao.entity.City;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface CityRepository extends BaseRepository<City, String> {

    List<City> findByCountryCode(String countryCode);

    Optional<City> findByCountryCodeAndCityCode(String countryCode, String code);

    Optional<City> findByCountryCodeAndName(String countryCode, String name);

    class QueryGeneration extends BaseRepository.QueryGeneration {
        public static Specification<City> search(String countryCode, String name, String code) {
            return (root, query, cb) -> {
                List<Predicate> predicates = getPredicateArray();
                predicates.add(equalPredicate(cb, root, "countryCode", countryCode));
                predicates.add(equalPredicate(cb, root, "cityCode", code));
                predicates.add(nameLikePredicate(cb, root, name));
                return cb.and(predicates.toArray(new Predicate[predicates.size()]));
            };
        }
    }
}