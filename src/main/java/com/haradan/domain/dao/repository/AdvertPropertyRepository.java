package com.haradan.domain.dao.repository;

import com.haradan.common.dao.repository.BaseRepository;
import com.haradan.domain.dao.entity.Advert;
import com.haradan.domain.dao.entity.AdvertProperty;

import java.util.List;
import java.util.Optional;

public interface AdvertPropertyRepository extends BaseRepository<AdvertProperty, String> {

    Optional<List<AdvertProperty>> findByAdvert(Advert advert);
}
