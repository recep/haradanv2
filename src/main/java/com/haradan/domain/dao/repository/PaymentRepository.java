package com.haradan.domain.dao.repository;

import com.haradan.common.dao.repository.BaseRepository;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.domain.dao.entity.Payment;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.List;

public interface PaymentRepository extends BaseRepository<Payment, String> {

    Payment findByMerchantId(String merchantId);

    class QueryGeneration extends BaseRepository.QueryGeneration {
        public static Specification<Payment> search(String productId, String email, String name, EntityStatus status) {
            return (root, query, cb) -> {
                List<Predicate> predicates = getPredicateArray();
                predicates.add(equalPredicate(cb, root, "productId", productId));
                predicates.add(likePredicate(cb, root, "userName", name));
                predicates.add(likePredicate(cb, root, "userEmail", email));
                predicates.add(statusPredicate(cb, root, status));
                return cb.and(predicates.toArray(new Predicate[predicates.size()]));
            };
        }
    }
}
