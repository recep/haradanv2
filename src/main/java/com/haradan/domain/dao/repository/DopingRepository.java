package com.haradan.domain.dao.repository;

import com.haradan.common.dao.repository.BaseRepository;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.domain.dao.entity.Advert;
import com.haradan.domain.dao.entity.Doping;

import java.util.List;
import java.util.Optional;

public interface DopingRepository extends BaseRepository<Doping, String> {

    Optional<List<Doping>> findByAdvert(Advert advert);

    List<Doping> findByStatus(EntityStatus status);

}
