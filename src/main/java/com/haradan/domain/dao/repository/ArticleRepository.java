package com.haradan.domain.dao.repository;

import java.util.List;

import javax.persistence.criteria.Predicate;

import org.springframework.data.jpa.domain.Specification;

import com.haradan.common.dao.repository.BaseRepository;
import com.haradan.common.enumeration.ArticleType;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.domain.dao.entity.Article;
import com.haradan.domain.dto.ArticleSearchDto;

public interface ArticleRepository extends BaseRepository<Article, String> {

    class QueryGeneration extends BaseRepository.QueryGeneration {
        public static Specification<Article> search(ArticleType type) {
            return (root, query, cb) -> {
                List<Predicate> predicates = getPredicateArray();
                predicates.add(cb.equal(root.get("type"), type));
                predicates.add(statusPredicate(cb, root, EntityStatus.ACTIVE));
                return cb.and(predicates.toArray(new Predicate[predicates.size()]));
            };
        }
        
        public static Specification<Article> search(ArticleSearchDto searchDto) {
            return (root, query, cb) -> {
                List<Predicate> predicates = getPredicateArray();
                predicates.add(idPredicate(cb, root, searchDto.getId()));
                predicates.add(statusPredicate(cb, root, searchDto.getStatus()));
                predicates.add(typePredicate(cb, root, searchDto.getType()));
                return cb.and(predicates.toArray(new Predicate[predicates.size()]));
            };
        }
    }

}
