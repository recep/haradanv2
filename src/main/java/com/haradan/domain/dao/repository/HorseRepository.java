package com.haradan.domain.dao.repository;

import com.haradan.common.dao.repository.BaseRepository;
import com.haradan.domain.dao.entity.horse.Horse;

public interface HorseRepository extends BaseRepository<Horse, String> {
    public Horse findByTjkNo(Long tjkNo);
}
