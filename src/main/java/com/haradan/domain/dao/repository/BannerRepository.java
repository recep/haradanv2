package com.haradan.domain.dao.repository;

import java.util.List;

import javax.persistence.criteria.Predicate;

import org.springframework.data.jpa.domain.Specification;

import com.haradan.common.dao.repository.BaseRepository;
import com.haradan.domain.dao.entity.Banner;
import com.haradan.domain.dto.BannerSearchDto;

public interface BannerRepository extends BaseRepository<Banner, String> {

    class QueryGeneration extends BaseRepository.QueryGeneration {
        public static Specification<Banner> search(BannerSearchDto searchDto) {
            return (root, query, cb) -> {
                List<Predicate> predicates = getPredicateArray();
                predicates.add(idPredicate(cb, root, searchDto.getId()));
                predicates.add(typePredicate(cb, root, searchDto.getType()));
                predicates.add(statusPredicate(cb, root, searchDto.getStatus()));
                return cb.and(predicates.toArray(new Predicate[predicates.size()]));
            };
        }
    }
}
