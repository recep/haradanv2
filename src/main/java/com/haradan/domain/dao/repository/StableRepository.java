package com.haradan.domain.dao.repository;

import com.haradan.common.dao.repository.BaseRepository;
import com.haradan.domain.dao.entity.Stable;
import com.haradan.domain.dto.StableSearchDto;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.List;

public interface StableRepository extends BaseRepository<Stable, String> {

    class QueryGeneration extends BaseRepository.QueryGeneration {
        public static Specification<Stable> search(StableSearchDto searchDto){
            return (root, query, cb)  -> {
                List<Predicate> predicates = getPredicateArray();
                predicates.add(likePredicate(cb, root, "name", searchDto.getName()));
                return cb.and(predicates.toArray(new javax.persistence.criteria.Predicate[predicates.size()]));
            };
        }
    }
}
