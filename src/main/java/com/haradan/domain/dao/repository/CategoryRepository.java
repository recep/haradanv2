package com.haradan.domain.dao.repository;

import com.haradan.common.dao.repository.BaseRepository;
import com.haradan.domain.dao.entity.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends BaseRepository<Category, String> {
    Optional<List<Category>> findByParent(Category parent);

    Optional<Category> findBySearchText(String searchText);

}
