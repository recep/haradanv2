package com.haradan.domain.dao.repository;

import java.util.List;
import java.util.Optional;

import com.haradan.common.dao.repository.BaseRepository;
import com.haradan.domain.dao.entity.Advert;
import com.haradan.domain.dao.entity.Favourite;
import com.haradan.domain.dao.entity.User;

public interface FavouriteRepository extends BaseRepository<Favourite, String> {

    Optional<Favourite> findByAdvertAndUser(Advert advert, User user);
    
    Optional<List<Favourite>> findByUser(User user);
    
    Optional<List<Favourite>> findByAdvert(Advert advert);

}
