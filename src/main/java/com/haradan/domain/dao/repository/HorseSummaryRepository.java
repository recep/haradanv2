package com.haradan.domain.dao.repository;

import com.haradan.common.dao.repository.BaseRepository;
import com.haradan.domain.dao.entity.horse.HorseSummary;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.List;

public interface HorseSummaryRepository extends BaseRepository<HorseSummary, String> {
    HorseSummary findByTjkNo(Long tjkNo);

    class QueryGeneration extends BaseRepository.QueryGeneration {
        public static Specification<HorseSummary> search(String name) {
            return (root, query, cb) -> {
                List<Predicate> predicates = getPredicateArray();
                predicates.add(nameLikePredicate(cb, root, name));
                return cb.and(predicates.toArray(new Predicate[predicates.size()]));
            };
        }
    }
}
