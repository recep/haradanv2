package com.haradan.domain.dao.repository;

import com.haradan.common.dao.repository.BaseRepository;
import com.haradan.domain.dao.entity.Media;
import org.springframework.stereotype.Repository;

@Repository
public interface MediaRepository extends BaseRepository<Media, String> {
}