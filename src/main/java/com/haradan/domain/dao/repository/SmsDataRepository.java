package com.haradan.domain.dao.repository;

import com.haradan.common.dao.repository.BaseRepository;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.domain.dao.entity.Category;
import com.haradan.domain.dao.entity.SmsData;

import java.util.List;
import java.util.Optional;

public interface SmsDataRepository extends BaseRepository<SmsData, String> {

    List<SmsData> findByStatus(EntityStatus status);

}
