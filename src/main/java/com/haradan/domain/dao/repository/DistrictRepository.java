package com.haradan.domain.dao.repository;

import com.haradan.common.dao.repository.BaseRepository;
import com.haradan.domain.dao.entity.City;
import com.haradan.domain.dao.entity.District;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.Predicate;
import java.util.List;
import java.util.Optional;

@Repository
public interface DistrictRepository extends BaseRepository<District, String> {

    List<District> findByCity(City city);

    Optional<District> findByCityAndDistrictCode(City city, String code);

    Optional<District> findByCityAndName(City city, String name);

    class QueryGeneration extends BaseRepository.QueryGeneration {
        public static Specification<District> search(String cityCode, String name, String code) {
            return (root, query, cb) -> {
                List<Predicate> predicates = getPredicateArray();
                predicates.add(cb.equal(root.join("city").get("identifier"), cityCode));
                predicates.add(equalPredicate(cb, root, "districtCode", code));
                predicates.add(nameLikePredicate(cb, root, name));
                return cb.and(predicates.toArray(new Predicate[predicates.size()]));
            };
        }
    }

}