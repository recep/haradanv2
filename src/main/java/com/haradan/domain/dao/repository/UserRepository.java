package com.haradan.domain.dao.repository;

import com.haradan.common.dao.repository.BaseRepository;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.domain.dao.entity.User;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.List;
import java.util.Optional;

public interface UserRepository extends BaseRepository<User, String> {

    @Cacheable(value = "user")
    Optional<User> findByEmail(String email);

    class QueryGeneration extends BaseRepository.QueryGeneration {
        public static Specification<User> search(String id, String email, String firstName, String lastName, String phoneNumber, EntityStatus status) {
            return (root, query, cb) -> {
                List<Predicate> predicates = getPredicateArray();
                predicates.add(idPredicate(cb, root, id));
                predicates.add(likePredicate(cb, root, "email", email));
                predicates.add(likePredicate(cb, root, "firstName", firstName));
                predicates.add(likePredicate(cb, root, "lastName", lastName));
                predicates.add(likePredicate(cb, root, "phoneNumber", phoneNumber));
                predicates.add(statusPredicate(cb, root, status));
                return cb.and(predicates.toArray(new Predicate[predicates.size()]));
            };
        }
    }
}
