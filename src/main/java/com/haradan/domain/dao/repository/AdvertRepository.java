package com.haradan.domain.dao.repository;

import com.haradan.common.dao.repository.BaseRepository;
import com.haradan.common.enumeration.DopingType;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.common.util.Constants;
import com.haradan.domain.dao.entity.Advert;
import com.haradan.domain.dao.entity.AdvertProperty;
import com.haradan.domain.dao.entity.Category;
import com.haradan.domain.dao.entity.Doping;
import com.haradan.domain.dao.entity.Favourite;
import com.haradan.domain.dao.entity.Media;
import com.haradan.domain.dao.entity.User;
import com.haradan.domain.dto.AdDopingSearchDto;
import com.haradan.domain.dto.SearchDto;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public interface AdvertRepository extends BaseRepository<Advert, String> {

    Optional<Advert> findByAdvertNo(Long advertNo);

    @Query(value = "select max(advert_no) from advert", nativeQuery = true)
    Optional<Long> findMaxAdvertNo();

    @Query(value = "select DISTINCT (UPPER(ap.value)) from advert_property ap, advert a " +
            "where ap.advert_id=a.id and a.status='ACTIVE' and ap.search_text=:searchText " +
            "order by UPPER(ap.value) asc;", nativeQuery = true)
    Optional<List<String>> findPedigree(@Param("searchText") String searchText);

    class QueryGeneration extends BaseRepository.QueryGeneration {

        public static Specification<Advert> search(SearchDto searchDto) {
            return (root, query, cb) -> {
                List<Predicate> predicates = getPredicateArray();
                predicates.add(statusPredicate(cb, root, searchDto.getStatus()));
                predicates.add(idPredicate(cb, root, searchDto.getId()));
                predicates.add(likePredicate(cb, root, "city", searchDto.getCity()));
                predicates.add(likePredicate(cb, root, "district", searchDto.getDistrict()));
                predicates.add(StringUtils.isEmpty(searchDto.getAdvertNo()) ? null : equalPredicate(cb, root, "advertNo", searchDto.getAdvertNo().toString()));
                predicates.add(StringUtils.isEmpty(searchDto.getMaxPrice()) ? null : cb.lessThanOrEqualTo(root.get("price"), searchDto.getMaxPrice()));
                predicates.add(StringUtils.isEmpty(searchDto.getMinPrice()) ? null : cb.greaterThanOrEqualTo(root.get("price"), searchDto.getMinPrice()));
                predicates.add(StringUtils.isEmpty(searchDto.getDateInterval()) ? null : cb.greaterThanOrEqualTo(root.get("createdDate"), ZonedDateTime.now().minusDays(searchDto.getDateInterval())));
                predicates.add(StringUtils.isEmpty(searchDto.getDayBefore()) ? null : cb.lessThan(root.get("createdDate"), ZonedDateTime.now().minusDays(searchDto.getDayBefore())));
                resolveCategory(predicates, query, root, cb, searchDto);
                resolveMedia(predicates, query, root, cb, searchDto);
                resolveUser(predicates, query, root, cb, searchDto);
                resolveProperty(predicates, query, root, cb, searchDto);
                resolveSearchText(predicates, query, root, cb, searchDto);
                return cb.and(predicates.toArray(new Predicate[predicates.size()]));
            };
        }

        public static Specification<Advert> search(AdDopingSearchDto searchDto) {
            return (root, query, cb) -> {
                List<Predicate> predicates = getPredicateArray();
                predicates.add(idPredicate(cb, root, searchDto.getAdvertId()));

                Subquery<Doping> subQuery = query.subquery(Doping.class);
                Root<Doping> subRoot = subQuery.from(Doping.class);
                subQuery.select(subRoot);
                List<Predicate> subQueryPredicates = getPredicateArray();
                if (searchDto.getStatus() != null) {
                    subQueryPredicates.add(statusPredicate(cb, subRoot, searchDto.getStatus()));
                } else {
                    subQueryPredicates.add(statusNotPredicate(cb, subRoot, EntityStatus.DELETED));
                }
                subQueryPredicates.add(dopingPredicate(cb, subRoot, searchDto.getType()));
                subQueryPredicates.add(cb.equal(subRoot.join("advert").get("identifier"), root.get("identifier")));
                subQuery.where(subQueryPredicates.toArray(new Predicate[]{}));

                predicates.add(cb.exists(subQuery));
                return cb.and(predicates.toArray(new Predicate[predicates.size()]));
            };
        }

        public static void resolveSearchText(List<Predicate> predicates, CriteriaQuery<?> query, Root<?> root, CriteriaBuilder cb, SearchDto searchDto) {
            if (!StringUtils.isEmpty(searchDto.getSearchText())) {
                List<Predicate> orPredicates = getPredicateArray();
                orPredicates.add(likePredicate(cb, root, "title", searchDto.getSearchText()));
                resolveCategorySearchText(orPredicates, query, root, cb, searchDto);
                resolvePropertySearchText(orPredicates, query, root, cb, searchDto);
                predicates.add(cb.or(orPredicates.toArray(new Predicate[orPredicates.size()])));
            }
        }

        private static void resolveCategory(List<Predicate> predicates, CriteriaQuery<?> query, Root<?> root, CriteriaBuilder cb, SearchDto searchDto) {
            if (!CollectionUtils.isEmpty(searchDto.getCategoryIds())) {
                Subquery<Category> subQuery = query.subquery(Category.class);
                Root<Category> subRoot = subQuery.from(Category.class);
                subQuery.select(subRoot);
                List<Predicate> subQueryPredicates = getPredicateArray();
                subQueryPredicates.add(idsPredicate(cb, subRoot, searchDto.getCategoryIds()));
                subQueryPredicates.add(cb.equal(subRoot.get("identifier"), root.join("category")));
                subQuery.where(subQueryPredicates.toArray(new Predicate[]{}));
                predicates.add(cb.exists(subQuery));
            }
        }

        private static void resolveMedia(List<Predicate> predicates, CriteriaQuery<?> query, Root<Advert> root, CriteriaBuilder cb, SearchDto searchDto) {
            if (BooleanUtils.isTrue(searchDto.getHasPhoto())) {
                Subquery<Media> subQuery = query.subquery(Media.class);
                Root<Media> subRoot = subQuery.from(Media.class);
                subQuery.select(subRoot);
                List<Predicate> subQueryPredicates = getPredicateArray();
                subQueryPredicates.add(cb.equal(subRoot.get("identifier"), root.join("media")));
                subQuery.where(subQueryPredicates.toArray(new Predicate[]{}));
                predicates.add(cb.exists(subQuery));
            }
        }

        private static void resolveUser(List<Predicate> predicates, CriteriaQuery<?> query, Root<Advert> root, CriteriaBuilder cb, SearchDto searchDto) {
            if (!StringUtils.isEmpty(searchDto.getUserId())) {
                Subquery<User> subQuery = query.subquery(User.class);
                Root<User> subRoot = subQuery.from(User.class);
                subQuery.select(subRoot);
                List<Predicate> subQueryPredicates = getPredicateArray();
                subQueryPredicates.add(cb.equal(subRoot.get("identifier"), searchDto.getUserId()));
                subQueryPredicates.add(cb.equal(subRoot.get("identifier"), root.join("user")));
                subQuery.where(subQueryPredicates.toArray(new Predicate[]{}));
                predicates.add(cb.exists(subQuery));
            }
        }

        private static void resolveProperty(List<Predicate> predicates, CriteriaQuery<?> query, Root<Advert> root, CriteriaBuilder cb, SearchDto searchDto) {
            if (!CollectionUtils.isEmpty(searchDto.getProperties())) {
                searchDto.getProperties().keySet().forEach(p -> {
                    Subquery<AdvertProperty> subQuery = query.subquery(AdvertProperty.class);
                    Root<AdvertProperty> subRoot = subQuery.from(AdvertProperty.class);
                    subQuery.select(subRoot);
                    List<Predicate> subQueryPredicates = getPredicateArray();
                    subQueryPredicates.add(equalPredicate(cb, subRoot, "searchText", p));
                    subQueryPredicates.add(inPredicate(cb, subRoot, "value", Arrays.stream(searchDto.getProperties().get(p).split(Constants.comma)).collect(Collectors.toSet())));
                    subQueryPredicates.add(cb.equal(subRoot.join("advert").get("identifier"), root.get("identifier")));
                    subQuery.where(subQueryPredicates.toArray(new Predicate[]{}));
                    predicates.add(cb.exists(subQuery));
                });
            }
        }

        private static void resolvePropertySearchText(List<Predicate> predicates, CriteriaQuery<?> query, Root<?> root, CriteriaBuilder cb, SearchDto searchDto) {
            if (!StringUtils.isEmpty(searchDto.getSearchText())) {
                Subquery<AdvertProperty> subQuery = query.subquery(AdvertProperty.class);
                Root<AdvertProperty> subRoot = subQuery.from(AdvertProperty.class);
                subQuery.select(subRoot);
                List<Predicate> subQueryPredicates = getPredicateArray();
                subQueryPredicates.add(cb.equal(subRoot.join("advert").get("identifier"), root.get("identifier")));
                subQueryPredicates.add(likePredicate(cb, subRoot, "value", searchDto.getSearchText()));
                subQuery.where(subQueryPredicates.toArray(new Predicate[]{}));
                predicates.add(cb.exists(subQuery));
            }
        }

        private static void resolveCategorySearchText(List<Predicate> predicates, CriteriaQuery<?> query, Root<?> root, CriteriaBuilder cb, SearchDto searchDto) {
            if (!StringUtils.isEmpty(searchDto.getSearchText())) {
                Subquery<Category> subQuery = query.subquery(Category.class);
                Root<Category> subRoot = subQuery.from(Category.class);
                subQuery.select(subRoot);
                List<Predicate> subQueryPredicates = getPredicateArray();
                subQueryPredicates.add(likePredicate(cb, subRoot, "name", searchDto.getSearchText()));
                subQueryPredicates.add(cb.equal(subRoot.get("identifier"), root.join("category")));
                subQuery.where(subQueryPredicates.toArray(new Predicate[]{}));
                predicates.add(cb.exists(subQuery));
            }
        }

        public static Specification<Advert> searchDoping(DopingType doping) {
            return (root, query, cb) -> {
                List<Predicate> predicates = getPredicateArray();
                predicates.add(statusPredicate(cb, root, EntityStatus.ACTIVE));

                Subquery<Doping> subQuery = query.subquery(Doping.class);
                Root<Doping> subRoot = subQuery.from(Doping.class);
                subQuery.select(subRoot);
                List<Predicate> subQueryPredicates = getPredicateArray();
                subQueryPredicates.add(statusPredicate(cb, subRoot, EntityStatus.ACTIVE));
                subQueryPredicates.add(cb.equal(subRoot.get("doping"), doping));
                subQueryPredicates.add(cb.equal(subRoot.join("advert").get("identifier"), root.get("identifier")));
                subQuery.where(subQueryPredicates.toArray(new Predicate[]{}));

                predicates.add(cb.exists(subQuery));
                return cb.and(predicates.toArray(new Predicate[predicates.size()]));
            };
        }

        public static Specification<Advert> searchFavouriteAds(User user) {
            return (root, query, cb) -> {
                List<Predicate> predicates = getPredicateArray();
                Subquery<Favourite> subQuery = query.subquery(Favourite.class);
                Root<Favourite> subRoot = subQuery.from(Favourite.class);
                subQuery.select(subRoot);
                List<Predicate> subQueryPredicates = getPredicateArray();
                subQueryPredicates.add(cb.equal(subRoot.get("user").get("identifier"), user.getIdentifier()));
                subQueryPredicates.add(cb.equal(subRoot.join("advert").get("identifier"), root.get("identifier")));
                subQuery.where(subQueryPredicates.toArray(new Predicate[]{}));

                predicates.add(cb.exists(subQuery));
                return cb.and(predicates.toArray(new Predicate[predicates.size()]));
            };
        }
    }
}
