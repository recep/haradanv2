package com.haradan.domain.dao.repository;

import com.haradan.common.dao.repository.BaseRepository;
import com.haradan.domain.dao.entity.Category;
import com.haradan.domain.dao.entity.Property;

import java.util.List;
import java.util.Optional;

public interface PropertyRepository extends BaseRepository<Property, String> {

    Optional<List<Property>> findByCategoryOrderByOrderId(Category category);
}
