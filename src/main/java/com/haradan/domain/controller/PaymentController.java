package com.haradan.domain.controller;

import com.haradan.common.controller.AbstractController;
import com.haradan.domain.service.PaymentService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/haradan/api/payment")
@AllArgsConstructor
public class PaymentController extends AbstractController {
    PaymentService paymentService;

    @PostMapping(value = "/paymentNotify")
    public void notify(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String responseObject = paymentService.notify(request);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(responseObject);
    }
}