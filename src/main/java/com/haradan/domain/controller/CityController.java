package com.haradan.domain.controller;

import com.haradan.common.dao.repository.CustomPagedResource;
import com.haradan.common.dao.repository.CustomPagedResourceConverter;
import com.haradan.common.exception.exceptions.MicroException;
import com.haradan.common.controller.AbstractController;
import com.haradan.domain.controller.request.CityRequest;
import com.haradan.domain.controller.response.CityResponse;
import com.haradan.domain.mapper.CityMapper;
import com.haradan.domain.service.CityService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/cities")
@AllArgsConstructor
public class CityController extends AbstractController {
    private CityService cityService;
    private CityMapper cityMapper;

    @GetMapping("/{id}")
    public CityResponse get(@PathVariable String id) throws MicroException {
        return cityMapper.toResponse(cityService.get(id));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public void put(@PathVariable String id, @RequestBody @Validated CityRequest cityRequest) {
        cityService.put(id, cityMapper.toEntity(cityRequest));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public void delete(@PathVariable String id) {
        cityService.delete(id);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public void save(@RequestBody @Validated CityRequest cityRequest) {
        cityService.save(cityMapper.toEntity(cityRequest));
    }

    @GetMapping("/search")
    public CustomPagedResource<List<CityResponse>> pageable(@RequestParam("page") int page,
                                                            @RequestParam("size") int size,
                                                            @RequestParam(required = false, defaultValue = "name") String sort,
                                                            @RequestParam(required = false, defaultValue = "ASC") String direction,
                                                            @RequestParam("countryCode") String countryCode,
                                                            @RequestParam(name = "name", required = false) String name,
                                                            @RequestParam(name = "code", required = false) String code) {
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.valueOf(direction), sort);
        Page<CityResponse> map = cityService.search(countryCode, name, code, pageable).map(m -> cityMapper.toResponse(m));
        return CustomPagedResourceConverter.map(map, sort, direction);
    }
}
