package com.haradan.domain.controller;

import com.haradan.common.enumeration.CategoryType;
import com.haradan.common.enumeration.LookupType;
import com.haradan.common.controller.AbstractController;
import com.haradan.common.controller.response.LookupDto;
import com.haradan.domain.controller.response.LookupDataResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/lookup")
public class LookUpDataController extends AbstractController {

	@GetMapping("/{lookup}")
	public ResponseEntity<LookupDataResponse> detail(@PathVariable String lookup, @RequestParam(name = "id") Integer id) {
		LookupDataResponse lookupDataResponse = new LookupDataResponse();
		if (LookupType.category.toString().equals(lookup)) {
			lookupDataResponse.setLookupDtos(getCategoryLookup(id));
		}
		return new ResponseEntity<>(lookupDataResponse, HttpStatus.OK);
	}

	private List<LookupDto> getCategoryLookup(Integer id) {
		List<CategoryType> categoryTypes = id == null ? CategoryType.getMainCategories() : CategoryType.getSubCategories(id);
		return categoryTypes
				.stream()
				.map(c -> {
					return new LookupDto(c.getId().toString(), c.getName());
				}).collect(Collectors.toList());
	}
}
