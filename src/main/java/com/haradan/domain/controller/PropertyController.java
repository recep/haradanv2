package com.haradan.domain.controller;

import com.haradan.common.enumeration.property.AgeType;
import com.haradan.common.enumeration.property.DonuType;
import com.haradan.common.enumeration.property.GenderType;
import com.haradan.common.enumeration.property.RaceType;
import com.haradan.common.exception.exceptions.MicroException;
import com.haradan.common.controller.AbstractController;
import com.haradan.common.controller.response.LookupDto;
import com.haradan.domain.controller.request.PropertyRequest;
import com.haradan.domain.controller.response.PropertyResponse;
import com.haradan.domain.mapper.PropertyMapper;
import com.haradan.domain.service.PropertyService;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/properties")
@AllArgsConstructor
public class PropertyController extends AbstractController {
    private PropertyService propertyService;
    private PropertyMapper propertyMapper;

    @GetMapping("/{id}")
    public PropertyResponse get(@PathVariable String id) throws MicroException {
        return propertyMapper.toResponse(propertyService.get(id));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public void put(@PathVariable String id, @RequestBody @Validated PropertyRequest propertyRequest) {
        propertyService.put(id, propertyMapper.toEntity(propertyRequest));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public void delete(@PathVariable String id) {
        propertyService.delete(id);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public void save(@RequestBody @Validated PropertyRequest propertyRequest) {
        propertyService.save(propertyMapper.toEntity(propertyRequest));
    }

    @GetMapping("/lookup/{key}")
    public List<LookupDto> getLookup(@PathVariable String key) throws MicroException {
        switch (key) {
            case "at-irki":
                return RaceType.getLookup();
            case "yas":
                return AgeType.getLookup();
            case "cinsiyet":
                return GenderType.getLookup();
            case "donu":
                return DonuType.getLookup();
            default:
                return null;
        }
    }
}
