package com.haradan.domain.controller.response;

import com.haradan.common.enumeration.EntityStatus;
import com.haradan.common.controller.response.BaseResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class CategoryResponse extends BaseResponse {
    private static final long serialVersionUID = -6795494548799475942L;
    private EntityStatus status;
    private String name;
    private String searchText;
    private Double price;
    private Long orderId;
    private String parentId;
    private List<CategoryResponse> children;
}
