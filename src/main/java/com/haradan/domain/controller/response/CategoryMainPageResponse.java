package com.haradan.domain.controller.response;

import com.haradan.common.controller.response.BaseResponse;
import com.haradan.common.controller.response.LookupDto;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class CategoryMainPageResponse extends BaseResponse {
    private List<LookupDto> categories;
}
