package com.haradan.domain.controller.response;

import com.haradan.common.controller.response.BaseResponse;
import com.haradan.common.enumeration.ChannelType;
import com.haradan.domain.dto.SettingDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class UserDetailResponse extends BaseResponse {
    private static final long serialVersionUID = -6795494548799475942L;
    private ChannelType channel;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private SettingDto setting;
    private String resetPasswordKey;
}
