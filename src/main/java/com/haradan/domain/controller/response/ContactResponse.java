package com.haradan.domain.controller.response;

import com.haradan.common.controller.response.BaseResponse;
import com.haradan.common.enumeration.EntityStatus;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class ContactResponse extends BaseResponse {
    private static final long serialVersionUID = -6795494548799475942L;
    private EntityStatus status;
    private String name;
    private String email;
    private String phoneNumber;
    private String message;
}
