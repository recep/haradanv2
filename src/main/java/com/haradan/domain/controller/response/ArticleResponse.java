package com.haradan.domain.controller.response;

import java.time.LocalDate;

import com.haradan.common.controller.response.BaseResponse;
import com.haradan.common.enumeration.ArticleType;
import com.haradan.common.enumeration.EntityStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ArticleResponse extends BaseResponse {
    private static final long serialVersionUID = -6795494548799475942L;
    private LocalDate createDate;
    private EntityStatus status;
    private ArticleType type;
    private String title;
    private String summary;
    private String article;
    private String searchText;
    private String media;
    private Long orderId;
}
