package com.haradan.domain.controller.response;

import com.haradan.common.controller.response.BaseResponse;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class StableResponse extends BaseResponse {
    private static final long serialVersionUID = 3809104814337239544L;
    private String name;
    private String phoneNumber;
    private String email;
    private String webpage;
    private String contactName;
    private String address;
    private String note;
}
