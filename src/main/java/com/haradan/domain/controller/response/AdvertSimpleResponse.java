package com.haradan.domain.controller.response;

import com.haradan.common.controller.response.BaseResponse;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.domain.dto.CategoryDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class AdvertSimpleResponse extends BaseResponse {
    private static final long serialVersionUID = -6795494548799475942L;
    private Long advertNo;
    private EntityStatus status;
    private String title;
    private BigDecimal price;
    private String city;
    private String district;
    private String url;
    private String imageUrl;
}
