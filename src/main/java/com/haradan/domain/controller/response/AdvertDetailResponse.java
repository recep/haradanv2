package com.haradan.domain.controller.response;

import com.haradan.common.controller.response.BaseResponse;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.domain.dto.AdvertPropertyDto;
import com.haradan.domain.dto.CategoryDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@Data
@EqualsAndHashCode(callSuper = false)
public class AdvertDetailResponse extends BaseResponse {
    private static final long serialVersionUID = -6795494548799475942L;
    private Long advertNo;
    private EntityStatus status;
    private CategoryDto category;
    private LocalDate createDate;
    private String title;
    private String description;
    private BigDecimal price;
    private String city;
    private String district;
    private String url;
    private String imageUrl;
    private String location;
	private Long tjkNo;
	private boolean charged;
    private UserDetailResponse user;
    private Map<String, List<String>> mediaMap;
    private List<AdvertPropertyDto> advertPropertyList;
    private List<DopingResponse> dopings;
}
