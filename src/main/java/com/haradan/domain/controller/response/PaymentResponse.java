package com.haradan.domain.controller.response;

import java.time.ZonedDateTime;

import org.hibernate.annotations.Type;

import com.haradan.common.controller.response.BaseResponse;
import com.haradan.common.enumeration.EntityStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class PaymentResponse extends BaseResponse {
	private static final long serialVersionUID = -6795494548799475942L;
	private ZonedDateTime createdDate;
	private EntityStatus status;
	private String merchantId;
	private String productId;
	private String productName;
	private String paymentAmount;
	private String userName;
	private String userEmail;
	private String userAddress;
	private String userPhone;
	private String ip;
	private String tokenRequest;
	private String tokenResponse;
	private String notifyRequest;
	private String notifyResponse;
}
