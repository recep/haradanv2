package com.haradan.domain.controller.response;

import com.haradan.common.controller.response.LookupDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class LookupDataResponse implements Serializable {
	private static final long serialVersionUID = -6795494548799475942L;
	private List<LookupDto> lookupDtos = new ArrayList<>();
}
