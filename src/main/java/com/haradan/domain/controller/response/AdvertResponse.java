package com.haradan.domain.controller.response;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import com.haradan.common.controller.response.BaseResponse;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.domain.dto.CategoryDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class AdvertResponse extends BaseResponse {
    private static final long serialVersionUID = -6795494548799475942L;
    private Long advertNo;
    private EntityStatus status;
    private CategoryDto category;
    private LocalDate createDate;
    private String title;
    private String description;
    private BigDecimal price;
    private String city;
    private String district;
    private String url;
    private String imageUrl;
    private Integer viewCount;
    private List<DopingResponse> dopings;
    private UserResponse user;
	private boolean charged;
}
