package com.haradan.domain.controller.response;

import com.haradan.common.controller.response.BaseResponse;
import com.haradan.common.controller.response.LookupDto;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.common.enumeration.PropertyType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class PropertyResponse extends BaseResponse {
    private static final long serialVersionUID = -6795494548799475942L;
    private EntityStatus status;
    private PropertyType type;
    private String name;
    private String searchText;
    private Boolean searchParam;
    private Boolean mandatory;
    private Long orderId;
    private String parentId;
    private String categoryId;
    private List<LookupDto> lookupData;
}
