package com.haradan.domain.controller.response;

import java.time.LocalDate;

import com.haradan.common.controller.response.BaseResponse;
import com.haradan.common.enumeration.BannerType;
import com.haradan.common.enumeration.EntityStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class BannerResponse extends BaseResponse {
    private static final long serialVersionUID = -6795494548799475942L;
    private EntityStatus status;
    private BannerType type;
    private String name;
    private String searchText;
    private Long price;
    private Long orderId;
    private String url;
    private String media;
    private String description;
    private LocalDate startDate;
    private LocalDate endDate;
    private LocalDate createDate;
}
