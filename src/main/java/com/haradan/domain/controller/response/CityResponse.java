package com.haradan.domain.controller.response;

import com.haradan.common.controller.response.BaseResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class CityResponse extends BaseResponse {
    private static final long serialVersionUID = -6795494548799475942L;
    private String countryCode;
    private String code;
    private String name;
}
