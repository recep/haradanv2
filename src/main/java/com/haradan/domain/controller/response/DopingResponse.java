package com.haradan.domain.controller.response;

import com.haradan.common.controller.response.BaseResponse;
import com.haradan.common.enumeration.EntityStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.ZonedDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class DopingResponse extends BaseResponse {
    private static final long serialVersionUID = -6795494548799475942L;
    private String name;
    private String searchText;
    private String desc;
    private String image;
    private String shortName;
    private Integer price;
    private EntityStatus status;
    private ZonedDateTime lastModifiedDate;
    private LocalDate approveDate;
    private LocalDate expireDate;
}

