package com.haradan.domain.controller;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.haradan.common.controller.AbstractController;
import com.haradan.common.exception.exceptions.MicroException;
import com.haradan.domain.controller.request.ContactRequest;
import com.haradan.domain.controller.response.ContactResponse;
import com.haradan.domain.mapper.ContactMapper;
import com.haradan.domain.service.ContactService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping(value = "/contacts")
@AllArgsConstructor
public class ContactController extends AbstractController {
    private ContactService contactService;
    private ContactMapper contactMapper;

    @GetMapping("/{id}")
    public ContactResponse get(@PathVariable String id) throws MicroException {
        return contactMapper.toResponse(contactService.get(id));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public void delete(@PathVariable String id) {
        contactService.delete(id);
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void save(@RequestBody @Validated ContactRequest contactRequest) {
        contactService.save(contactMapper.toEntity(contactRequest));
    }
}
