package com.haradan.domain.controller;

import com.haradan.common.controller.AbstractController;
import com.haradan.common.controller.query.QueryController;
import com.haradan.common.dao.repository.CustomPagedResource;
import com.haradan.common.dao.repository.CustomPagedResourceConverter;
import com.haradan.common.enumeration.ArticleType;
import com.haradan.common.exception.exceptions.MicroException;
import com.haradan.domain.controller.request.AdvertRequest;
import com.haradan.domain.controller.request.ArticleRequest;
import com.haradan.domain.controller.response.AdvertResponse;
import com.haradan.domain.controller.response.ArticleResponse;
import com.haradan.domain.dao.entity.Advert;
import com.haradan.domain.dao.entity.Article;
import com.haradan.domain.dao.repository.AdvertRepository;
import com.haradan.domain.dao.repository.ArticleRepository;
import com.haradan.domain.mapper.AdvertMapper;
import com.haradan.domain.mapper.ArticleMapper;
import com.haradan.domain.service.ArticleService;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/articles")
@AllArgsConstructor
public class ArticleController extends QueryController<Article, String, ArticleRequest, ArticleResponse> {
    @Getter
    private ArticleRepository repository;
    @Getter
    private ArticleMapper mapper;
    private ArticleService articleService;
    private ArticleMapper articleMapper;

    @GetMapping("/{id}")
    public ArticleResponse get(@PathVariable String id) throws MicroException {
        return articleMapper.toResponse(articleService.get(id));
    }

    @PutMapping("/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public void put(@PathVariable String id, @RequestBody @Validated ArticleRequest articleRequest) {
        articleService.put(id, articleMapper.toEntity(articleRequest));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public void delete(@PathVariable String id) {
        articleService.delete(id);
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public void save(@RequestBody @Validated ArticleRequest articleRequest) {
        articleService.save(articleMapper.toEntity(articleRequest));
    }

    @GetMapping("/search")
    public CustomPagedResource<List<ArticleResponse>> pageable(@RequestParam("type") ArticleType type,
                                                              @RequestParam("size") int size,
                                                              @RequestParam(defaultValue = "0", required = false) int page,
                                                              @RequestParam(defaultValue = "orderId", required = false) String sort,
                                                              @RequestParam(defaultValue = "ASC", required = false) String direction) {
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.valueOf(direction), sort);
        Page<ArticleResponse> map = articleService.search(type, pageable).map(m -> articleMapper.toResponse(m));
        return CustomPagedResourceConverter.map(map, sort, direction);
    }
}
