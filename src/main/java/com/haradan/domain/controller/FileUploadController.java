package com.haradan.domain.controller;

import com.haradan.common.util.Constants;
import com.haradan.common.util.Utils;
import com.haradan.domain.dao.repository.AdvertRepository;
import com.haradan.domain.service.AdvertService;
import com.haradan.domain.service.file.upload.FileOperationService;
import com.haradan.domain.service.file.upload.FileUploadFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/files")
public class FileUploadController {

    private final AdvertService advertService;
    private final AdvertRepository advertRepository;
    private final FileUploadFactory fileUploadService;
    private final FileOperationService fileOperationService;

    @PostMapping("/{id}/upload")
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void uploadFiles(@PathVariable(name = "id") String id,
                            @RequestParam("files") MultipartFile[] files,
                            @RequestParam(defaultValue = "advert") String entity) {
        Map<String, MultipartFile> fileMap = Arrays.asList(files)
                .stream()
                .collect(Collectors.toMap(f -> Utils.uniqueFileName(f.getOriginalFilename()), f -> f));
        String media = fileMap.keySet()
                .stream()
                .peek(key -> fileUploadService.of(entity).upload(id, fileMap.get(key), key))
                .collect(Collectors.joining(Constants.comma));
        fileUploadService.of(entity).save(id, media);
    }

    @GetMapping("/{id}/list")
    public Map<String, String> getFiles(@PathVariable String id,
                                        @RequestParam(defaultValue = "advert") String entity) {
        return fileUploadService.of(entity).getFileMap(id);
    }


    @PostMapping("/{id}/change")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void changeDefaultImage(@PathVariable String id,
                                   @RequestParam String fileName,
                                   @RequestParam(defaultValue = "advert") String entity) {
        fileUploadService.of(entity).changeDefaultImage(id, fileName);
    }

    @PostMapping("/{id}/delete")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void deleteImage(@PathVariable String id,
                            @RequestParam String fileName,
                            @RequestParam(defaultValue = "advert") String entity) {
        fileUploadService.of(entity).deleteImage(id, fileName);
    }

}