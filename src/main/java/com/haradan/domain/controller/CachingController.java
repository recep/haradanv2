package com.haradan.domain.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.cache.CacheManager;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cache")
@RequiredArgsConstructor
public class CachingController {

    private final CacheManager cacheManager;

    @DeleteMapping
    public void evictAllCaches() {
        cacheManager.getCacheNames().stream()
                .forEach(cacheName -> cacheManager.getCache(cacheName).clear());
    }

    @DeleteMapping("{cache}")
    public void evictAllCacheValues(@PathVariable String cache) {
        cacheManager.getCache(cache).clear();
    }

    @DeleteMapping("{cache}/{key}")
    public void evictSingleCacheValue(@PathVariable String cache, @PathVariable String key) {
        cacheManager.getCache(cache).evict(key);
    }
}