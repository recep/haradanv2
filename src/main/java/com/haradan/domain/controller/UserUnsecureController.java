package com.haradan.domain.controller;

import com.haradan.common.controller.AbstractController;
import com.haradan.common.enumeration.ChannelType;
import com.haradan.domain.controller.request.CreateNewPasswordRequest;
import com.haradan.domain.controller.request.PasswordRequest;
import com.haradan.domain.controller.request.UserRequest;
import com.haradan.domain.controller.response.UserResponse;
import com.haradan.domain.dao.entity.User;
import com.haradan.domain.dto.NotificationDto;
import com.haradan.domain.mapper.UserMapper;
import com.haradan.domain.service.AdvertService;
import com.haradan.domain.service.UserService;
import com.haradan.domain.service.notification.NotificationService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import static com.haradan.common.enumeration.NotificationType.REGISTRATION;

@RestController
@RequestMapping(value = "/user")
@AllArgsConstructor
public class UserUnsecureController extends AbstractController {
    private UserMapper userMapper;
    private UserService userService;
    private AdvertService advertService;
    private NotificationService notificationService;

    @PostMapping
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void save(@RequestBody @Validated UserRequest userRequest) {
        User user = userMapper.toEntity(userRequest);
        userService.save(user);
        notificationService.notify(NotificationDto.builder().type(REGISTRATION).entity(user).build());
    }

    @PostMapping("/social-login")
    public UserResponse socialLogin(@RequestParam(value = "fullName") String fullName,
                                    @RequestParam(value = "email") String email,
                                    @RequestParam(value = "channel") ChannelType channel) {

        String password = userService.socialLogin(email, fullName, channel);
        return UserResponse.builder().email(email).password(password).build();
    }

    @PutMapping("/{id}/create-new-password")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void createNewPassword(@PathVariable String id, @RequestBody @Validated CreateNewPasswordRequest passwordRequest) {
        userService.createNewPassword(id, passwordRequest);
    }

    @PostMapping("/{email}/send-forgot-password-link")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void sendForgotPasswordLink(@PathVariable String email) {
        userService.sendForgotPasswordLink(email);
    }

    @PutMapping("/encrypt")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void encryptPassword(@RequestBody @Validated PasswordRequest passwordRequest) {
        userService.encryptPassword(passwordRequest);
    }

    @GetMapping("/{advertId}/favourite-count")
    public int getFavouriteCount(@PathVariable String advertId) {
        return advertService.getFavouriteCount(advertId);
    }
}
