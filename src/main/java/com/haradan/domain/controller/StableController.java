package com.haradan.domain.controller;

import com.haradan.common.controller.query.QueryController;
import com.haradan.common.dao.repository.CustomPagedResource;
import com.haradan.common.dao.repository.CustomPagedResourceConverter;
import com.haradan.common.exception.exceptions.MicroException;
import com.haradan.domain.controller.request.StableRequest;
import com.haradan.domain.controller.response.StableResponse;
import com.haradan.domain.dao.entity.Stable;
import com.haradan.domain.dao.repository.StableRepository;
import com.haradan.domain.dto.StableSearchDto;
import com.haradan.domain.mapper.StableMapper;
import com.haradan.domain.service.StableService;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/stables")
@AllArgsConstructor
public class StableController extends QueryController<Stable, String, StableRequest, StableResponse> {

    @Getter
    private StableMapper mapper;

    @Getter
    private final StableRepository repository;

    private StableService stableService;

    @GetMapping("/{id}")
    public StableResponse get(@PathVariable String id) throws MicroException {
        return mapper.toResponse(stableService.get(id));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public void delete(@PathVariable String id) {
        stableService.delete(id);
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public void save(@RequestBody @Validated StableRequest stableRequest) {
        stableService.save(mapper.toEntity(stableRequest));
    }

    @PutMapping("/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void put(@PathVariable String id, @RequestBody @Validated StableRequest stableRequest) {
        stableService.put(id, mapper.toEntity(stableRequest));
    }

    @GetMapping("/search")
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public CustomPagedResource<List<StableResponse>> pageable(@RequestParam("page") int page,
                                                              @RequestParam("size") int size,
                                                              @RequestParam(defaultValue = "name", required = false) String sort,
                                                              @RequestParam(required = false, defaultValue = "ASC") String direction,
                                                              @RequestParam(name = "name", required = false) String name) {
        StableSearchDto searchDto = StableSearchDto.builder().name(name).build();

        Pageable pageable = PageRequest.of(page, size, Sort.Direction.valueOf(direction), sort);
        Page<StableResponse> map = stableService.search(searchDto, pageable).map(m -> mapper.toResponse(m));
        return CustomPagedResourceConverter.map(map, sort, direction);
    }
}
