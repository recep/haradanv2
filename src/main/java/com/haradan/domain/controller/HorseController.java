package com.haradan.domain.controller;

import com.haradan.common.controller.AbstractController;
import com.haradan.common.enumeration.horse.DonuType;
import com.haradan.common.enumeration.horse.GenderType;
import com.haradan.domain.dao.entity.Advert;
import com.haradan.domain.dao.entity.horse.Horse;
import com.haradan.domain.dao.entity.horse.HorseSummary;
import com.haradan.domain.dto.horse.HorseDto;
import com.haradan.domain.dto.horse.HorseSummaryDto;
import com.haradan.domain.dto.horse.Pedigri;
import com.haradan.domain.mapper.HorseMapper;
import com.haradan.domain.mapper.HorseSummaryMapper;
import com.haradan.domain.service.AdvertService;
import com.haradan.domain.service.HorseService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.haradan.common.util.Utils.mapper;

@Slf4j
@RestController
@RequestMapping(value = "/horses")
@RequiredArgsConstructor
public class HorseController extends AbstractController {
    private final HorseService horseService;
    private final HorseMapper horseMapper;
    private final HorseSummaryMapper horseSummaryMapper;
    private final AdvertService advertService;

    @PostMapping("/{name}/list")
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public List<HorseDto> getHorseInfo(@PathVariable String name) throws Exception {
        if (StringUtils.isNotEmpty(name)) { // at bilgilerini ekleme
            Horse horse = new Horse();
            horse.setName(URLDecoder.decode(name, "UTF-8"));
            List<HorseSummary> horseInfoList = horseService.getHorsesSummary(horse);
            return horseSummaryMapper.toDto(horseInfoList);
        }
        return new ArrayList<>();
    }

    @PostMapping("/{tjkNo}")
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public HorseDto getHorseDetail(@PathVariable Long tjkNo) throws Exception {
        Horse horse = horseService.getHorseDetail(tjkNo);
        horseService.saveHorseDetail(horse);
        horseService.saveHorseAllData(tjkNo);
        HorseDto horseDto = horseMapper.toHorse(horse);
        horseDto.setDonu(DonuType.getDesc(horse.getDonu()));
        horseDto.setGender(GenderType.getGender(horse.getGender()));
        return horseDto;
    }

    @PostMapping("/summary/{pageNumber}")
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public List<HorseDto> getHorsesSummary(@PathVariable Integer pageNumber) throws Exception {
        HorseSummaryDto horseSummary = horseService.getHorsesSummary(pageNumber);
        horseService.saveHorsesSummary(horseSummary.getHorseSummaries());
        return horseSummaryMapper.toDto(horseSummary.getHorseSummaries());
    }

    @GetMapping("advert/{id}")
    public HorseDto getAdvertHorseDetail(@PathVariable String id) {
        Advert advert = advertService.get(id);
        try {
            Horse horse = horseService.getHorseDetailFromDB(advert.getTjkNo());
            HorseDto horseDto = horseMapper.toHorse(horse);
            horseDto.setPedigri(Arrays.asList(mapper.readValue(horse.getHorsePedigri().getPedigri(), Pedigri[].class)));
            return horseDto;
        } catch (Exception e) {
            log.error("At detay bilgileri getirilirken hata oluştu.", e);
            return null;
        }
    }
}
