package com.haradan.domain.controller;

import com.haradan.common.controller.query.QueryController;
import com.haradan.common.dao.repository.CustomPagedResource;
import com.haradan.common.dao.repository.CustomPagedResourceConverter;
import com.haradan.common.enumeration.DopingType;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.common.enumeration.SpecialPathType;
import com.haradan.common.exception.exceptions.MicroException;
import com.haradan.domain.controller.request.AdvertDopingRequest;
import com.haradan.domain.controller.request.AdvertPropertyRequest;
import com.haradan.domain.controller.request.AdvertRequest;
import com.haradan.domain.controller.response.*;
import com.haradan.domain.dao.entity.Advert;
import com.haradan.domain.dao.entity.AdvertProperty;
import com.haradan.domain.dao.entity.Doping;
import com.haradan.domain.dao.repository.AdvertRepository;
import com.haradan.domain.dto.SearchDto;
import com.haradan.domain.mapper.AdvertMapper;
import com.haradan.domain.service.AdvertService;
import com.haradan.domain.service.PropertyService;
import com.haradan.domain.service.file.upload.impl.AdvertFileService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.haradan.common.util.Utils.getProperties;

@RestController
@RequestMapping(value = "/adverts")
@RequiredArgsConstructor
public class AdvertController extends QueryController<Advert, String, AdvertRequest, AdvertResponse> {

    @Getter
    private AdvertRepository repository;
    @Getter
    private AdvertMapper mapper;
    private final AdvertService advertService;
    private final PropertyService propertyService;
    private final AdvertFileService advertFileService;

    @Transactional
    @GetMapping("/{id}")
    public AdvertDetailResponse get(@PathVariable String id) throws MicroException {
        Advert advert = advertService.get(id);
        AdvertDetailResponse advertDetailResponse = mapper.toDetailResponse(advert);
        advertDetailResponse.setAdvertPropertyList(mapper.toPropertyDtos(advertService.getAdvertProperties(advert)));
        advertDetailResponse.setDopings(mapper.toDopingDtos(advertService.getDopings(advert.getIdentifier())));
        advertDetailResponse.setMediaMap(advertFileService.createAdvertMediaMap(advert));
        return advertDetailResponse;
    }

    @PutMapping("/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void put(@PathVariable String id, @RequestBody @Validated AdvertRequest advertRequest) {
        advertService.put(id, mapper.toEntity(advertRequest));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void delete(@PathVariable String id) {
        advertService.delete(id);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public AdvertResponse save(@RequestBody @Validated AdvertRequest advertRequest) {
        Advert advert = advertService.save(mapper.toEntity(advertRequest));
        return mapper.toResponse(advert);
    }

    @PutMapping("/{id}/status/{status}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void updateStatus(@PathVariable String id, @PathVariable EntityStatus status) {
        advertService.updateStatus(id, status);
    }

    @PutMapping("/{id}/category/{categoryId}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void updateCategory(@PathVariable String id, @PathVariable String categoryId) {
        advertService.updateCategory(id, categoryId);
    }

    @PostMapping("/{id}/property")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void saveProperty(@PathVariable String id, @RequestBody List<AdvertPropertyRequest> request) {
        List<AdvertProperty> properties = mapper.toAdvertProperties(request);
        properties.forEach(p -> p.setSearchText(propertyService.get(p.getProperty().getIdentifier()).getSearchText()));
        advertService.saveProperty(id, properties);
    }

    @PostMapping("/{id}/doping")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void saveDoping(@PathVariable String id, @RequestBody List<AdvertDopingRequest> request) {
        List<Doping> dopings = mapper.toDopings(request);
        advertService.saveDoping(id, dopings);
    }

    @PutMapping("/{id}/doping")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public void approveDoping(@PathVariable String id, @RequestBody List<AdvertDopingRequest> request) {
        List<Doping> dopings = mapper.toDopings(request);
        advertService.updateDopingStatusAsync(id, dopings);
    }

    @GetMapping("/dopings")
    public List<DopingResponse> getDopings() {
        return mapper.toDopingDtos(advertService.getDopings());
    }

    @GetMapping("/{id}/doping")
    public List<DopingResponse> getAdvertDoping(@PathVariable String id) {
        return mapper.toDopingDtos(advertService.getDopings(id));
    }

    @GetMapping("/{id}/similar-adverts")
    public List<AdvertSimpleResponse> getSimilarAdverts(@PathVariable String id) {
        Advert advert = advertService.get(id);
        return advertService.getSimilarAdverts(advert).stream()
                .map(mapper::toSimpleResponse)
                .collect(Collectors.toList());
    }

    @GetMapping("/search/{categorySearchText}")
    @Transactional
    public CustomPagedResource<List<AdvertSimpleResponse>> pageable(@PathVariable(required = false) String categorySearchText,
                                                                    @RequestParam("page") int page,
                                                                    @RequestParam("size") int size,
                                                                    @RequestParam(defaultValue = "createdDate", required = false) String sort,
                                                                    @RequestParam(defaultValue = "DESC", required = false) String direction,
                                                                    @RequestParam(required = false) String id,
                                                                    @RequestParam(required = false) Long advertNo,
                                                                    @RequestParam(required = false) String searchText,
                                                                    @RequestParam(required = false) EntityStatus status,
                                                                    @RequestParam(name = "date", required = false) Integer dateInterval,
                                                                    @RequestParam(required = false) Boolean hasPhoto,
                                                                    @RequestParam(required = false) Double minPrice,
                                                                    @RequestParam(required = false) Double maxPrice,
                                                                    @RequestParam(required = false) String city,
                                                                    @RequestParam(required = false) String district,
                                                                    @RequestParam(required = false) String userId,
                                                                    @RequestParam(required = false) Map<String, String> params) {
        SearchDto searchDto = getProperties(params);
        searchDto.setCategorySearchText(SpecialPathType.of(categorySearchText) != null ? SpecialPathType.of(categorySearchText).getCategorySearchText() : categorySearchText);
        searchDto.setId(id);
        searchDto.setAdvertNo(advertNo);
        searchDto.setSearchText(searchText);
        searchDto.setStatus(EntityStatus.ACTIVE);
        searchDto.setDateInterval(dateInterval);
        searchDto.setHasPhoto(hasPhoto);
        searchDto.setMinPrice(minPrice);
        searchDto.setMaxPrice(maxPrice);
        searchDto.setCity(city);
        searchDto.setDistrict(district);
        searchDto.setUserId(userId);

        Pageable pageable = PageRequest.of(page, size, Sort.Direction.valueOf(direction), sort);
        Page<AdvertSimpleResponse> map = advertService.search(searchDto, pageable).map(m -> mapper.toSimpleResponse(m));

        return CustomPagedResourceConverter.map(map, sort, direction);
    }

    @Transactional
    @GetMapping("/doping")
    public CustomPagedResource<List<AdvertSimpleResponse>> pageableByDoping(@RequestParam(defaultValue = "0", required = false) int page,
                                                                            @RequestParam(defaultValue = "20", required = false) int size,
                                                                            @RequestParam(defaultValue = "createdDate", required = false) String sort,
                                                                            @RequestParam(defaultValue = "DESC", required = false) String direction,
                                                                            @RequestParam("doping") DopingType doping) {
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.valueOf(direction), sort);
        Page<AdvertSimpleResponse> map = advertService.searchDoping(doping, pageable).map(m -> mapper.toSimpleResponse(m));
        return CustomPagedResourceConverter.map(map, sort, direction);
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PostMapping("/{advertId}/{userId}/add-to-favourites")
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void addToFavourites(@PathVariable String advertId, @PathVariable String userId) {
        advertService.addToFavourites(advertId, userId);
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PostMapping("/{advertId}/{userId}/remove-from-favourites")
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void removeFromFavourites(@PathVariable String advertId, @PathVariable String userId) {
        advertService.removeFromFavourites(advertId, userId);
    }

}