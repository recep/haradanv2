package com.haradan.domain.controller;

import com.haradan.common.controller.AbstractController;
import com.haradan.common.exception.Exceptions;
import com.haradan.common.exception.exceptions.MicroException;
import com.haradan.common.util.Constants;
import com.haradan.domain.dao.entity.Advert;
import com.haradan.domain.dao.repository.AdvertRepository;
import com.haradan.domain.dao.repository.SmsDataRepository;
import com.haradan.domain.service.UserService;
import com.haradan.domain.service.notification.SmsSenderService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@RestController
@RequestMapping(value = "/process")
@AllArgsConstructor
public class ProcessController extends AbstractController {
    private AdvertRepository advertRepository;
    private UserService userService;
    private SmsSenderService smsSenderService;
    private SmsDataRepository smsDataRepository;

    @ApiOperation(value = "Update User or date, date format (2021-03-20)")
    @PutMapping("/advert/{advertNo}")
    public void put(@PathVariable Long advertNo,
                    @RequestParam(required = false) String userId,
                    @RequestParam(required = false) String date,
                    @RequestHeader String key) {
        if (!Constants.DEFAULT_PASSWORD.equals(key)) {
            throw new MicroException(Exceptions.RESET_PASSWORD_KEY_NOT_MATCH);
        }
        Advert advert = advertRepository.findByAdvertNo(advertNo).orElseThrow(() ->
                MicroException.newDataNotFoundException(Advert.class.getSimpleName(), advertNo.toString()));

        advert.setUser(StringUtils.isEmpty(userId) ? advert.getUser() : userService.get(userId));

        advert.setCreatedDate(StringUtils.isEmpty(date) ? advert.getCreatedDate() :
                ZonedDateTime.of(LocalDate.parse(date), LocalTime.now(), ZoneId.systemDefault()));

        advertRepository.save(advert);
    }


    @ApiOperation(value = "send SMS")
    @PostMapping("/sms/send")
    public void sendSms(@RequestHeader String key) {
        if (!Constants.DEFAULT_PASSWORD.equals(key)) {
            throw new MicroException(Exceptions.RESET_PASSWORD_KEY_NOT_MATCH);
        }
        smsSenderService.sendSms();
    }

    @ApiOperation(value = "prepare Sms data")
    @PostMapping("/sms/prepare")
    public void obtainSmsData(@RequestHeader String key) {
        if (!Constants.DEFAULT_PASSWORD.equals(key)) {
            throw new MicroException(Exceptions.RESET_PASSWORD_KEY_NOT_MATCH);
        }
        // get data from user -advert
        // smsDataRepository.save();
    }
}
