package com.haradan.domain.controller.request;

import com.haradan.common.enumeration.EntityStatus;
import com.haradan.common.enumeration.PropertyType;
import com.haradan.common.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class PropertyRequest extends BaseRequest {
	private static final long serialVersionUID = 3809104814337239544L;
	private EntityStatus status;
	private PropertyType type;
	private String name;
	private Boolean searchParam;
	private Boolean mandatory;
	private Long orderId;
	private String parentId;
	private String categoryId;
}
