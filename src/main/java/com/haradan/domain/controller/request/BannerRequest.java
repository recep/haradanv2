package com.haradan.domain.controller.request;

import com.haradan.common.enumeration.BannerType;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.common.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;

@Data
@EqualsAndHashCode(callSuper = false)
public class BannerRequest extends BaseRequest {
    private static final long serialVersionUID = 3809104814337239544L;
    private EntityStatus status;
    private BannerType type;
    private String name;
    private Long price;
    private Long orderId;
    private String url;
    private String media;
    private String description;
    private LocalDate startDate;
    private LocalDate endDate;
}
