package com.haradan.domain.controller.request;

import com.haradan.common.controller.request.BaseRequest;
import com.haradan.common.enumeration.EntityStatus;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = false)
public class AdvertRequest extends BaseRequest {
	private static final long serialVersionUID = 3809104814337239544L;
	@NotNull
	private String category;
	private EntityStatus status;
	private String title;
	private String description;
	private BigDecimal price;
	private String city;
	private String district;
	private String mediaFolder;
	private Long tjkNo;
}
