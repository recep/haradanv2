package com.haradan.domain.controller.request;

import com.haradan.common.controller.request.BaseRequest;
import com.haradan.common.enumeration.EntityStatus;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class ContactRequest extends BaseRequest {
    private static final long serialVersionUID = 3809104814337239544L;
    private EntityStatus status;
    private String name;
    private String email;
    private String phoneNumber;
    private String message;
}
