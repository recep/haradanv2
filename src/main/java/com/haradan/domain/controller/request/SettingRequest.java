package com.haradan.domain.controller.request;

import com.haradan.common.controller.request.BaseRequest;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class SettingRequest extends BaseRequest {
    private static final long serialVersionUID = 3809104814337239544L;
    private boolean showPhone;
    private boolean showEmail;
    private boolean notification;
    private boolean campaignNotification;
}
