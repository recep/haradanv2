package com.haradan.domain.controller.request;

import javax.validation.constraints.NotNull;

import com.haradan.common.controller.request.BaseRequest;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateNewPasswordRequest extends BaseRequest {
    private static final long serialVersionUID = 3809104814337239544L;
    @NotNull
    private String resetPasswordKey;
    @NotNull
    private String newPassword;
}
