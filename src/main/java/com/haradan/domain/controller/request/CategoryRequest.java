package com.haradan.domain.controller.request;

import com.haradan.common.enumeration.EntityStatus;
import com.haradan.common.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class CategoryRequest extends BaseRequest {
    private static final long serialVersionUID = 3809104814337239544L;
    private EntityStatus status;
    private String name;
    private Double price;
    private Long orderId;
    private String parentId;

}
