package com.haradan.domain.controller.request;

import com.haradan.common.enumeration.ArticleType;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.common.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class ArticleRequest extends BaseRequest {
    private static final long serialVersionUID = 3809104814337239544L;
    private EntityStatus status;
    private ArticleType type;
    private String title;
    private String summary;
    private String article;
    private String media;
    private Long orderId;
}
