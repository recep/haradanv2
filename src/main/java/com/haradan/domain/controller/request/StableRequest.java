package com.haradan.domain.controller.request;

import com.haradan.common.controller.request.BaseRequest;
import com.haradan.common.enumeration.EntityStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class StableRequest extends BaseRequest {
    private static final long serialVersionUID = 3809104814337239544L;
    private String name;
    private String phoneNumber;
    private String email;
    private String webpage;
    private String contactName;
    private String address;
    private String note;
}
