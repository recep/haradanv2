package com.haradan.domain.controller;

import com.haradan.common.controller.query.QueryController;
import com.haradan.common.dao.repository.CustomPagedResource;
import com.haradan.common.dao.repository.CustomPagedResourceConverter;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.domain.controller.request.PasswordRequest;
import com.haradan.domain.controller.request.SettingRequest;
import com.haradan.domain.controller.request.UserRequest;
import com.haradan.domain.controller.response.UserDetailResponse;
import com.haradan.domain.controller.response.UserResponse;
import com.haradan.domain.dao.entity.User;
import com.haradan.domain.dao.repository.UserRepository;
import com.haradan.domain.dto.NotificationDto;
import com.haradan.domain.mapper.UserMapper;
import com.haradan.domain.service.SecurityService;
import com.haradan.domain.service.UserService;
import com.haradan.domain.service.notification.NotificationService;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.haradan.common.enumeration.NotificationType.REGISTRATION;

@RestController
@RequestMapping(value = "/users")
@AllArgsConstructor
public class UserController extends QueryController<User, String, UserRequest, UserResponse> {

    @Getter
    private UserRepository repository;
    @Getter
    private UserMapper mapper;
    private UserService userService;
    private SecurityService securityService;
    private NotificationService notificationService;

    @GetMapping("/user-info")
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public UserResponse userInfo() {
        return mapper.toResponse(securityService.getLoginUser());
    }

    @Transactional
    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public UserDetailResponse get(@PathVariable String id) {
        return mapper.toDetailResponse(userService.get(id));
    }

    @PutMapping("/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void put(@PathVariable String id, @RequestBody @Validated UserRequest userRequest) {
        boolean setAsAdmin = "on".equals(userRequest.getAdmin());
        userService.put(id, mapper.toEntity(userRequest), setAsAdmin);
    }

    @PutMapping("/{id}/setting")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void putSetting(@PathVariable String id, @RequestBody @Validated SettingRequest settingRequest) {
        userService.putSetting(id, mapper.toEntity(settingRequest));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void delete(@PathVariable String id) {
        User user = userService.get(id);
        user.setStatus(EntityStatus.PASSIVE);
        repository.save(user);
    }

    @PostMapping("/{id}/activate")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public void activate(@PathVariable String id) {
        User user = userService.get(id);
        user.setStatus(EntityStatus.ACTIVE);
        repository.save(user);
    }

    @PutMapping("/{id}/status/{status}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void updateStatus(@PathVariable String id, @PathVariable EntityStatus status) {
        userService.updateStatus(id, status);
    }

    @PutMapping("/{id}/phone/{phoneNumber}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void updatePhoneNumber(@PathVariable String id, @PathVariable String phoneNumber) {
        userService.updatePhoneNumber(id, phoneNumber);
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void save(@RequestBody @Validated UserRequest userRequest) {
        User user = mapper.toEntity(userRequest);
        userService.save(user);
        notificationService.notify(NotificationDto.builder().type(REGISTRATION).entity(user).build());
    }

    @PutMapping("/{id}/password")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    public void changePassword(@PathVariable String id, @RequestBody @Validated PasswordRequest passwordRequest) {
        userService.changePassword(id, passwordRequest);
    }

    @GetMapping("/search")
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public CustomPagedResource<List<UserResponse>> pageable(@RequestParam("page") int page,
                                                            @RequestParam("size") int size,
                                                            @RequestParam(required = false, defaultValue = "createdDate") String sort,
                                                            @RequestParam(required = false, defaultValue = "ASC") String direction,
                                                            @RequestParam(name = "id", required = false) String id,
                                                            @RequestParam(name = "email", required = false) String email,
                                                            @RequestParam(name = "firstName", required = false) String firstName,
                                                            @RequestParam(name = "lastName", required = false) String lastName,
                                                            @RequestParam(name = "phoneNumber", required = false) String phoneNumber,
                                                            @RequestParam(name = "status", required = false) EntityStatus status) {
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.valueOf(direction), sort);
        Page<UserResponse> map = userService.search(id, email, firstName, lastName, phoneNumber, status, pageable).map(m -> mapper.toResponse(m));
        return CustomPagedResourceConverter.map(map, sort, direction);
    }
}
