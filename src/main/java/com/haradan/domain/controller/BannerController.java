package com.haradan.domain.controller;

import com.haradan.common.controller.query.QueryController;
import com.haradan.common.dao.repository.CustomPagedResource;
import com.haradan.common.dao.repository.CustomPagedResourceConverter;
import com.haradan.common.enumeration.BannerType;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.common.exception.exceptions.MicroException;
import com.haradan.domain.controller.request.BannerRequest;
import com.haradan.domain.controller.response.BannerResponse;
import com.haradan.domain.dao.entity.Banner;
import com.haradan.domain.dao.repository.BannerRepository;
import com.haradan.domain.dto.BannerSearchDto;
import com.haradan.domain.mapper.BannerMapper;
import com.haradan.domain.service.BannerService;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/banners")
@AllArgsConstructor
public class BannerController extends QueryController<Banner, String, BannerRequest, BannerResponse> {

    @Getter
    private BannerRepository repository;
    private BannerService bannerService;
    @Getter
    private BannerMapper mapper;

    @GetMapping("/{id}")
    public BannerResponse get(@PathVariable String id) throws MicroException {
        return mapper.toResponse(bannerService.get(id));
    }

    @PutMapping("/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public void put(@PathVariable String id, @RequestBody @Validated BannerRequest bannerRequest) {
        bannerService.put(id, mapper.toEntity(bannerRequest));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public void delete(@PathVariable String id) {
        bannerService.delete(id);
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public void save(@RequestBody @Validated BannerRequest bannerRequest) {
        bannerService.save(mapper.toEntity(bannerRequest));
    }

    @GetMapping("/search")
    public CustomPagedResource<List<BannerResponse>> pageable(@RequestParam("type") BannerType type,
    		 												  @RequestParam(name = "status", required = false) EntityStatus status,
                                                              @RequestParam("size") int size,
                                                              @RequestParam(defaultValue = "0", required = false) int page,
                                                              @RequestParam(defaultValue = "createDate", required = false) String sort,
                                                              @RequestParam(defaultValue = "DESC", required = false) String direction) {
        
    	BannerSearchDto searchDto = BannerSearchDto.builder()
    			.status(status)
    			.type(type)
                .build();
    	
    	Pageable pageable = PageRequest.of(page, size, Sort.Direction.valueOf(direction), sort);
        Page<BannerResponse> map = bannerService.search(searchDto, pageable).map(m -> mapper.toResponse(m));
        return CustomPagedResourceConverter.map(map, sort, direction);
    }
}
