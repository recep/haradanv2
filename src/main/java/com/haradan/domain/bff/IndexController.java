package com.haradan.domain.bff;

import com.haradan.common.controller.AbstractController;
import com.haradan.common.dao.repository.CustomPagedResource;
import com.haradan.common.dao.repository.CustomPagedResourceConverter;
import com.haradan.common.enumeration.BannerType;
import com.haradan.common.enumeration.DopingType;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.common.util.Constants;
import com.haradan.domain.controller.response.*;
import com.haradan.domain.dto.ArticleSearchDto;
import com.haradan.domain.dto.BannerSearchDto;
import com.haradan.domain.dto.SearchDto;
import com.haradan.domain.dto.StableSearchDto;
import com.haradan.domain.mapper.*;
import com.haradan.domain.service.*;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@AllArgsConstructor
public class IndexController extends AbstractController {
    private AdvertService advertService;
    private CategoryService categoryService;
    private BannerService bannerService;
    private UserService userService;
    private AdvertMapper advertMapper;
    private CategoryMapper categoryMapper;
    private BannerMapper bannerMapper;
    private UserMapper userMapper;
    private final ArticleService articleService;
    private final ArticleMapper articleMapper;
    private StableService stableService;
    private StableMapper stableMapper;

    @GetMapping("/")
    @Transactional
    public String index(Model model) {

        BannerSearchDto searchDto = BannerSearchDto.builder().status(EntityStatus.ACTIVE).type(BannerType.SLIDER)
                .build();

        List<AdvertResponse> topOrderAdverts = advertService.searchDoping(DopingType.TOP_ORDER, 50, Constants.CREATED_DATE);

        Pageable pageableLastAds = PageRequest.of(0, 15, Sort.Direction.DESC, Constants.CREATED_DATE);
        List<AdvertResponse> lastAdverts = advertService
                .search(SearchDto.builder().status(EntityStatus.ACTIVE).build(), pageableLastAds)
                .map(m -> advertMapper.toResponse(m))
                .get()
                .filter(a -> !topOrderAdverts.contains(a))
                .collect(Collectors.toList());

        model.addAttribute("lastAdverts", lastAdverts);
        model.addAttribute("topOrderAdverts", topOrderAdverts);
        model.addAttribute("urgentAdverts", advertService.searchDoping(DopingType.EMERGENCY, 50, Constants.CREATED_DATE));
        model.addAttribute("showcaseAdverts", advertService.searchDoping(DopingType.MAINPAGE_SHOW_CASE, 50, Constants.CREATED_DATE));

        Pageable pageableSlides = PageRequest.of(0, 20, Sort.Direction.DESC, "orderId");
        Page<BannerResponse> mapSlides = bannerService.search(searchDto, pageableSlides)
                .map(m -> bannerMapper.toResponse(m));
        model.addAttribute("slides", mapSlides.get().collect(Collectors.toList()));

        searchDto.setType(BannerType.CORPORATE_ADVERT);
        Pageable pageableCorporateAds = PageRequest.of(0, 20, Sort.Direction.DESC, "orderId");
        Page<BannerResponse> mapCorporateAds = bannerService.search(searchDto, pageableCorporateAds)
                .map(m -> bannerMapper.toResponse(m));
        model.addAttribute("corporateAdverts", mapCorporateAds.get().collect(Collectors.toList()));

        Pageable pageableArticles = PageRequest.of(0, 3, Sort.Direction.DESC, "createdDate");
        Page<ArticleResponse> mapArticles = articleService
                .search(ArticleSearchDto.builder().status(EntityStatus.ACTIVE).build(), pageableArticles)
                .map(m -> articleMapper.toResponse(m));
        model.addAttribute("lastArticles", mapArticles.get().collect(Collectors.toList()));

        List<CategoryResponse> categoryMap = categoryMapper.toResponse(categoryService.getParentCategories());
        model.addAttribute("categoryList", categoryMap);
        return "home";
    }

    @GetMapping("/ekuri-ilanlari")
    @Transactional
    public String corporateAds(Model model) {

        BannerSearchDto searchDto = BannerSearchDto.builder().status(EntityStatus.ACTIVE)
                .type(BannerType.CORPORATE_ADVERT).build();

        Pageable pageableCorporateAds = PageRequest.of(0, 3, Sort.Direction.DESC, "orderId");
        Page<BannerResponse> mapCorporateAds = bannerService.search(searchDto, pageableCorporateAds)
                .map(m -> bannerMapper.toResponse(m));
        model.addAttribute("corporateAdverts", mapCorporateAds.get().collect(Collectors.toList()));
        model.addAttribute("page", BannerType.CORPORATE_ADVERT.toString());
        return "featured-ads";
    }

    @GetMapping("/giris-yap")
    @Transactional
    public String loginPage(Model model, @RequestParam(required = false) String ref) {
        model.addAttribute("ref", ref);
        return "login";
    }

    @GetMapping("/uye-ol")
    @Transactional
    public String registerPage(Model model) {
        return "register";
    }

    @GetMapping("/sifremi-unuttum")
    @Transactional
    public String forgotPasswordPage(Model model) {
        return "forgot-password";
    }

    @GetMapping("/arama")
    @Transactional
    public String arama(Model model) {
        List<CategoryResponse> categoryMap = categoryMapper.toResponse(categoryService.getParentCategories());
        model.addAttribute("categoryList", categoryMap);
        return "search-mobil";
    }

    @GetMapping("/yeni-sifre-olustur")
    @Transactional
    public String createNewPasswordPage(@RequestParam(name = "id", required = true) String id,
                                        @RequestParam(name = "key", required = true) String key, Model model) {
        UserDetailResponse userDetail = userMapper.toDetailResponse(userService.get(id));
        model.addAttribute("userDetail", userDetail);
        model.addAttribute("resetPasswordKey", key);
        return "create-new-password";
    }

    @GetMapping("/blog/{searchText}/{id:[\\w-]+$}")
    @Transactional
    public String articleDetail(HttpServletRequest request, Model model, @PathVariable String searchText,
                                @PathVariable String id) {
        ArticleResponse article = articleMapper.toResponse(articleService.get(id));
        model.addAttribute("article", article);
        return "article/article-detail";
    }

    @GetMapping("/haralar")
    @Transactional
    public String haralar(@RequestParam(value = "page", defaultValue = "0") int page,
                          Model model) {
        StableSearchDto searchDto = StableSearchDto.builder().build();

        Pageable pageable = PageRequest.of(page, 18, Sort.Direction.ASC, "name");
        Page<StableResponse> map = stableService.search(searchDto, pageable).map(stableMapper::toResponse);
        CustomPagedResource<List<StableResponse>> stableResource =  CustomPagedResourceConverter.map(map, "name", Sort.Direction.ASC.toString());
        model.addAttribute("stableResource", stableResource);

        return "stables";
    }
}