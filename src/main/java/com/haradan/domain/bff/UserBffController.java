package com.haradan.domain.bff;

import com.haradan.common.controller.AbstractController;
import com.haradan.common.dao.repository.CustomPagedResource;
import com.haradan.common.dao.repository.CustomPagedResourceConverter;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.common.util.Constants;
import com.haradan.domain.controller.response.AdvertResponse;
import com.haradan.domain.dto.SearchDto;
import com.haradan.domain.mapper.AdvertMapper;
import com.haradan.domain.mapper.UserMapper;
import com.haradan.domain.service.AdvertService;
import com.haradan.domain.service.CustomLogoutHandler;
import com.haradan.domain.service.SecurityService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@AllArgsConstructor
@PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
public class UserBffController extends AbstractController {

	private final SecurityService securityService;
	private final UserMapper userMapper;
	private final AdvertService advertService;
	private final AdvertMapper advertMapper;
	private final CustomLogoutHandler logoutHandler;

	@GetMapping("/panelim")
	public String dashboard(Model model) {

		if (securityService.isAdmin()) {
			return Constants.REDIRECT_ADMIN_PANEL;
		} else {
			// Tamamlanmayı bekleyen ilanlar okunur
			SearchDto searchDto = SearchDto.builder().userId(securityService.getLoginUser().getIdentifier())
					.status(EntityStatus.NOT_COMPLETED).build();

			Pageable pageable = PageRequest.of(0, 1000, Sort.Direction.DESC, Constants.CREATED_DATE);
			Page<AdvertResponse> map = advertService.search(searchDto, pageable).map(advertMapper::toResponse);
			CustomPagedResource<List<AdvertResponse>> notCompletedAdList = CustomPagedResourceConverter.map(map,
					Constants.CREATED_DATE, Sort.Direction.DESC.toString());

			model.addAttribute("notCompletedAdList", notCompletedAdList);

			// Aktif ve onay bekleyen ilanlar okunur
			searchDto = SearchDto.builder().userId(securityService.getLoginUser().getIdentifier()).build();
			map = advertService.search(searchDto, pageable).map(advertMapper::toResponse);
			CustomPagedResource<List<AdvertResponse>> activeAndWaitingAdList = CustomPagedResourceConverter.map(map,
					Constants.CREATED_DATE, Sort.Direction.DESC.toString());

			// Kullanıcının hiç ilanı yoksa panelde mesaj göstermek için eklendi.
			model.addAttribute("userAdCount", activeAndWaitingAdList.getPage().getTotalElements());

			activeAndWaitingAdList.setData(
					activeAndWaitingAdList.getData().stream().filter(a -> (EntityStatus.ACTIVE.equals(a.getStatus())
							|| EntityStatus.WAITING_APPROVAL.equals(a.getStatus()))).map(a -> {
								a.setDopings(advertMapper.toDopingDtos(advertService.getDopings(a.getIdentifier())));
								return a;
							})
							.filter(a -> a.getDopings().stream()
									.anyMatch(x -> EntityStatus.WAITING_APPROVAL.equals(x.getStatus())))
							.collect(Collectors.toList()));

			model.addAttribute("activeAndWaitingAdList", activeAndWaitingAdList);

			model.addAttribute("page", "panelim");

			return "user/dashboard";
		}
	}

	@GetMapping("/ilanlarim")
	@Transactional
	public String myAds(@RequestParam(name = "status", required = false) EntityStatus status, Model model) {

		SearchDto searchDto = SearchDto.builder().userId(securityService.getLoginUser().getIdentifier()).build();

		Pageable pageable = PageRequest.of(0, 1000, Sort.Direction.DESC, "createdDate");
		Page<AdvertResponse> map = advertService.search(searchDto, pageable).map(advertMapper::toResponse);
		CustomPagedResource<List<AdvertResponse>> adList = CustomPagedResourceConverter.map(map, "createdDate",
				Sort.Direction.DESC.toString());
		adList.setData(adList.getData().stream()
				.filter(a -> (EntityStatus.ACTIVE.equals(status)) ? status.equals(a.getStatus())
						: !EntityStatus.ACTIVE.equals(a.getStatus()))
				.map(a -> {
					a.setDopings(advertMapper.toDopingDtos(advertService.getDopings(a.getIdentifier())));
					return a;
				}).collect(Collectors.toList()));
		model.addAttribute("adList", adList);
		model.addAttribute("status", status);

		return "user/my-ads";
	}

	@Transactional
	@GetMapping("/favori-ilanlarim")
	public String favouriteAds(Model model) {
		Pageable pageable = PageRequest.of(0, 1000, Sort.Direction.DESC, "createdDate");
		Page<AdvertResponse> map = advertService
				.searchFavouriteAds(securityService.getLoginUser().getIdentifier(), pageable)
				.map(advertMapper::toResponse);
		CustomPagedResource<List<AdvertResponse>> adList = CustomPagedResourceConverter.map(map, "createdDate",
				Sort.Direction.DESC.toString());
		model.addAttribute("adList", adList);

		return "user/favourite-ads";
	}

	@GetMapping("/uyelik-bilgilerim")
	public String myAccount(Model model, @RequestParam(required = false) String ref) {
		model.addAttribute("user", userMapper.toResponse(securityService.getLoginUser()));
		model.addAttribute("ref", ref);
		return "user/my-account";
	}

	@GetMapping("/sifre-degisikligi")
	public String changePassword(Model model) {
		model.addAttribute("user", userMapper.toResponse(securityService.getLoginUser()));
		return "user/change-password";
	}

	@GetMapping("/bilgilendirme-ayarlari")
	@Transactional
	public String mySettings(Model model) {
		model.addAttribute("user", userMapper.toDetailResponse(securityService.getLoginUser()));
		return "user/my-settings";
	}

	@GetMapping(value = "/cikis-yap")
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null) {
			logoutHandler.logout(request, response, authentication);
		}
		return "redirect:/";
	}
}