package com.haradan.domain.bff;

import com.haradan.common.controller.AbstractController;
import com.haradan.common.enumeration.StaticPageType;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@AllArgsConstructor
public class StaticPageController extends AbstractController {

    @GetMapping(StaticPageType.Path.ERROR)
    public String error() {
        return StaticPageType.Path.ERROR;
    }

    @GetMapping(StaticPageType.Path.STALLION_INTRODUCE)
    public String stallionIntroduce() {
        return StaticPageType.STALLION_INTRODUCE.getPage();
    }

    @GetMapping(StaticPageType.Path.CORPORATE_ADVERT)
    public String corporateAdvert() {
        return StaticPageType.CORPORATE_ADVERT.getPage();
    }

    @GetMapping(StaticPageType.Path.HOW_TO_GIVE_AD)
    public String howToGiveAd() {
        return StaticPageType.HOW_TO_GIVE_AD.getPage();
    }

    @GetMapping(StaticPageType.Path.GIVE_ADVERTISEMENT)
    public String giveAdvertisement() {
        return StaticPageType.GIVE_ADVERTISEMENT.getPage();
    }

    @GetMapping(StaticPageType.Path.GIVE_CORPORATE_AD)
    public String giveCorporateAd() {
        return StaticPageType.GIVE_CORPORATE_AD.getPage();
    }

    @GetMapping(StaticPageType.Path.GIVE_AD_WITH_SOCIAL)
    public String giveAdWithFacebook() {
        return StaticPageType.GIVE_AD_WITH_SOCIAL.getPage();
    }

    @GetMapping(StaticPageType.Path.AD_CREATION_RULE)
    public String adCreationRule() {
        return StaticPageType.AD_CREATION_RULE.getPage();
    }

    @GetMapping(StaticPageType.Path.ABOUT)
    public String about() {
        return StaticPageType.ABOUT.getPage();
    }

    @GetMapping(StaticPageType.Path.CONTACT)
    public String contact() {
        return StaticPageType.CONTACT.getPage();
    }

    @GetMapping(StaticPageType.Path.SERVICES)
    public String services() {
        return StaticPageType.SERVICES.getPage();
    }

    @GetMapping(StaticPageType.Path.MEMBERSHIP_AGGREMENT)
    public String membershipAggrement() {
        return StaticPageType.MEMBERSHIP_AGGREMENT.getPage();
    }

    @GetMapping(StaticPageType.Path.BANNED_PRODUCT_LIST)
    public String bannedProductList() {
        return StaticPageType.BANNED_PRODUCT_LIST.getPage();
    }

    @GetMapping(StaticPageType.Path.TERMS_OF_USE)
    public String termOfUse() {
        return StaticPageType.TERMS_OF_USE.getPage();
    }

    @GetMapping(StaticPageType.Path.PRIVACY_POLICY)
    public String privacyPolicy() {
        return StaticPageType.PRIVACY_POLICY.getPage();
    }

    @GetMapping(StaticPageType.Path.KVKK)
    public String kvkk() {
        return StaticPageType.KVKK.getPage();
    }

    @GetMapping(StaticPageType.Path.HELP)
    public String help() {
        return StaticPageType.HELP.getPage();
    }

    @GetMapping(StaticPageType.Path.DOPINGS)
    public String dopings() {
        return StaticPageType.DOPINGS.getPage();
    }
}