package com.haradan.domain.bff;

import com.haradan.common.controller.AbstractController;
import com.haradan.common.dao.repository.CustomPagedResource;
import com.haradan.common.dao.repository.CustomPagedResourceConverter;
import com.haradan.common.enumeration.DopingType;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.common.enumeration.SpecialPathType;
import com.haradan.common.enumeration.StaticPageType;
import com.haradan.common.exception.exceptions.MicroException;
import com.haradan.common.util.Constants;
import com.haradan.common.util.Utils;
import com.haradan.domain.controller.response.AdvertDetailResponse;
import com.haradan.domain.controller.response.AdvertResponse;
import com.haradan.domain.controller.response.BannerResponse;
import com.haradan.domain.controller.response.CategoryResponse;
import com.haradan.domain.controller.response.CityResponse;
import com.haradan.domain.dao.entity.Advert;
import com.haradan.domain.dao.entity.AdvertProperty;
import com.haradan.domain.dao.entity.Category;
import com.haradan.domain.dao.entity.User;
import com.haradan.domain.dao.entity.horse.Horse;
import com.haradan.domain.dto.SearchDto;
import com.haradan.domain.dto.SearchResultDto;
import com.haradan.domain.dto.horse.HorseDto;
import com.haradan.domain.dto.horse.Pedigri;
import com.haradan.domain.mapper.AdvertMapper;
import com.haradan.domain.mapper.BannerMapper;
import com.haradan.domain.mapper.CategoryMapper;
import com.haradan.domain.mapper.CityMapper;
import com.haradan.domain.mapper.HorseMapper;
import com.haradan.domain.mapper.UserMapper;
import com.haradan.domain.service.AdvertService;
import com.haradan.domain.service.BannerService;
import com.haradan.domain.service.CategoryService;
import com.haradan.domain.service.CityService;
import com.haradan.domain.service.HorseService;
import com.haradan.domain.service.PaymentService;
import com.haradan.domain.service.PropertyService;
import com.haradan.domain.service.SecurityService;
import com.haradan.domain.service.UserService;
import com.haradan.domain.service.file.upload.impl.AdvertFileService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.mobile.device.Device;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.haradan.common.util.Constants.OG_TITLE;
import static com.haradan.common.util.Constants.STEP_ILAN_BILGILERI;
import static com.haradan.common.util.Constants.STEP_KATEGORI_SECIM;
import static com.haradan.common.util.Constants.STEP_ODEME_YAP;
import static com.haradan.common.util.Constants.STEP_SONUC;
import static com.haradan.common.util.Constants.comma;
import static com.haradan.common.util.Constants.slash;
import static com.haradan.common.util.Utils.getProperties;
import static com.haradan.common.util.Utils.isValidPostAdStep;
import static com.haradan.common.util.Utils.mapper;
import static java.util.Objects.isNull;
import static org.springframework.util.StringUtils.isEmpty;

@Slf4j
@Controller
@RequiredArgsConstructor
public class AdvertBffController extends AbstractController {
    private final AdvertService advertService;
    private final AdvertMapper advertMapper;
    private final CategoryService categoryService;
    private final CategoryMapper categoryMapper;
    private final CityService cityService;
    private final CityMapper cityMapper;
    private final PropertyService propertyService;
    private final AdvertFileService advertFileService;
    private final PaymentService paymentService;
    private final SecurityService securityService;
    private final HorseService horseService;
    private final HorseMapper horseMapper;
    private final BannerService bannerService;
    private final BannerMapper bannerMapper;
    private final UserService userService;
    private final UserMapper userMapper;

    @GetMapping("/{categoryText}/{searchText}/{id:[\\w-]+$}")
    @Transactional
    public String adDetailPage(HttpServletRequest request, Model model,
                               @PathVariable String categoryText,
                               @PathVariable String searchText,
                               @PathVariable String id) {
        try {
            Advert advert = advertService.get(id);
            AdvertDetailResponse advertDetailResponse = advertMapper.toDetailResponse(advert);
            List<AdvertProperty> advertProperties = advertService.getAdvertProperties(advert);
            advertDetailResponse.setAdvertPropertyList(advertMapper.toPropertyDtos(advertProperties));
            advertDetailResponse.setDopings(advertMapper.toDopingDtos(advertService.getDopings(advert.getIdentifier())));
            advertDetailResponse.setMediaMap(advertFileService.createAdvertMediaMap(advert));
            model.addAttribute("advertDetail", advertDetailResponse);

            setSimilarAdverts(model, advert);
            setHorseDetail(model, advert.getTjkNo());
            request.setAttribute(OG_TITLE, advert.getCategory().getName() + Constants.reagent + advert.getTitle());
            model.addAttribute("pageName", "ad-detail");
        } catch (Exception e) {
            return StaticPageType.Path.ERROR;
        }
        return "ad-detail";
    }

    private void setHorseDetail(Model model, Long tjkNo) {
        if (tjkNo == null) {
            return;
        }
        try {
            Horse horse = horseService.getHorseDetailFromDB(tjkNo);
            HorseDto horseDto = horseMapper.toHorse(horse);

            if (!isNull(horseDto)) {
                model.addAttribute("statistics", horseDto.getHorseStatistics());
                model.addAttribute("siblings", horseDto.getHorseSiblings());
            }
            if (!isNull(horse.getHorsePedigri()) && !isNull(horse.getHorsePedigri().getPedigri())) {
                model.addAttribute("pedigri", Arrays.asList(mapper.readValue(horse.getHorsePedigri().getPedigri(), Pedigri[].class)));
            }
        } catch (Exception e) {
            log.error("At detay bilgileri getirilirken hata oluştu.", e);
        }
    }

    private void setSimilarAdverts(Model model, Advert advert) {
        List<AdvertResponse> similarAdverts = advertService.getSimilarAdverts(advert).stream()
                .map(advertMapper::toResponse)
                .collect(Collectors.toList());

        model.addAttribute("similarAdverts", similarAdverts);
    }

    @Transactional
    @PreAuthorize("hasAuthority('{authority=ROLE_USER}')")
    @GetMapping(value = {"/ilan-ver", "/ilan-ver/{step}", "/ilan-ver/{step}/{advert_id}"})
    public String postAdPage(HttpServletRequest request, Model model, Device device,
                             @PathVariable(required = false) String step,
                             @PathVariable(name = "advert_id", required = false) String advertId) throws Exception {

        Advert advert = null;

        if (securityService.getLoginUser().getIdentifier() != null
                && (securityService.getLoginUser().getPhoneNumber() == null || securityService.getLoginUser().getPhoneNumber().length() != 10)) {
            return Constants.REDIRECT_UPDATE_PHONE_NUMBER;
        }

        if (!isEmpty(advertId)) {
            if (!isValidPostAdStep(step)) {
                return Constants.REDIRECT_POST_AD_AD_INFO + "/" + advertId;
            }
            try {
                advert = advertService.get(advertId);
                if (!securityService.isAdmin() && !advert.getUser().getIdentifier().equals(securityService.getLoginUser().getIdentifier()))
                    return Constants.REDIRECT_POST_AD_CAT_SELECT;

                AdvertDetailResponse advertDetailResponse = advertMapper.toDetailResponse(advert);
                advertDetailResponse.setAdvertPropertyList(advertMapper.toPropertyDtos(advertService.getAdvertProperties(advert)));
                model.addAttribute("advertDetail", advertDetailResponse);

                model.addAttribute("subCategoryList", categoryMapper
                        .toResponse(categoryService.get(advert.getCategory().getParent().getIdentifier()).getChildren()));
                model.addAttribute("parentCategory", advert.getCategory().getParent().getIdentifier());
            } catch (MicroException e) {
                return Constants.REDIRECT_POST_AD_CAT_SELECT;
            }
        }

        if (isEmpty(advertId) || !isValidPostAdStep(step) || STEP_KATEGORI_SECIM.equals(step)) {
            step = STEP_KATEGORI_SECIM;
            List<CategoryResponse> categoryMap = categoryMapper.toResponse(categoryService.getParentCategories());
            model.addAttribute("categoryList", categoryMap);
        } else if (STEP_ILAN_BILGILERI.equals(step)) {
            Pageable pageable = PageRequest.of(0, 100, Sort.Direction.ASC, "name");
            Page<CityResponse> cityMap = cityService.search("TR", null, null, pageable).map(cityMapper::toResponse);
            model.addAttribute("cityList", cityMap.get().collect(Collectors.toList()));
            model.addAttribute("dopingList", advertMapper.toDopingDtos(advertService.getDopings()));
            model.addAttribute("advertDopingList", advertMapper.toDopingDtos(advertService.getDopings(advertId)));
            model.addAttribute("propertyList", propertyService.getByCategoryIdWithLookup(advert.getCategory().getIdentifier()));
        } else if (STEP_ODEME_YAP.equals(step)) {
            if (securityService.isAdmin()) {
                step = STEP_SONUC;
            } else {
                int amount = advertService.getAmount(advert);
                String paytrFormContent = paymentService.getPaytrIframe(request, advert, amount);
                model.addAttribute("paytrFormContent", paytrFormContent);
            }
        }
        model.addAttribute("step", step);
        model.addAttribute("advert_id", advertId);
        model.addAttribute("advertNo", advert == null ? 0L : advert.getAdvertNo());
        model.addAttribute("deviceType", Utils.getDeviceType(device));
        return "post-ad";
    }

    @Transactional
    @GetMapping("/at-sahibi/{userId}")
    public String searchByAdvertOwner(Model model, @PathVariable String userId) {
        SearchDto searchDto = SearchDto.builder()
                .status(EntityStatus.ACTIVE)
                .userId(userId).build();
        Map<String, List<SearchResultDto>> searchResultList = advertService.searchAdvertForCategoryGroup(searchDto);

        Long searchAdCount = searchResultList.entrySet()
                .stream()
                .flatMap(s -> s.getValue().stream())
                .mapToLong(SearchResultDto::getAdCount)
                .sum();

        User user = userService.get(userId);

        model.addAttribute("searchResultList", searchResultList);
        model.addAttribute("searchCriteria", "userId");
        model.addAttribute("searchCriteriaValue", userId);
        model.addAttribute("searchHorseOwnerTitle", user.getFirstName() + " " + user.getLastName());
        model.addAttribute("searchAdCount", searchAdCount);

        return "search-with-text";
    }

    @Transactional
    @GetMapping("/at-bilgileri/{searchText}")
    public String searchByText(HttpServletRequest request, Model model, @PathVariable String searchText) {
        if (NumberUtils.isDigits(searchText)) {
            try {
                Advert advert = advertService.get(Long.valueOf(searchText));
                return Constants.REDIRECT + advert.getUrl();
            } catch (Exception e) {
                log.info("Full text search going on although advert not found! ", e);
            }
        }

        SearchDto searchDto = SearchDto.builder()
                .status(EntityStatus.ACTIVE)
                .searchText(searchText).build();
        Map<String, List<SearchResultDto>> searchResultList = advertService.searchAdvertForCategoryGroup(searchDto);

        Long searchAdCount = searchResultList.entrySet()
                .stream()
                .flatMap(s -> s.getValue().stream())
                .mapToLong(SearchResultDto::getAdCount)
                .sum();

        model.addAttribute("searchResultList", searchResultList);
        model.addAttribute("searchCriteria", "searchText");
        model.addAttribute("searchCriteriaValue", searchText);
        model.addAttribute("searchCriteriaTitle", searchText);
        model.addAttribute("searchAdCount", searchAdCount);

        return "search-with-text";
    }

    @GetMapping("{categorySearchText:[\\w-]+$}")
    @Transactional
    public String search(HttpServletRequest request, HttpServletResponse response, Model model, Device device,
                         @PathVariable String categorySearchText,
                         @RequestParam(value = "page", defaultValue = "0") int page,
                         @RequestParam(value = "size", defaultValue = "20") int size,
                         @RequestParam(defaultValue = Constants.CREATED_DATE, required = false) String sort,
                         @RequestParam(defaultValue = Constants.DIRECTION_DESC, required = false) String direction,
                         @RequestParam(name = "id", required = false) String id,
                         @RequestParam(name = "advertNo", required = false) Long advertNo,
                         @RequestParam(name = "searchText", required = false) String searchText,
                         @RequestParam(name = "date", required = false) Integer dateInterval,
                         @RequestParam(name = "hasPhoto", required = false) Boolean hasPhoto,
                         @RequestParam(name = "minPrice", required = false) Double minPrice,
                         @RequestParam(name = "maxPrice", required = false) Double maxPrice,
                         @RequestParam(name = "city", required = false) String city,
                         @RequestParam(name = "district", required = false) String district,
                         @RequestParam(name = "userId", required = false) String userId,
                         @RequestParam Map<String, String> params) {
        System.out.println("categorySearchText:" + categorySearchText);
        SearchDto searchDto = getProperties(params);
        searchDto.setCategorySearchText(SpecialPathType.of(categorySearchText) != null ? SpecialPathType.of(categorySearchText).getCategorySearchText() : categorySearchText);
        searchDto.setPage(page);
        searchDto.setSize(size);
        searchDto.setSort(sort);
        searchDto.setDirection(direction);
        searchDto.setId(id);
        searchDto.setAdvertNo(advertNo);
        searchDto.setSearchText(searchText);
        searchDto.setStatus(EntityStatus.ACTIVE);
        searchDto.setDateInterval(dateInterval);
        searchDto.setHasPhoto(hasPhoto);
        searchDto.setMinPrice(minPrice);
        searchDto.setMaxPrice(maxPrice);
        searchDto.setCity(city);
        searchDto.setDistrict(district);
        searchDto.setUserId(userId);

        try {
            Category category = categoryService.getBySearchText(searchDto.getCategorySearchText());
            consolideSearchResult(model, device, category, searchDto, categorySearchText);
            model.addAttribute("propertyList", propertyService.getByCategoryIdWithLookup(category.getIdentifier()));
            model.addAttribute("categoryTree", getCategoryTree(category));

            request.setAttribute(OG_TITLE, SpecialPathType.of(categorySearchText) != null ? SpecialPathType.of(categorySearchText).getTitle() : category.getName());

            model.addAttribute("categorySearchText", searchDto.getCategorySearchText());

            model.addAttribute("topOrderAdverts", advertService.searchDoping(DopingType.TOP_ORDER, 50, Constants.CREATED_DATE));
        } catch (MicroException e) {
            Utils.redirect(response, HttpServletResponse.SC_MOVED_PERMANENTLY, slash);
            return null;
        } catch (Exception e) {
            log.error("İlanlar getirilirken hata oluştu. categorySearchText: {} ", searchDto.getCategorySearchText(), e);
            return StaticPageType.Path.ERROR;
        }
        return "search";
    }

    @Transactional
    @GetMapping("/satilik-ekuri-atlari/{searchText}/{id}")
    public String corporateAdvert(Model model, @PathVariable String searchText, @PathVariable String id) {

        BannerResponse banner = bannerMapper.toResponse(bannerService.get(id));

        model.addAttribute("banner", banner);

        List<Advert> advertList = Arrays.stream(banner
                        .getDescription()
                        .split(comma))
                .map(advertNo -> advertService.get(Long.valueOf(advertNo)))
                .collect(Collectors.toList());


        List<AdvertDetailResponse> advertDetailResponseList = new ArrayList<AdvertDetailResponse>();
        for (Advert advert : advertList) {
            AdvertDetailResponse advertDetailResponse = advertMapper.toDetailResponse(advert);
            List<AdvertProperty> advertProperties = advertService.getAdvertProperties(advert);
            advertDetailResponse.setAdvertPropertyList(advertMapper.toPropertyDtos(advertProperties));
            advertDetailResponse.setMediaMap(advertFileService.createAdvertMediaMap(advert));

            advertDetailResponseList.add(advertDetailResponse);
        }
        model.addAttribute("corporateAdverts", advertDetailResponseList);

        model.addAttribute("mayAlsoInterestedAdverts", advertService.searchDoping(DopingType.MAINPAGE_SHOW_CASE, 10, Constants.CREATED_DATE));

        model.addAttribute("pageName", "corporate-ad-detail");
        return "corporate-ad";
    }


    @Transactional
    @GetMapping("/acil-satilik-at-ilanlari")
    public String emergency(HttpServletRequest request, Model model) {
        model.addAttribute("searchAdverts", advertService.searchDoping(DopingType.EMERGENCY, 50, Constants.CREATED_DATE));
        model.addAttribute("page", DopingType.EMERGENCY.toString());
        request.setAttribute(OG_TITLE, "Acil Satılık At ilanları");
        return "featured-ads";
    }

    @Transactional
    @GetMapping("/satilik-at-ilanlari/{doping}")
    public String pageableByDoping(@PathVariable("doping") String dopingSearchText, Model model) {
        model.addAttribute("searchAdverts", advertService.searchDoping(DopingType.of(dopingSearchText), 50, Constants.CREATED_DATE));
        model.addAttribute("page", DopingType.of(dopingSearchText).toString());
        return "featured-ads";
    }


    @Transactional
    @GetMapping("/soy-agaci/{type}")
    public String familyTree(@PathVariable("type") String type, Model model) {
        String searchText = Constants.FATHER_NAME;
        if (Constants.ALL_MOTHERS.equals(type)) {
            searchText = Constants.MOTHER_NAME;
        } else if (Constants.ALL_FATHERS_OF_MOTHERS.equals(type)) {
            searchText = Constants.FATHER_NAME_OF_MOTHER;
        }

        model.addAttribute("pedigreeNames", advertService.findPedigree(searchText));
        model.addAttribute("searchText", searchText);
        return "pedigree";
    }

    @Transactional
    @GetMapping("/horses/horse-list-view/{searchText}")
    public String horseListView(@PathVariable("searchText") String searchText, Model model) throws Exception {
        Horse horse = new Horse();
        horse.setName(URLDecoder.decode(searchText, "UTF-8"));

        model.addAttribute("horseList", horseService.getHorsesSummary(horse));
        return "post-ad/horse-list :: horse-list";
    }

    private void consolideSearchResult(Model model, Device device, Category category, SearchDto searchDto, String categorySearchText) {
        Pageable pageable = PageRequest.of(searchDto.getPage(), searchDto.getSize(), Sort.Direction.valueOf(searchDto.getDirection()), searchDto.getSort());

        Page<AdvertResponse> map = advertService.search(searchDto, pageable).map(advertMapper::toResponse);
        CustomPagedResource<List<AdvertResponse>> advertResource = CustomPagedResourceConverter.map(map, searchDto.getSort(), searchDto.getDirection());
        model.addAttribute("advertResource", advertResource);

        Pageable pageableCity = PageRequest.of(0, 100, Sort.Direction.ASC, "name");
        Page<CityResponse> cityMap = cityService.search("TR", null, null, pageableCity).map(cityMapper::toResponse);
        model.addAttribute("cityList", cityMap.get().collect(Collectors.toList()));

        String searchText = isEmpty(searchDto.getSearchText()) ?
                (SpecialPathType.of(categorySearchText) != null ? SpecialPathType.of(categorySearchText).getTitle() : category.getName())
                : searchDto.getSearchText();

        model.addAttribute("headInfo", getHeadInfo(category, searchText, advertResource.getPage().getTotalElements()));
        model.addAttribute("filterParams", searchDto);
        model.addAttribute("pageName", "search");
        model.addAttribute("deviceType", Utils.getDeviceType(device));
    }

    private List<Category> getCategoryTree(Category category) {
        List<Category> categoryList = new ArrayList<>();
        if (category.getParent() != null) {
            categoryList.add(category.getParent());
            categoryList.add(category);
            categoryList.addAll(category.getChildren());
        } else {
            categoryList.add(category);
            categoryList.addAll(category.getChildren());
        }
        return categoryList;
    }

    private String getHeadInfo(Category category, String searchText, Long itemSize) {
        StringBuilder st = new StringBuilder();
        st.append("\"" + (isEmpty(searchText) ? category.getName() : searchText) + "\" ");
        st.append(itemSize > 0 ? "aramanızda " + itemSize + " ilan bulundu." : "aramanızda ilan bulunamadı.");
        return st.toString();
    }
}