package com.haradan.domain.bff;

import java.util.List;
import java.util.stream.Collectors;

import com.haradan.domain.controller.response.*;
import com.haradan.domain.dao.entity.User;
import com.haradan.domain.dto.*;
import com.haradan.domain.mapper.*;
import com.haradan.domain.service.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.haradan.common.controller.AbstractController;
import com.haradan.common.dao.repository.CustomPagedResource;
import com.haradan.common.dao.repository.CustomPagedResourceConverter;
import com.haradan.common.enumeration.ArticleType;
import com.haradan.common.enumeration.BannerType;
import com.haradan.common.enumeration.DopingType;
import com.haradan.common.enumeration.EntityStatus;
import com.haradan.common.util.Constants;

import lombok.AllArgsConstructor;

@Controller
@AllArgsConstructor
@RequestMapping(value = "/admin")
@PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
public class AdminBffController extends AbstractController {
    private final SecurityService securityService;
    private final UserMapper userMapper;
    private final AdvertService advertService;
    private final UserService userService;
    private final AdvertMapper advertMapper;
    private final BannerService bannerService;
    private final BannerMapper bannerMapper;
    private final ArticleService articleService;
    private final ArticleMapper articleMapper;
    private final PaymentService paymentService;
    private final PaymentMapper paymentMapper;
    private final StableService stableService;
    private final StableMapper stableMapper;

    @GetMapping("/panelim")
    public String dashboard(Model model) {
        model.addAttribute("user", userMapper.toResponse(securityService.getLoginUser()));
        return "admin/dashboard";
    }

    @Transactional
    @GetMapping("/ilanlar")
    public String allAds(@RequestParam(value = "page", defaultValue = "0") int page,
                         @RequestParam(value = "size", defaultValue = "20") int size,
                         @RequestParam(defaultValue = Constants.CREATED_DATE, required = false) String sort,
                         @RequestParam(defaultValue = Constants.DIRECTION_DESC, required = false) String direction,
                         @RequestParam(name = "id", required = false) String id,
                         @RequestParam(name = "advertNo", required = false) Long advertNo,
                         @RequestParam(name = "status", required = false) EntityStatus status,
                         @RequestParam(name = "searchText", required = false) String searchText,
                         @RequestParam(name = "userId", required = false) String userId, Model model) {

        SearchDto searchDto = SearchDto.builder()
                .id(id)
                .advertNo(advertNo)
                .userId(userId)
                .status(status)
                .searchText(searchText)
                .build();

        Pageable pageable = PageRequest.of(page, size, Sort.Direction.valueOf(direction), sort);
        Page<AdvertResponse> map = advertService.search(searchDto, pageable).map(advertMapper::toResponse);
        CustomPagedResource<List<AdvertResponse>> adList = CustomPagedResourceConverter.map(map, sort, direction);
        model.addAttribute("dataList", adList);

        return "admin/ads";
    }

    @Transactional
    @GetMapping("/odemeler")
    public String payments(@RequestParam(value = "page", defaultValue = "0") int page,
                           @RequestParam(value = "size", defaultValue = "20") int size,
                           @RequestParam(defaultValue = Constants.CREATED_DATE, required = false) String sort,
                           @RequestParam(defaultValue = Constants.DIRECTION_DESC, required = false) String direction,
                           @RequestParam(name = "productId", required = false) String productId,
                           @RequestParam(name = "email", required = false) String email,
                           @RequestParam(name = "name", required = false) String name,
                           @RequestParam(name = "status", required = false) EntityStatus status, Model model) {

        Pageable pageable = PageRequest.of(page, size, Sort.Direction.valueOf(direction), sort);
        Page<PaymentResponse> map = paymentService.search(productId, email, name, status, pageable)
                .map(paymentMapper::toResponse);
        CustomPagedResource<List<PaymentResponse>> result = CustomPagedResourceConverter.map(map, sort, direction);
        model.addAttribute("dataList", result);

        return "admin/payments";
    }
    
    @GetMapping("/payment/detail")
    public String paymentDetail(@RequestParam(name = "id", required = true) String id, Model model) {
    	PaymentResponse payment = paymentMapper.toResponse(paymentService.get(id));
        model.addAttribute("payment", payment);
        return "admin/payments/payment-detail :: payment-detail-modal";
    }

    @Transactional
    @GetMapping("/kullanicilar")
    public String users(@RequestParam(value = "page", defaultValue = "0") int page,
                        @RequestParam(value = "size", defaultValue = "20") int size,
                        @RequestParam(defaultValue = Constants.CREATED_DATE, required = false) String sort,
                        @RequestParam(defaultValue = Constants.DIRECTION_DESC, required = false) String direction,
                        @RequestParam(name = "id", required = false) String id,
                        @RequestParam(name = "email", required = false) String email,
                        @RequestParam(name = "firstName", required = false) String firstName,
                        @RequestParam(name = "lastName", required = false) String lastName,
                        @RequestParam(name = "phoneNumber", required = false) String phoneNumber,
                        @RequestParam(name = "status", required = false) EntityStatus status, Model model) {

        Pageable pageable = PageRequest.of(page, size, Sort.Direction.valueOf(direction), sort);
        Page<UserResponse> map = userService.search(id, email, firstName, lastName, phoneNumber, status, pageable)
                .map(userMapper::toResponse);
        CustomPagedResource<List<UserResponse>> userList = CustomPagedResourceConverter.map(map, sort, direction);
        model.addAttribute("dataList", userList);

        return "admin/users";
    }

    @GetMapping("/kullanici")
    public String userForm(@RequestParam(name = "id", required = false) String id, Model model) {

        UserResponse userResponse = UserResponse.builder().build();
        if (id != null) {
            userResponse = userMapper.toResponse(userService.get(id));
        }
        model.addAttribute("user", userResponse);
        return "admin/users/user-form :: user-form-modal";
    }

    @Transactional
    @GetMapping("/bannerlar")
    public String banners(@RequestParam(value = "page", defaultValue = "0") int page,
                          @RequestParam(value = "size", defaultValue = "20") int size,
                          @RequestParam(defaultValue = Constants.CREATED_DATE, required = false) String sort,
                          @RequestParam(defaultValue = Constants.DIRECTION_DESC, required = false) String direction,
                          @RequestParam(name = "id", required = false) String id,
                          @RequestParam(name = "type", required = false) BannerType type,
                          @RequestParam(name = "status", required = false) EntityStatus status, Model model) {

        BannerSearchDto searchDto = BannerSearchDto.builder().status(status).id(id).type(type).build();

        Pageable pageable = PageRequest.of(page, size, Sort.Direction.valueOf(direction), sort);
        Page<BannerResponse> map = bannerService.search(searchDto, pageable).map(bannerMapper::toResponse);
        CustomPagedResource<List<BannerResponse>> bannerList = CustomPagedResourceConverter.map(map, sort, direction);

        model.addAttribute("dataList", bannerList);
        return "admin/banners";
    }

    @GetMapping("/banner")
    public String bannerForm(@RequestParam(name = "id", required = false) String id, Model model) {

        BannerResponse banner = BannerResponse.builder().build();
        if (id != null) {
            banner = bannerMapper.toResponse(bannerService.get(id));
        }
        model.addAttribute("banner", banner);
        return "admin/banners/banner-form :: banner-form-modal";
    }
    
    @Transactional
    @GetMapping("/yazilar")
    public String articles(@RequestParam(value = "page", defaultValue = "0") int page,
                          @RequestParam(value = "size", defaultValue = "20") int size,
                          @RequestParam(defaultValue = Constants.CREATED_DATE, required = false) String sort,
                          @RequestParam(defaultValue = Constants.DIRECTION_DESC, required = false) String direction,
                          @RequestParam(name = "id", required = false) String id,
                          @RequestParam(name = "status", required = false) EntityStatus status,
                          @RequestParam(name = "type", required = false) ArticleType type,Model model) {

        ArticleSearchDto searchDto = ArticleSearchDto.builder().status(status).type(type).id(id).build();

        Pageable pageable = PageRequest.of(page, size, Sort.Direction.valueOf(direction), sort);
        Page<ArticleResponse> map = articleService.search(searchDto, pageable).map(articleMapper::toResponse);
        CustomPagedResource<List<ArticleResponse>> articleList = CustomPagedResourceConverter.map(map, sort, direction);

        model.addAttribute("dataList", articleList);
        return "admin/articles";
    }
    
    @GetMapping("/yazi/detay")
    public String articleDetail(@RequestParam(name = "id", required = false) String id, Model model) {

    	ArticleResponse article = ArticleResponse.builder().build();
        if (id != null) {
        	article = articleMapper.toResponse(articleService.get(id));
        }
        model.addAttribute("article", article);
        return "admin/articles/article-detail";
    }

    @Transactional
    @GetMapping("/ilan-dopingleri")
    public String adDopings(@RequestParam(value = "page", defaultValue = "0") int page,
                            @RequestParam(value = "size", defaultValue = "20") int size,
                            @RequestParam(defaultValue = Constants.CREATED_DATE, required = false) String sort,
                            @RequestParam(defaultValue = Constants.DIRECTION_DESC, required = false) String direction,
                            @RequestParam(name = "advertId", required = false) String advertId,
                            @RequestParam(name = "type", required = false) DopingType type,
                            @RequestParam(name = "status", required = false) EntityStatus status, Model model) {

        AdDopingSearchDto searchDto = AdDopingSearchDto.builder().status(status).advertId(advertId).type(type).build();

        Pageable pageable = PageRequest.of(page, size, Sort.Direction.valueOf(direction), sort);
        Page<AdvertResponse> map = advertService.search(searchDto, pageable).map(advertMapper::toResponse);
        CustomPagedResource<List<AdvertResponse>> adList = CustomPagedResourceConverter.map(map, sort, direction);
        adList.getData().stream().map(a -> {
            a.setDopings(advertMapper.toDopingDtos(advertService.getDopings(a.getIdentifier())));
            return a;
        }).collect(Collectors.toList());
        model.addAttribute("dataList", adList);
        return "admin/ad-dopings";
    }

    @Transactional
    @GetMapping("/haralar")
    public String stables(@RequestParam(value = "page", defaultValue = "0") int page,
                          @RequestParam(value = "size", defaultValue = "20") int size,
                          @RequestParam(defaultValue = "name", required = false) String sort,
                          @RequestParam(defaultValue = Constants.DIRECTION_DESC, required = false) String direction,
                          @RequestParam(name = "name", required = false) String name,Model model) {

        StableSearchDto searchDto = StableSearchDto.builder().name(name).build();

        Pageable pageable = PageRequest.of(page, size, Sort.Direction.valueOf(direction), sort);
        Page<StableResponse> map = stableService.search(searchDto, pageable).map(stableMapper::toResponse);
        CustomPagedResource<List<StableResponse>> stableList = CustomPagedResourceConverter.map(map, sort, direction);

        model.addAttribute("dataList", stableList);
        return "admin/stables";
    }

    @GetMapping("/hara")
    public String stableForm(@RequestParam(name = "id", required = false) String id, Model model) {

        StableResponse stable = StableResponse.builder().build();
        if (id != null) {
            stable = stableMapper.toResponse(stableService.get(id));
        }
        model.addAttribute("stable", stable);
        return "admin/stables/stable-form :: stable-form-modal";
    }

    @GetMapping("/uyelik-bilgilerim")
    public String myAccount(Model model) {
        model.addAttribute("user", userMapper.toResponse(securityService.getLoginUser()));
        return "user/my-account";
    }

    @GetMapping("/sifre-degisikligi")
    public String changePassword(Model model) {
        model.addAttribute("user", userMapper.toResponse(securityService.getLoginUser()));
        return "user/change-password";
    }
}
