package com.haradan.domain.mapper;

import com.haradan.domain.dao.entity.horse.Horse;
import com.haradan.domain.dao.entity.horse.HorseSummary;
import com.haradan.domain.dto.horse.HorseDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface HorseSummaryMapper {

    HorseSummary toEntity(Horse horse);

    List<HorseSummary> toEntity(List<Horse> horse);

    List<HorseDto> toDto(List<HorseSummary> entity);

    HorseDto toDto(HorseSummary entity);

}
