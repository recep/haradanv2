package com.haradan.domain.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.haradan.common.mapper.BaseMapper;
import com.haradan.domain.controller.request.SettingRequest;
import com.haradan.domain.controller.request.UserRequest;
import com.haradan.domain.controller.response.UserDetailResponse;
import com.haradan.domain.controller.response.UserResponse;
import com.haradan.domain.dao.entity.Setting;
import com.haradan.domain.dao.entity.User;
import com.haradan.domain.dto.SettingDto;

@Mapper(componentModel = "spring")
public interface UserMapper extends BaseMapper<User, UserRequest, UserResponse> {

    UserDetailResponse toDetailResponse(User user);

    SettingDto toDto(Setting entity);

    Setting toEntity(SettingRequest settingRequest);
    
    @Mapping(target = "createDate", expression = "java(entity.getCreatedDate().toLocalDate())")
    @Mapping(target = "admin", expression = "java(entity.hasAdminAuthority())")
    UserResponse toResponse(User entity);
}
