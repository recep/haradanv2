package com.haradan.domain.mapper;

import com.haradan.common.mapper.BaseMapper;
import com.haradan.domain.controller.request.PropertyRequest;
import com.haradan.domain.controller.response.PropertyResponse;
import com.haradan.domain.dao.entity.Property;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PropertyMapper extends BaseMapper<Property, PropertyRequest, PropertyResponse> {

    @Override
    @Mappings({@Mapping(target = "categoryId", source = "category.identifier"),
            @Mapping(target = "parentId", source = "parent.identifier")})
    PropertyResponse toResponse(Property entity);

    @Override
    @Mappings({@Mapping(target = "category.identifier", source = "categoryId"),
            @Mapping(target = "parent.identifier", source = "parentId")})
    Property toEntity(PropertyRequest request);

    List<PropertyResponse> toResponse(List<Property> entity);
}
