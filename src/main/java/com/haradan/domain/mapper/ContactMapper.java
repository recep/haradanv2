package com.haradan.domain.mapper;

import org.mapstruct.Mapper;

import com.haradan.common.mapper.BaseMapper;
import com.haradan.domain.controller.request.ContactRequest;
import com.haradan.domain.controller.response.ContactResponse;
import com.haradan.domain.dao.entity.Contact;

@Mapper(componentModel = "spring")
public interface ContactMapper extends BaseMapper<Contact, ContactRequest, ContactResponse> {
}
