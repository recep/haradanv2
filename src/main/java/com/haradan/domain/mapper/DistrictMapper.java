package com.haradan.domain.mapper;

import com.haradan.common.mapper.BaseMapper;
import com.haradan.domain.controller.request.DistrictRequest;
import com.haradan.domain.controller.response.DistrictResponse;
import com.haradan.domain.dao.entity.District;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DistrictMapper extends BaseMapper<District, DistrictRequest, DistrictResponse> {
}
