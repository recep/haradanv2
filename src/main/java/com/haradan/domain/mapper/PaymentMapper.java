package com.haradan.domain.mapper;

import com.haradan.domain.controller.response.PaymentResponse;
import com.haradan.domain.dao.entity.Payment;
import com.haradan.domain.dao.entity.User;
import com.haradan.domain.dto.PaymentTokenDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface PaymentMapper {

    @Mapping(target = "status", ignore = true)
    Payment toEntity(PaymentTokenDto dto);

    PaymentResponse toResponse(Payment entity);
}
