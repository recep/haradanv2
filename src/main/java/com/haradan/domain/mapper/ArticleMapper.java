package com.haradan.domain.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.haradan.common.mapper.BaseMapper;
import com.haradan.domain.controller.request.ArticleRequest;
import com.haradan.domain.controller.response.ArticleResponse;
import com.haradan.domain.dao.entity.Article;

@Mapper(componentModel = "spring")
public interface ArticleMapper extends BaseMapper<Article, ArticleRequest, ArticleResponse> {
    @Mapping(target = "createDate", expression = "java(entity.getCreatedDate().toLocalDate())")
    ArticleResponse toResponse(Article entity);
}
