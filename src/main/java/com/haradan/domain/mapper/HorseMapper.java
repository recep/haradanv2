package com.haradan.domain.mapper;

import com.haradan.common.mapper.BaseMapper;
import com.haradan.domain.controller.request.AdvertRequest;
import com.haradan.domain.controller.response.AdvertResponse;
import com.haradan.domain.dao.entity.horse.Horse;
import com.haradan.domain.dao.entity.horse.HorsePedigri;
import com.haradan.domain.dao.entity.horse.HorseSibling;
import com.haradan.domain.dao.entity.horse.HorseStatistic;
import com.haradan.domain.dto.horse.HorseDto;
import com.haradan.domain.dto.horse.HorsePedigriDto;
import com.haradan.domain.dto.horse.HorseSiblingDto;
import com.haradan.domain.dto.horse.HorseStatisticDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface HorseMapper extends BaseMapper<Horse, AdvertRequest, AdvertResponse> {

    List<HorseDto> toHorse(List<Horse> entity);

    HorseDto toHorse(Horse entity);

    List<HorseStatisticDto> toHorseStatistic(List<HorseStatistic> entities);

    HorseStatisticDto toHorseStatistic(HorseStatistic entity);

    List<HorseSiblingDto> toHorseSibling(List<HorseSibling> entities);

    HorseSiblingDto toHorseSibling(HorseSibling entity);

    HorsePedigriDto toHorsePedigri(HorsePedigri entity);
}
