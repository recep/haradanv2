package com.haradan.domain.mapper;

import com.haradan.common.mapper.BaseMapper;
import com.haradan.domain.controller.request.StableRequest;
import com.haradan.domain.controller.response.StableResponse;
import com.haradan.domain.dao.entity.Stable;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface StableMapper extends BaseMapper<Stable, StableRequest, StableResponse> {
}
