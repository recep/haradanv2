package com.haradan.domain.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.haradan.common.mapper.BaseMapper;
import com.haradan.domain.controller.request.BannerRequest;
import com.haradan.domain.controller.response.BannerResponse;
import com.haradan.domain.dao.entity.Banner;

@Mapper(componentModel = "spring")
public interface BannerMapper extends BaseMapper<Banner, BannerRequest, BannerResponse> {
	
    @Mapping(target = "createDate", expression = "java(entity.getCreatedDate().toLocalDate())")
    BannerResponse toResponse(Banner entity);
}
