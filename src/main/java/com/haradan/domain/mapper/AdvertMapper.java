package com.haradan.domain.mapper;

import com.haradan.common.mapper.BaseMapper;
import com.haradan.domain.controller.request.AdvertDopingRequest;
import com.haradan.domain.controller.request.AdvertPropertyRequest;
import com.haradan.domain.controller.request.AdvertRequest;
import com.haradan.domain.controller.response.AdvertDetailResponse;
import com.haradan.domain.controller.response.AdvertResponse;
import com.haradan.domain.controller.response.AdvertSimpleResponse;
import com.haradan.domain.controller.response.DopingResponse;
import com.haradan.domain.dao.entity.Advert;
import com.haradan.domain.dao.entity.AdvertProperty;
import com.haradan.domain.dao.entity.Category;
import com.haradan.domain.dao.entity.Doping;
import com.haradan.domain.dto.AdvertPropertyDto;
import com.haradan.domain.dto.CategoryDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", imports = {UserMapper.class})
public interface AdvertMapper extends BaseMapper<Advert, AdvertRequest, AdvertResponse> {

    @Mapping(target = "imageUrl", source = "media.imageUrl")
    @Mapping(target = "createDate", expression = "java(entity.getCreatedDate().toLocalDate())")
    AdvertResponse toResponse(Advert entity);

    @Mapping(target = "imageUrl", source = "media.imageUrl")
    AdvertSimpleResponse toSimpleResponse(Advert entity);

    CategoryDto toDto(Category category);

    @Mapping(target = "category.identifier", source = "category")
    Advert toEntity(AdvertRequest request);

    Doping toEntity(AdvertDopingRequest request);

    List<Doping> toDopings(List<AdvertDopingRequest> request);

    @Mapping(target = "property.identifier", source = "property")
    AdvertProperty toEntity(AdvertPropertyRequest request);

    List<AdvertProperty> toAdvertProperties(List<AdvertPropertyRequest> request);

    @Mapping(target = "imageUrl", source = "media.imageUrl")
    @Mapping(target = "createDate", expression = "java(entity.getCreatedDate().toLocalDate())")
    AdvertDetailResponse toDetailResponse(Advert entity);

    @Mapping(target = "name", source = "property.name")
    @Mapping(target = "type", source = "property.type")
    @Mapping(target = "propertyId", source = "property.identifier")
    AdvertPropertyDto toDto(AdvertProperty advertProperty);

    List<AdvertPropertyDto> toPropertyDtos(List<AdvertProperty> advertProperties);

    @Mapping(target = "name", source = "doping.name")
    @Mapping(target = "desc", source = "doping.desc")
    @Mapping(target = "image", source = "doping.image")
    @Mapping(target = "price", source = "doping.price")
    @Mapping(target = "shortName", source = "doping.shortName")
    @Mapping(target = "searchText", source = "doping.searchText")
    DopingResponse toDto(Doping doping);

    List<DopingResponse> toDopingDtos(List<Doping> dopings);

}
