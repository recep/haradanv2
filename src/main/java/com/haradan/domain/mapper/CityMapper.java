package com.haradan.domain.mapper;

import com.haradan.common.mapper.BaseMapper;
import com.haradan.domain.controller.request.CityRequest;
import com.haradan.domain.controller.response.CityResponse;
import com.haradan.domain.dao.entity.City;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CityMapper extends BaseMapper<City, CityRequest, CityResponse> {
}
