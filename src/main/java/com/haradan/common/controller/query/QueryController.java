package com.haradan.common.controller.query;

import com.haradan.common.controller.query.dto.PageRequestDto;
import com.haradan.common.controller.query.dto.PagedResponse;
import com.haradan.common.controller.query.dto.SearchRequestDto;
import com.haradan.common.controller.request.BaseRequest;
import com.haradan.common.controller.response.BaseResponse;
import com.haradan.common.dao.entity.IdEntity;
import com.haradan.common.exception.exceptions.MicroException;
import com.haradan.common.mapper.BaseMapper;
import io.github.perplexhub.rsql.RSQLJPASupport;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


public abstract class QueryController<Entity extends IdEntity, Id extends Serializable, Request extends BaseRequest, Response extends BaseResponse>
        implements Controller<Entity, Id> {

    protected abstract BaseMapper<Entity, Request, Response> getMapper();

    @GetMapping(
            name = "Query resource by id",
            path = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Transactional(readOnly = true)
    public ResponseEntity<Response> getById(@PathVariable("id") Id id) {
        Optional<Entity> one = getRepository().findById(id);

        Response response = one.map(entity -> getMapper().toResponse(entity))
                .orElseThrow(() -> new MicroException(5L, "Resource Id:" + id));
        return ResponseEntity.ok(response);
    }

    @PostMapping(
            name = "Query resources by post request",
            value = "/v2/search",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public ResponseEntity<PagedResponse<Response>> post(
            @Valid @RequestBody SearchRequestDto requestDto
    ) {
        Page<Entity> page = getResults(requestDto.getFilter(), toPageRequest(requestDto.getPageRequest()));
        PagedResponse<Response> pagedResponse = toPagedResponse(page);
        return ResponseEntity.ok(pagedResponse);
    }

    @GetMapping(
            name = "Query filter in Rsql syntax, Reference: https://github.com/jirutka/rsql-parser#examples",
            value = "/v2/search",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @Transactional(readOnly = true)
    @PreAuthorize("hasAuthority('{authority=ROLE_ADMIN}')")
    public ResponseEntity<PagedResponse<Response>> get(
            @RequestParam(value = "filter", required = false) String filter,
            PageRequestDto pageRequestDto
    ) {
        Page<Entity> page = getResults(filter, toPageRequest(pageRequestDto));
        PagedResponse<Response> pagedResponse = toPagedResponse(page);
        return ResponseEntity.ok(pagedResponse);
    }

    private Page<Entity> getResults(String filter, Pageable pageable) {
        if (StringUtils.isBlank(filter)) {
            return getRepository().findAll(pageable);
        }
        Specification<Entity> specification = RSQLJPASupport.toSpecification(filter);
        return getRepository().findAll(specification, pageable);
    }

    PageRequest toPageRequest(PageRequestDto requestDto) {
        List<Sort.Order> orders = requestDto.getSort()
                .stream()
                .map(order -> new Sort.Order(
                        order.getDirection(),
                        order.getProperty()
                )).collect(Collectors.toList());

        return PageRequest.of(requestDto.getPage(), requestDto.getSize(), Sort.by(orders));
    }

    private PagedResponse<Response> toPagedResponse(Page<Entity> page) {
        List<Response> contentDtos = page.getContent().stream()
                .map(entity -> getMapper().toResponse(entity))
                .collect(Collectors.toList());

        PagedResponse.Page pageDto = PagedResponse.Page.builder()
                .number(page.getNumber())
                .size(page.getSize())
                .totalElements(page.getTotalElements())
                .totalPages(page.getTotalPages())
                .build();

        return PagedResponse.<Response>builder()
                .content(contentDtos)
                .page(pageDto)
                .build();
    }
}