package com.haradan.common.controller.query.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SearchRequestDto {

    private String filter;

    @Builder.Default
    private PageRequestDto pageRequest = PageRequestDto.builder().build();
}
