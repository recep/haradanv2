package com.haradan.common.controller.query.dto;

import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;


@Data
@Builder
public class PagedResponse<R> {

    @Builder.Default
    private List<R> content = new ArrayList<>();

    private Page page;


    @Data
    @Builder
    public static class Page {

        private int number;

        private int size;

        private long totalElements;

        private int totalPages;

    }

}
