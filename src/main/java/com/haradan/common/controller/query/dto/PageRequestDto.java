package com.haradan.common.controller.query.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PageRequestDto {

    @Builder.Default
    int page = 0;

    @Builder.Default
    int size = 10;

    @Builder.Default
    List<PageOrderDto> sort = new ArrayList<>();

}