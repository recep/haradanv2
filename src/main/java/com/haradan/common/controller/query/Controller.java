package com.haradan.common.controller.query;

import com.haradan.common.dao.repository.BaseRepository;

import java.io.Serializable;

public interface Controller<E, I extends Serializable> {

    BaseRepository<E, I> getRepository();

}
