package com.haradan.common.controller.response;

import lombok.Data;

import java.io.Serializable;

@Data
public class JsonResponse<T> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3607224717845776041L;

	private int code;

	private String message;

	private T result;

}
