package com.haradan.common.controller.response;

import lombok.Data;

import java.io.Serializable;

@Data
public class BaseResponse implements Serializable {
	private static final long serialVersionUID = -7850717198720734352L;
	private String identifier;
}
