package com.haradan.common.controller.request;

import lombok.Data;

import java.io.Serializable;

@Data
public class BaseRequest implements Serializable {
	private static final long serialVersionUID = 3234823955403791695L;
}
