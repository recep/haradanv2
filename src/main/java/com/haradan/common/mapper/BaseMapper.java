package com.haradan.common.mapper;

import com.haradan.common.controller.request.BaseRequest;
import com.haradan.common.controller.response.BaseResponse;
import com.haradan.common.dao.entity.IdEntity;

public interface BaseMapper<Entity extends IdEntity, Request extends BaseRequest, Response extends BaseResponse> {

    Response toResponse(Entity entity);

    Entity toEntity(Request request);
    
}
