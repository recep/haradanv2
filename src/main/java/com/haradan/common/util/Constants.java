package com.haradan.common.util;

public interface Constants {
    String empty = "";
    String space = " ";
    String slash = "/";
    String hyphen = "-";
    String comma = ",";
    String dat = ".";
    String percentage = "%";
    String reagent = " | ";

    String DIRECTION_DESC = "DESC";
    String CREATED_DATE = "createdDate";
    String LOGIN_WITH_REF = "/giris-yap?ref=";
    String REDIRECT = "redirect:/";
    String REDIRECT_LOGIN_WITH_REF = "redirect:/giris-yap?ref=";
    String REDIRECT_POST_AD_CAT_SELECT = "redirect:/ilan-ver/kategori-secim";
    String REDIRECT_UPDATE_PHONE_NUMBER = "redirect:/uyelik-bilgilerim?ref=ilan-ver";
    String REDIRECT_POST_AD_AD_INFO = "redirect:/ilan-ver/ilan-bilgileri";
    String REDIRECT_ADMIN_PANEL = "redirect:/admin/panelim";
    String DEFAULT_PASSWORD = "haraP";
    String OG_TITLE = "ogTitle";
    String SATILIK_YARIS_ATI = "satilik-yaris-ati";
    String SATILIK_KISRAK = "satilik-kisrak";
    String RACE = "at-irki";
    String PREGNANT = "gebe";
    String SEX = "cinsiyet";
    String FATHER_NAME = "baba-adi";
    String MOTHER_NAME = "anne-adi";
    String FATHER_NAME_OF_MOTHER = "annesinin-baba-adi";

    String STEP_KATEGORI_SECIM = "kategori-secim";
    String STEP_ILAN_BILGILERI = "ilan-bilgileri";
    String STEP_ODEME_YAP = "odeme-yap";
    String STEP_SONUC = "sonuc";

    String ALL_FATHERS = "tum-aygirlar";
    String ALL_MOTHERS = "tum-kisraklar";
    String ALL_FATHERS_OF_MOTHERS = "tum-kisrak-babalari";
}