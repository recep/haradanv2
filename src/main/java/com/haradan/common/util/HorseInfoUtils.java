package com.haradan.common.util;

import com.haradan.common.enumeration.EntityStatus;
import com.haradan.common.enumeration.horse.PistType;
import com.haradan.domain.dao.entity.horse.Horse;
import com.haradan.domain.dao.entity.horse.HorseSibling;
import com.haradan.domain.dao.entity.horse.HorseStatistic;
import com.haradan.domain.dao.entity.horse.HorseSummary;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;

import static com.haradan.common.util.Constants.comma;
import static com.haradan.common.util.Constants.dat;

public class HorseInfoUtils {
    public static final String stringPattern = "dd.MM.yyyy";
    public static final String T = "t";

    public static String getAge(String str) {
        String age = null;
        if (str != null) {
            age = str.replaceAll("[^0-9]", StringUtils.EMPTY);
        }
        return age;
    }

    public static String getDonuKey(String str) {
        if (StringUtils.isNotBlank(str)) {
            str = str.replaceAll("[^a-zA-Z]", StringUtils.EMPTY);
            str = StringUtils.substring(str, str.length() - 2, str.length() - 1);
        }
        return str;
    }

    public static String getGenderKey(String str) {
        if (StringUtils.isNotBlank(str)) {
            str = str.replaceAll("[^a-zA-Z]", StringUtils.EMPTY);
            str = StringUtils.substring(str, str.length() - 1);
        }
        return str;
    }

    public static String getFatherName(Element element) {
        try {
            return element.select("a[href]").get(0).text();
        } catch (Exception e) {
            return StringUtils.EMPTY;
        }
    }

    public static String getMotherName(Element element) {
        try {
            return element.select("a[href]").get(1).text();
        } catch (Exception e) {
            return StringUtils.EMPTY;
        }
    }

    public static Long getEarning(String value) {
        if (StringUtils.isEmpty(value) || value.equals(dat)) {
            return 0L;
        }
        value = value.contains(dat) ? value.replace(dat, StringUtils.EMPTY) : value;
        value = value.contains(comma) ? value.substring(0, value.indexOf(comma)) : value;
        value = value.contains(T) ? value.replace(T, StringUtils.EMPTY) : value;
        return (Long) Utils.toObject(Long.class, value.trim());
    }

    public static void setName(String value, Horse horseInfo) {
        horseInfo.setStatus(EntityStatus.ACTIVE);
        horseInfo.setName(value);
        if (value.contains("(Öldü)")) {
            horseInfo.setStatus(EntityStatus.DELETED);
            horseInfo.setName(value.replace("(Öldü)", StringUtils.EMPTY));
        }
    }

    public static void mapHorseStatistics(int index, String value, HorseStatistic horseStatistic) {
        if (index == 0) {
            if (value.contains("Yılı")) {
                horseStatistic.setYear((Integer) Utils.toObject(Integer.class, value.split(" ")[0]));
            }
            horseStatistic.setPistType(PistType.getPist(value));
        } else if (index == 1) {
            horseStatistic.setRaceCount((Integer) Utils.toObject(Integer.class, value));
        } else if (index == 2) {
            horseStatistic.setFirst((Integer) Utils.toObject(Integer.class, value));
        } else if (index == 3) {
            horseStatistic.setSecond((Integer) Utils.toObject(Integer.class, value));
        } else if (index == 4) {
            horseStatistic.setThird((Integer) Utils.toObject(Integer.class, value));
        } else if (index == 5) {
            horseStatistic.setFourth((Integer) Utils.toObject(Integer.class, value));
        } else if (index == 6) {
            horseStatistic.setFifth((Integer) Utils.toObject(Integer.class, value));
        } else if (index == 7) {
            horseStatistic.setEarning(getEarning(value));
        }
    }

    public static void mapHorseSibling(int index, String value, HorseSibling horseSibling) {
        if (index == 0) {
            horseSibling.setSiblingName(value);
        } else if (index == 1) {
            horseSibling.setFatherName(value);
        } else if (index == 2) {
            horseSibling.setRaceCount((Integer) Utils.toObject(Integer.class, value));
        } else if (index == 3) {
            horseSibling.setFirst((Integer) Utils.toObject(Integer.class, value));
        } else if (index == 4) {
            horseSibling.setSecond((Integer) Utils.toObject(Integer.class, value));
        } else if (index == 5) {
            horseSibling.setThird((Integer) Utils.toObject(Integer.class, value));
        } else if (index == 6) {
            horseSibling.setFourth((Integer) Utils.toObject(Integer.class, value));
        } else if (index == 7) {
            horseSibling.setEarning(getEarning(value));
        }
    }

    public static void mapToHorseInfo(String key, String value, HorseSummary horseSummary) {
        if ("sorgu-Atlar-AtIsmi".equals(key)) {
            horseSummary.setName(value);
        }
        if ("sorgu-Atlar-IrkAdi".equals(key))
            horseSummary.setRace(value);
        if ("sorgu-Atlar-BabaAdi".equals(key))
            horseSummary.setFatherName(value);
        if ("sorgu-Atlar-AnneAdi".equals(key))
            horseSummary.setMotherName(value);
    }
}
