package com.haradan.common.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.haradan.common.dao.entity.IdEntity;
import com.haradan.domain.dto.SearchDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static com.haradan.common.util.Constants.STEP_ILAN_BILGILERI;
import static com.haradan.common.util.Constants.STEP_KATEGORI_SECIM;
import static com.haradan.common.util.Constants.STEP_ODEME_YAP;
import static com.haradan.common.util.Constants.STEP_SONUC;

@Slf4j
@Component
public class Utils {

    public static ObjectMapper mapper = new ObjectMapper();

    @SuppressWarnings("unchecked")
    public static <T> Collector<T, ?, List<T>> toShuffledList() {
        return (Collector<T, ?, List<T>>) SHUFFLER;
    }

    private static final Collector<?, ?, ?> SHUFFLER = Collectors.collectingAndThen(
            Collectors.toCollection(ArrayList::new),
            list -> {
                Collections.shuffle(list);
                return list;
            }
    );

    public static SearchDto getProperties(Map<String, String> params) {
        params.keySet().removeIf(p -> in(p, "page", "size", "sort", "direction", "id", "searchText", "status", "date",
                "hasPhoto", "minPrice", "maxPrice", "city", "district", "userId"));
        return SearchDto.builder().properties(params).build();
    }

    public static String changeOrder(String str, String part) {
        if (StringUtils.isNotEmpty(part) && StringUtils.isNotEmpty(part) && !str.startsWith(part)) {
            str = part.concat(Constants.comma + str).replace(Constants.comma + part, Constants.empty);
        }
        return str;
    }

    public static void createPath(Path root) throws IOException {
        if (!Files.isDirectory(root)) {
            Files.createDirectories(root);
        }
    }

    public static String getExternalIp(HttpServletRequest request, String baseUrl) {
        URL whatismyip;
        String externalIp = null;
        try {
            if (baseUrl.contains("localhost")) {
                whatismyip = new URL("http://checkip.amazonaws.com");
                BufferedReader in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));
                externalIp = in.readLine();
                in.close();
            } else { // local
                externalIp = request.getHeader("X-FORWARDED-FOR");
                externalIp = StringUtils.isEmpty(externalIp) ? request.getRemoteAddr() : externalIp;
            }
        } catch (Exception e) {
            log.error("IP Address alinirken hata olustu", e);
        }
        return externalIp;
    }

    public static String hashMD5(String userName, String password) {
        return DigestUtils.md5DigestAsHex((StringUtils.lowerCase(userName) + password).getBytes());
    }

    public static boolean isValidEmailAddress(String email) {
        try {
            new InternetAddress(email).validate();
            return true;
        } catch (AddressException ex) {
            return false;
        }
    }

    public static <T> T unserialize(String json, Class<T> clazz) {
        try {
            return mapper.readValue(json, clazz);
        } catch (Exception e) {
            log.error("unserialize got exception", e);
            return null;
        }
    }

    public static String getshortPhoneNumber(String phoneNumber) {
        if (StringUtils.isNotEmpty(phoneNumber)) {
            phoneNumber = phoneNumber.replace("(", "").replace(")", "").replace("-", "").replace(" ", "");
        }
        return phoneNumber;
    }

    private static String replaceTurkishChars(String str) {
        String result = "";
        if (StringUtils.isNotBlank(str)) {
            result = str.replaceAll("Ç", "C").replaceAll("ç", "c").replaceAll("Ö", "O").replaceAll("ö", "o")
                    .replaceAll("ı", "i").replaceAll("İ", "I").replaceAll("ş", "s").replaceAll("Ş", "S")
                    .replaceAll("Ğ", "G").replaceAll("ğ", "g").replaceAll("Ü", "U").replaceAll("ü", "u");
        }
        return result;
    }

    private static String replaceSpecialChars(String str) {
        String result = "";
        if (StringUtils.isNotBlank(str)) {
            result = str.replaceAll(" ", "-").replaceAll("\\+", "-").replaceAll("/", "-").replaceAll("\\?", "")
                    .replaceAll("\\'", "");
        }
        return result;
    }

    public static int random() {
        return (int) (Math.random() * 100);
    }

    public static String uniqueFileName(String fileName) {
        return Utils.normalizeCharacters(System.currentTimeMillis() + random() + fileName.substring(fileName.lastIndexOf(Constants.dat)));
    }

    public static String normalizeCharacters(String str) {
        String result = "";
        if (StringUtils.isNotBlank(str)) {
            result = Utils.replaceSpecialChars(Utils.replaceTurkishChars(str.toLowerCase()));
        }
        return result;
    }

    public static void redirect(HttpServletResponse response, int status, String url) {
        response.setStatus(status);
        response.setHeader("Location", url);
        response.setHeader("Connection", "close");
    }

    public static InputStream callRestService(String targetUrl) throws IOException {
        URL url = new URL(targetUrl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");
        OutputStream os = conn.getOutputStream();
        os.flush();
        conn.disconnect();
        return conn.getInputStream();
    }

    public static <T> T call(String url, Class<T> responseType) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Connect-Type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(headers);
        try {
            ResponseEntity<T> response = restTemplate.postForEntity(url, entity, responseType);
            return response.getBody();
        } catch (Exception e) {
            log.error("An exception occured on Service call ", e);
        }
        return null;
    }

    public static boolean in(String obj, String... list) {
        if (StringUtils.isEmpty(obj)) {
            return false;
        }
        for (String item : list) {
            if (obj.equals(item)) {
                return true;
            }
        }
        return false;
    }

    public static boolean notIn(String obj, String... list) {
        return !in(obj, list);
    }

    public static String removeChar(String str, Integer count) {
        if (StringUtils.isEmpty(str) || count == null || str.length() <= count) {
            return str;
        }
        return str.substring(0, str.length() - count);
    }

    public static Object toObject(Class clazz, String value) {
        if (StringUtils.isEmpty(value)) {
            return null;
        } else {
            value = value.trim();
        }
        if (Boolean.class == clazz || Boolean.TYPE == clazz)
            return Boolean.parseBoolean(value);
        if (Byte.class == clazz || Byte.TYPE == clazz)
            return Byte.parseByte(value);
        if (Short.class == clazz || Short.TYPE == clazz)
            return Short.parseShort(value);
        if (Integer.class == clazz || Integer.TYPE == clazz)
            return Integer.parseInt(value);
        if (Long.class == clazz || Long.TYPE == clazz)
            return Long.parseLong(value);
        if (Float.class == clazz || Float.TYPE == clazz)
            return Float.parseFloat(value);
        if (Double.class == clazz || Double.TYPE == clazz)
            return Double.parseDouble(value);
        return value;
    }

    public static <T> String getJson(T data) throws JsonProcessingException {
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(data);
    }


    public static <T extends IdEntity> T checkEntity(T entity) {
        return (entity == null || entity.getIdentifier() == null) ? null : entity;
    }

    public static String getDeviceType(Device device) {
        String deviceType = null;
        if (device.isMobile()) {
            deviceType = "mobile";
        } else if (device.isTablet()) {
            deviceType = "tablet";
        } else {
            deviceType = "desktop";
        }
        return deviceType;
    }

    public static boolean isValidPostAdStep(String step) {
        return STEP_KATEGORI_SECIM.equals(step)
                || STEP_ILAN_BILGILERI.equals(step)
                || STEP_ODEME_YAP.equals(step)
                || STEP_SONUC.equals(step);
    }

    public static String generateRandomText(int length) {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        Random random = new Random();

        return random.ints(leftLimit, rightLimit + 1)
                .limit(length)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }
}
