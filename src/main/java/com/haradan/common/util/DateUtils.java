package com.haradan.common.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Slf4j
@Component
public class DateUtils {
    private static final String shortPattern = "dd/MM/yyyy";
    private static final String stringPattern = "yyyy-MM-dd";

    public static Date toShortDate(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat(shortPattern);
        try {
            return formatter.parse(formatter.format(date));
        } catch (ParseException e) {
            log.error("DateUtil parse error");
        }
        return null;
    }

    public static Date toShortDate(String dateString, String pattern) {
        if (!StringUtils.isEmpty(dateString)) {
            DateFormat stringFormatter = new SimpleDateFormat(pattern != null ? pattern : stringPattern);
            DateFormat formatter = new SimpleDateFormat(shortPattern);
            try {
                Date date = stringFormatter.parse(dateString);
                return formatter.parse(formatter.format(date));
            } catch (ParseException e) {
                log.error("DateUtil parse error", e);
            }
        }
        return null;
    }

	public static String toString(Date date, String pattern) {
		if (date != null) {
			DateFormat formatter = new SimpleDateFormat(pattern != null ? pattern : stringPattern);
			return formatter.format(date);
		}
		return null;
	}
    
    public static boolean smallerThan(Date date1, Date date2) {
        return date1.compareTo(date2) == -1;
    }

    public static boolean greaterThan(Date date1, Date date2) {
        return date1.compareTo(date2) == 1;
    }

    /** Checks the value is greater than or equal to the given date or not */
    public static boolean greaterThanEqualTo(Date date1, Date date2) {
        return greaterThan(date1, date2) || date1.compareTo(date2) == 0;
    }

    /** Checks the value is smaller than or equal to the given date or not */
    public static boolean smallerThanEqualTo(Date date1, Date date2) {
        return smallerThan(date1, date2) || date1.compareTo(date2) == 0;
    }

    /** check date is bettween to given max and min date */
    public static boolean between(Date date, Date minDate, Date maxDate, boolean exclusive) {
        if (date == null || minDate == null || maxDate == null)
            return false;
        if (exclusive)
            return (greaterThan(date, minDate) && smallerThan(date, maxDate));
        else
            return (greaterThanEqualTo(date, minDate) && smallerThanEqualTo(date, maxDate));
    }

    /** add or minus particular count of day from given date */
    public static Date addDay(Date date, int numberOfDays) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, numberOfDays);
        return calendar.getTime();
    }

    public static Date getTime(Long millis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);
        return calendar.getTime();
    }

    public static Date getTimeStamp(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.getTime();
    }
}
