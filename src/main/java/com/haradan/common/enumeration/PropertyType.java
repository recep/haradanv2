package com.haradan.common.enumeration;

public enum PropertyType {
    text,
    number,
    checkbox,
    date,
    select,
    radio,
    yesNo;
}
