package com.haradan.common.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum DopingType {
    EMERGENCY("Acil Satılık", 100, "<b>'Acil İlanlar'</b> sayfasında 1 ay yayınlansın.", "acil.png", "EMERGENCY", "acil-ilanlar"),
    MAINPAGE_SHOW_CASE("Vitrin İlanı", 100, "Anasayfa <b>'Vitrin İlanları'</b> bölümünde 1 ay yayınlansın.", "vitrin.png", "MAINPAGE_SHOW_CASE", "vitrin-ilanlari"),
    TOP_ORDER("Üst Sırada", 100, "Arama sayfasında <b>'Üst Sırada'</b> 1 ay yayınlansın.", "ustsirada.png", "TOP_ORDER", "ust-sira-ilanlari"),
    SOCIAL("Sosyal Medya", 100, "<b>'Sosyal Medya'</b> hesaplarımızda paylaşılsın.", "vitrin.png", "SOCIAL", null),
    UP_TO_DATE("Güncelim", 50, "İlan tarihiniz güncellensin.", "ustsirada.png", "UP_TO_DATE", null);

    private String name;
    private Integer price;
    private String desc;
    private String image;
    private String shortName;
    private String searchText;

    public static DopingType of(String searchText) {
        return Arrays.stream(DopingType.values()).filter(d -> d.searchText.equals(searchText)).findFirst().get();
    }

    public int getDayOfDuration() {
        return (SOCIAL.equals(this) || UP_TO_DATE.equals(this)) ? 1 : 30;
    }

}