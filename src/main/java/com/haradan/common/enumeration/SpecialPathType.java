package com.haradan.common.enumeration;

import com.haradan.common.controller.response.LookupDto;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

import static com.haradan.common.util.Constants.SATILIK_KISRAK;
import static com.haradan.common.util.Constants.SATILIK_YARIS_ATI;
import static java.util.Arrays.stream;

@RequiredArgsConstructor
public enum SpecialPathType {
    SATILIK_TAYLAR("satilik-taylar", SATILIK_YARIS_ATI, "Satılık Taylar", "Tay"),
    IDMANDA_ATLAR("idmanda-atlar", SATILIK_YARIS_ATI, "İdmanda Atlar", "İdmanda"),
    KIRALIK_ATLAR("kiralik-atlar", SATILIK_YARIS_ATI, "Kiralık Atlar", "Kiralık"),
    INGILIZ_TAYLAR("ingiliz-taylar", SATILIK_YARIS_ATI, "İngiliz Taylar", "Tay"),
    ARAP_TAYLAR("arap-taylar", SATILIK_YARIS_ATI, "Arap Taylar", "Tay"),
    KOSAR_DURUMDA_INGILIZ_ATI("kosar-durumda-ingiliz-ati", SATILIK_YARIS_ATI, "Koşar Durumda İngiliz Atları", "Koşar durumda"),
    KOSAR_DURUMDA_ARAP_ATI("kosar-durumda-arap-ati", SATILIK_YARIS_ATI, "Koşar Durumda Arap Atları", "Koşar durumda"),
    INGILIZ_KISRAKLAR("ingiliz-kisraklar", SATILIK_KISRAK, "İngiliz Kısraklar", "Kısrak"),
    ARAP_KISRAKLAR("arap-kisraklar", SATILIK_KISRAK, "Arap Kısraklar", "Kısrak"),
    GEBE_KISRAKLAR("gebe-kisraklar", SATILIK_KISRAK, "Gebe Kısraklar", "Gebe"),
    BOS_KISRAKLAR("bos-kisraklar", SATILIK_KISRAK, "Boş Kısraklar", "Kısrak");

    @Getter
    private final String path;
    @Getter
    private final String categorySearchText;
    @Getter
    private final String title;
    @Getter
    private final String shortName;

    public static SpecialPathType of(String specialPath) {
        return stream(SpecialPathType.values())
                .filter(p -> p.getPath().equals(specialPath))
                .findFirst()
                .orElse(null);
    }

    public static List<LookupDto> mainpageCategories() {
        List<LookupDto> lookupDtos = new ArrayList<>();
        lookupDtos.add(new LookupDto(SATILIK_TAYLAR.getCategorySearchText(), SATILIK_TAYLAR.getShortName()));
        lookupDtos.add(new LookupDto(IDMANDA_ATLAR.getCategorySearchText(), IDMANDA_ATLAR.getShortName()));
        lookupDtos.add(new LookupDto(KIRALIK_ATLAR.getCategorySearchText(), KIRALIK_ATLAR.getShortName()));
        lookupDtos.add(new LookupDto(KOSAR_DURUMDA_INGILIZ_ATI.getCategorySearchText(), KOSAR_DURUMDA_INGILIZ_ATI.getShortName()));
        return lookupDtos;
    }
}
