package com.haradan.common.enumeration;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

@RequiredArgsConstructor
public enum BannerType {
    SLIDER("slider", "Slider", true),
    CORPORATE_ADVERT("ekuri-ilani", "Eküri İlanı", true);

    @Getter
    private final String name;
    @Getter
    private final String desc;
    @Getter
    private final boolean repeated;

    public static String getDesc(BannerType bannerType) {
        return Arrays.stream(BannerType.values())
                .filter(p -> p.equals(bannerType))
                .findFirst()
                .map(BannerType::toString)
                .orElse(null);
    }
}
