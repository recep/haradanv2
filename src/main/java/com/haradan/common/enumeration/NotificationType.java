package com.haradan.common.enumeration;

import com.haradan.domain.dao.entity.Advert;
import com.haradan.domain.dao.entity.Contact;
import com.haradan.domain.dao.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

import static com.haradan.common.enumeration.EntityStatus.ACTIVE;
import static com.haradan.common.enumeration.EntityStatus.DEFAULT;
import static com.haradan.common.enumeration.EntityStatus.DELETED;
import static com.haradan.common.enumeration.EntityStatus.NOT_COMPLETED;
import static com.haradan.common.enumeration.EntityStatus.PASSIVE;
import static com.haradan.common.enumeration.EntityStatus.REJECTED;
import static com.haradan.common.enumeration.EntityStatus.WAITING_APPROVAL;

@Getter
@AllArgsConstructor
public enum NotificationType {
    ADVERT_START(NOT_COMPLETED, NOT_COMPLETED, MailType.ADVERT_START, null, Advert.class),
    ADVERT_CREATE(NOT_COMPLETED, WAITING_APPROVAL, MailType.ADVERT_CREATE, null, Advert.class),
    ADVERT_UPDATE(ACTIVE, WAITING_APPROVAL, MailType.ADVERT_UPDATE, null, Advert.class),
    ADVERT_APPROVE(WAITING_APPROVAL, ACTIVE, MailType.ADVERT_APPROVE, null, Advert.class),
    ADVERT_REJECT(WAITING_APPROVAL, REJECTED, MailType.ADVERT_REJECT, null, Advert.class),
    ADVERT_PASSIVE(DEFAULT, PASSIVE, MailType.ADVERT_PASSIVE, null, Advert.class),
    ADVERT_DELETE(DEFAULT, DELETED, MailType.ADVERT_DELETE, null, Advert.class),
    ADVERT_DOPING(DEFAULT, DEFAULT, MailType.ADVERT_DOPING, null, Advert.class),
    ADVERT_DOPING_EXPIRE(DEFAULT, DEFAULT, MailType.ADVERT_DOPING_EXPIRE, null, Advert.class),
    CONTACT(DEFAULT, DEFAULT, MailType.CONTACT, null, Contact.class),
    REGISTRATION(DEFAULT, DEFAULT, MailType.REGISTRATION, null, User.class),
    PASSWORD_CHANGE(DEFAULT, DEFAULT, MailType.PASSWORD_CHANGE, null, User.class),
    CREATE_NEW_PASSWORD(DEFAULT, DEFAULT, MailType.CREATE_NEW_PASSWORD, null, User.class),
    PASSWORD_FORGOT(DEFAULT, DEFAULT, MailType.PASSWORD_FORGOT, SmsType.PASSWORD_FORGOT, User.class),
    ADVERT_REMEMBER(DEFAULT, DEFAULT, null, SmsType.ADVERT_REMEMBER, Advert.class),
    DOPING_EXPIRATION(DEFAULT, ACTIVE, null, SmsType.DOPING_EXPIRATION, Advert.class);

    private EntityStatus oldStatus;
    private EntityStatus status;
    private MailType mailType;
    private SmsType smsType;
    private Class clazz;

    public static NotificationType of(EntityStatus oldStatus, EntityStatus status, Class clazz) {
        return Arrays.stream(NotificationType.values())
                .filter(s -> (s.oldStatus.equals(DEFAULT) || s.oldStatus.equals(oldStatus)) && s.status.equals(status) && s.clazz.equals(clazz))
                .findFirst()
                .orElse(null);
    }
}
