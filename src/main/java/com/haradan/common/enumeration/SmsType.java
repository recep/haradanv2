package com.haradan.common.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SmsType {
    ADVERT_REMEMBER("Satılık atlarınız için Haradan.com'da ilan vermek ister misiniz."),
    DOPING_EXPIRATION("%s nolu ilanınıza yapılan dopinglerin süresi bitti."),
    PASSWORD_FORGOT("Haradan.com yeni şifre belirlemek için tıklayınız.");

    private String text;

    public String getText(String advertNo) {
        return String.format(this.text, advertNo);
    }

}
