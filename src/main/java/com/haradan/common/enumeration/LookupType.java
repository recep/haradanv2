package com.haradan.common.enumeration;

public enum LookupType {
	category,
	parameter,
	city,
	county,
	district
}