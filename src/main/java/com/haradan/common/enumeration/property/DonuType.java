package com.haradan.common.enumeration.property;

import com.haradan.common.controller.response.LookupDto;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum DonuType {
    DORU( "Doru"),
    AL( "Al"),
    KIR( "Kır"),
    BEYAZ( "Beyaz"),
    YAGIZ( "Yağız"),
    KULA( "Kula"),
    BOZ("Boz"),
    AHREC( "Ahreç"),
    OTHER("Diğer");

    private String desc;

    DonuType(String desc) {
        this.desc = desc;
    }

    public int getCode() {
        return this.ordinal();
    }

    public String getDesc() {
        return desc;
    }

    public static String getDesc(int code) {
        for (DonuType currentItem : DonuType.values()) {
            if (currentItem.ordinal() == code) {
                return currentItem.getDesc();
            }
        }
        return null;
    }

    public static List<LookupDto> getLookup() {
        return Stream.of(DonuType.values())
                .map(e -> {
                    return new LookupDto(String.valueOf(e.ordinal()), e.desc);
                })
                .collect(Collectors.toList());
    }
}
