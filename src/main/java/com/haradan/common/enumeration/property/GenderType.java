package com.haradan.common.enumeration.property;

import com.haradan.common.controller.response.LookupDto;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum GenderType {
    MALE("Erkek"),
    FEMALE("Dişi"),
    OTHER("Diğer");

    private String desc;

    GenderType(String desc) {
        this.desc = desc;
    }

    public static String getDesc(int code) {
        for (GenderType currentItem : GenderType.values()) {
            if (currentItem.ordinal() == code) {
                return currentItem.getDesc();
            }
        }
        return null;
    }

    public int getCode() {
        return this.ordinal();
    }

    public String getDesc() {
        return desc;
    }

    public static List<LookupDto> getLookup() {
        return Stream.of(GenderType.values())
                .map(e -> {
                    return new LookupDto(String.valueOf(e.ordinal()), e.desc);
                })
                .collect(Collectors.toList());
    }
}