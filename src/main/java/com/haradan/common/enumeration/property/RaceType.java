package com.haradan.common.enumeration.property;

import com.haradan.common.controller.response.LookupDto;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum RaceType {
    ARABIAN( "Arap"),
    ENGLISH( "İngiliz"),
    OTHER("Diğer");

    private String desc;

    RaceType(String desc) {
        this.desc = desc;
    }

    public int getCode() {
        return this.ordinal();
    }

    public String getDesc() {
        return desc;
    }

    public static String getDesc(int code) {
        for (RaceType currentItem : RaceType.values()) {
            if (currentItem.ordinal() == code) {
                return currentItem.getDesc();
            }
        }
        return null;
    }

    public static List<LookupDto> getLookup() {
        return Stream.of(RaceType.values())
                .map(e -> {
                    return new LookupDto(String.valueOf(e.ordinal()), e.desc);
                })
                .collect(Collectors.toList());
    }
}
