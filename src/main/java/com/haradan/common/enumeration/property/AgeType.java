package com.haradan.common.enumeration.property;

import com.haradan.common.controller.response.LookupDto;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum AgeType {
	ZERO( "0"),
	ONE( "1"),
    ONE_HALF( "1.5"),
    TWO( "2"),
    THREE( "3"),
    FOUR( "4"),
    FIVE( "5"),
    SIX("6"),
    SEVEN( "7"),
    EIGHT("8"),
    NINE("9"),
    TEN( "10-15 arası"),
    FIFTEEN("15 üzeri");

    private String desc;

    AgeType(String desc) {
        this.desc = desc;
    }

    public int getCode() {
        return this.ordinal();
    }

    public String getDesc() {
        return desc;
    }

    public static String getDesc(int code) {
        for (AgeType currentItem : AgeType.values()) {
            if (currentItem.ordinal() == code) {
                return currentItem.getDesc();
            }
        }
        return null;
    }

    public static List<LookupDto> getLookup() {
        return Stream.of(AgeType.values())
                .map(e -> {
                    return new LookupDto(String.valueOf(e.ordinal()), e.desc);
                })
                .collect(Collectors.toList());
    }
}
