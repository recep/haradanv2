package com.haradan.common.enumeration.horse;

public enum SiblingType {
	FATHER, MOTHER;
}
