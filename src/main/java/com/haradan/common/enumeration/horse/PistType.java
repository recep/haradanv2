package com.haradan.common.enumeration.horse;

public enum PistType {
	TOTAL("TOPLAM"),
	CIM("Çim"), 
	KUM("Kum"), 
	SENTETIK("Sentetik");
	private String key;

	PistType(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public static PistType getPist(String key) {
		for (PistType currentItem : PistType.values()) {
			if (currentItem.getKey().equals(key)) {
				return currentItem;
			}
		}
		return null;
	}
}
