package com.haradan.common.enumeration.horse;

public enum DonuType {
    DORU("d", "Doru"),
    AL("a", "Al"),
    KIR("k", "Kır");

    private String code;
    private String desc;

    DonuType(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static String getDesc(String code) {
        for (DonuType currentItem : DonuType.values()) {
            if (currentItem.getCode().equals(code)) {
                return currentItem.getDesc();
            }
        }
        return null;
    }
}
