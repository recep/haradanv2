package com.haradan.common.enumeration.horse;

public enum TjkUrlType {
		BULK_SEARCH("https://www.tjk.org/TR/YarisSever/Query/DataRows/Atlar?PageNumber=%s&Sort=AtIsmi&X-Requested-With=XMLHttpRequest"),
		SEARCH("https://www.tjk.org/TR/YarisSever/Query/Page/Atlar?QueryParameter_OLDUFLG=on&"),
		STATISTICS("https://www.tjk.org/TR/YarisSever/Query/ConnectedPage/AtKosuBilgileri?1=1&QueryParameter_AtId=%s"),
		PEDIGRI("https://www.tjk.org//TR/YarisSever/Query/Pedigri/Pedigri?Atkodu=%s"),
		FAMILY("https://www.tjk.org/TR/YarisSever/Query/Kardes/Kardes?Atkodu=%s"),
		STALLION("https://www.tjk.org/TR/YarisSever/Query/Page/AygirIstatistikleri?QueryParameter_AygirAdi=%s");

		private String url;

		private TjkUrlType(String url) {
			this.url = url;
		}

		public String getUrl() {
			return url;
		}

		public String getUrl(String... arg) {
			return String.format(url, arg);
		}
	}