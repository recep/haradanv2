package com.haradan.common.enumeration.horse;

public enum GenderType {
    YAS_2_3_DISI("d", "Dişi"), 
    YAS_4_UZERI_DISI("k", "Dişi"), 
    YAS_2_3_ERKEK("e", "Erkek"),
    YAS_4_UZERI_ERKEK("a", "Erkek"),
    IGDIS("g", "Diğer");
    private String code;
    private String desc;

    GenderType(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static String getGender(String code) {
        for (GenderType currentItem : GenderType.values()) {
            if (currentItem.getCode().equals(code)) {
                return currentItem.getDesc();
            }
        }
        return null;
    }
}
