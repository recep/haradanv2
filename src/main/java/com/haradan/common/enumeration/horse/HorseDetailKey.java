package com.haradan.common.enumeration.horse;

import com.haradan.common.util.DateUtils;
import com.haradan.common.util.HorseInfoUtils;
import com.haradan.common.util.Utils;
import com.haradan.domain.dao.entity.horse.Horse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import static com.haradan.common.util.HorseInfoUtils.getEarning;

@Slf4j
public enum HorseDetailKey {
	NAME("İsim"),
	AGE("Yaş"),
	BIRTH_DATE("Doğ. Trh"),
	HP("Handikap P."),
	FATHER("Baba"),
	MOTHER("Anne"),
	OWNER("Gerçek Sahip"),
	GROWER("Yetiştirici"),
	EARNING("Kazanç");

	private String key;

	HorseDetailKey(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public static void setdata(Horse horseInfo, String key, String value) {
		try {
			if (NAME.getKey().equals(key)) {
				HorseInfoUtils.setName(value, horseInfo);
			} else if (AGE.getKey().equals(key)) {
				horseInfo.setAge((Integer) Utils.toObject(Integer.class, HorseInfoUtils.getAge(value)));
				horseInfo.setDonu(HorseInfoUtils.getDonuKey(value));
				horseInfo.setGender(HorseInfoUtils.getGenderKey(value));
			} else if (BIRTH_DATE.getKey().equals(key)) {
				horseInfo.setBirthdate(DateUtils.toShortDate(value, HorseInfoUtils.stringPattern));
			} else if (HP.getKey().equals(key)) {
				horseInfo.setHPoint(StringUtils.isEmpty(value) ? 0 : Integer.parseInt(value));
			} else if (FATHER.getKey().equals(key)) {
				horseInfo.setFatherName(value);
			} else if (MOTHER.getKey().equals(key)) {
				String[] names = value.split("/");
				horseInfo.setMotherName(names[0].trim());
				horseInfo.setMaidenFatherName(names[1].trim());
			} else if (OWNER.getKey().equals(key)) {
				horseInfo.setOwner(value);
			} else if (GROWER.getKey().equals(key)) {
				horseInfo.setGrower(value);
			}
		} catch (Exception e) {
			log.error("An error occured when get and parsing horse detail data", e);
		}
	}
}
