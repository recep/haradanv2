package com.haradan.common.enumeration;

public enum AuthorityType {

    ROLE_USER(Names.ROLE_USER), ROLE_ADMIN(Names.ROLE_ADMIN);

    private String name;

    AuthorityType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static class Names {
        public static final String ROLE_USER = "{authority=ROLE_USER}";
        public static final String ROLE_ADMIN = "{authority=ROLE_ADMIN}";
    }

}
