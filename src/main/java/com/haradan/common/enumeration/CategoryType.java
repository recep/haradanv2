package com.haradan.common.enumeration;

import com.haradan.common.util.Utils;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Getter
public enum CategoryType {
    STALLION_AR(15, "Arap Aygır"),
    STALLION_EN(14, "İngiliz Aygır"),
    FARRIER(13, "Nalbantlar"),
    HOSTEL(12, "Pansiyon Haralar"),
    TRANSPORT(11, "At Nakliyesi"),
    JAVELIN(10, "Satılık Cirit Atı"),
    RAHVAN(9, "Satılık Rahvan Atı"),
    PONY(8, "Satılık Pony"),
    PASSENGER(7, "Satılık Binek Atı"),
    STALLION(6, "Satılık Aygır"),
    MARE(5, "Satılık Kısrak"),
    RACER(4, "Satılık Yarış Atı"),
    MATING(3, "Aşım Hizmetleri", STALLION_EN, STALLION_AR),
    SERVICES(2, "At Hizmetleri", TRANSPORT, HOSTEL, FARRIER),
    HORSES(1, "Satılık Atlar", RACER, MARE, STALLION, PASSENGER, PONY, RAHVAN, JAVELIN);

    Integer id;
    String name;
    String searchText;
    CategoryType[] subCategories;

    CategoryType() {
    }

    CategoryType(Integer id, String name, CategoryType... subCategories) {
        this.id = id;
        this.name = name;
        this.searchText = Utils.normalizeCharacters(name);
        this.subCategories = subCategories;
    }

    public static List<CategoryType> getMainCategories() {
        List<CategoryType> categoryTypeList = new ArrayList<>();
        categoryTypeList.add(CategoryType.HORSES);
        categoryTypeList.add(CategoryType.SERVICES);
        categoryTypeList.add(CategoryType.MATING);
        return categoryTypeList;
    }

    public static List<CategoryType> getSubCategories(Integer mainCategoryId) {
        CategoryType category = getCategory(mainCategoryId);
        return Arrays.asList(category.getSubCategories());
    }

    public static List<CategoryType> getSubCategories(String mainCategoryName) {
        CategoryType category = getCategory(mainCategoryName);
        return Arrays.asList(category.getSubCategories());
    }

    public static CategoryType getCategory(Integer id) {
        for (CategoryType category : CategoryType.values()) {
            if (category.getId() == id) return category;
        }
        return null;
    }

    public static CategoryType getCategory(String name) {
        for (CategoryType category : CategoryType.values()) {
            if (category.toString().equals(name)) return category;
        }
        return null;
    }
}
