package com.haradan.common.enumeration;

import com.haradan.common.util.Constants;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@AllArgsConstructor
public enum PhotoType {
    MAINPAGE("_m", "whiteImage_m.png", 340, 268);
    //DETAIL_BIG("_d", "whiteImage_d.png", 368, 290),
    //DETAIL_SMALL("_t", "whiteImage_t.png", 100, 79);

    @Getter
    private String suffix;
    @Getter
    private String whiteImage;
    @Getter
    private int width;
    @Getter
    private int height;

    public static boolean isResized(String fileName) {
        return Arrays.stream(PhotoType.values()).anyMatch(p -> fileName.contains(p.suffix));
    }

    public static String of(String fileName, PhotoType type) {
        return new StringBuilder(fileName)
                .insert(fileName.lastIndexOf(Constants.dat), type.getSuffix())
                .toString();
    }


    public static String getMainPageVersion(String fileName) {
        return of(fileName, PhotoType.MAINPAGE);
    }

    /*
        public static String getThumbnail(String fileName) {
            return getVersionOf(fileName, PhotoType.DETAIL_SMALL);
        }

        public static String getDetailVersion(String fileName) {
            return getVersionOf(fileName, PhotoType.DETAIL_BIG);
        }
    */
}
