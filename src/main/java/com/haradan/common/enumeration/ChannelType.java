package com.haradan.common.enumeration;

public enum ChannelType {
    FORM, FACEBOOK, GOOGLE, ADMIN_FORM;
}
