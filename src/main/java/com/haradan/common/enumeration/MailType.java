package com.haradan.common.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum MailType {
    ADVERT_START("%s nolu ilan oluşturuluyor.", "mail/advert-start", EntityStatus.NOT_COMPLETED),
    ADVERT_CREATE("Tebrikler, %s nolu ilanınız oluşturuldu, moderator onayı ile yayınlanacaktır", "mail/advert-create", EntityStatus.NOT_COMPLETED),
    ADVERT_UPDATE("Tebrikler, %s nolu ilanınız güncellendi, moderator onayı ile yayınlanacaktır", "mail/advert-update", EntityStatus.WAITING_APPROVAL),
    ADVERT_APPROVE("%s nolu ilanınız yayınlandı", "mail/advert-approve", EntityStatus.ACTIVE),
    ADVERT_REJECT("%s nolu ilanınız ilan verme kurallarına uymadığından yayınlanamamıştır", "mail/advert-reject", EntityStatus.REJECTED),
    ADVERT_PASSIVE("%s nolu ilanınız yayından kaldırılmıştır", "mail/advert-passive", EntityStatus.PASSIVE),
    ADVERT_DELETE("%s nolu ilanınız silinmiştir", "mail/advert-passive", EntityStatus.DELETED),
    ADVERT_DOPING("%s nolu ilanınıza doping yapılmıştır", "mail/advert-doping", EntityStatus.DEFAULT),
    ADVERT_DOPING_EXPIRE("%s nolu ilanınıza yaptığınız doping süresi dolmuştur.", "mail/advert-doping-expire", EntityStatus.DEFAULT),
    REGISTRATION("Haradan Ailesine Hoş Geldiniz", "mail/registration", EntityStatus.DEFAULT),
    CONTACT("Haradan.com | Mesajınız Var", "mail/contact", EntityStatus.DEFAULT),
    PASSWORD_FORGOT("Haradan.com | Şifremi Unuttum", "mail/password-forgot", EntityStatus.DEFAULT),
    PASSWORD_CHANGE("Haradan.com | Şifre Güncelleme", "mail/password-change", EntityStatus.DEFAULT),
    CREATE_NEW_PASSWORD("Haradan.com | Yeni Şifre Oluşturma", "mail/create-new-password", EntityStatus.DEFAULT);

    private String subject;
    private String template;
    private EntityStatus entityStatus;

    public String getSubject(String advertNo) {
        return String.format(this.subject, advertNo);
    }
}
