package com.haradan.common.enumeration;

public enum EntityStatus {
	DEFAULT, ACTIVE, PASSIVE, DELETED, NOT_COMPLETED, SOLD, WAITING_APPROVAL, REJECTED
}
