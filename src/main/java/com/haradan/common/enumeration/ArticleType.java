package com.haradan.common.enumeration;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ArticleType {
    JOB_ADVERTISEMENT("İş İlanı","is-ilani"),
    GENERAL("Genel","genel"),
    HORSE_SERVICE("Atçılık Hizmeti","atcilik-hizmeti"),
    RACING("Yarışçılık","yariscilik"),
    CORPORATE_ADVERT("Eküri İlanı","ekuri-ilanı"),
    STALLION_INTRODUCE("Aygır Tanıtım","aygir");

    private final String name;
    private final String shortName;
}