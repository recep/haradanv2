package com.haradan.common.enumeration;

public enum PaymentProductType {
	ADVERT("advert", "İlan Ücreti"),
	DOPING1("doping", "Vitrin İlanı - 1 Ay"),
	BANNER1("banner", "Reklam Alanı - 1 Ay"),
	CORP_AD1("corporateAd", "Eküri İlanı - 1 Ay"),
	CORP_AD3("corporateAd", "Eküri İlanı - 3 Ay"),
	CORP_AD6("corporateAd", "Eküri İlanı - 6 Ay"),
	STALLION("stallion", "Aygır Tanıtım");

	private String value;
	private String desc;

	PaymentProductType(String value, String desc) {
		this.value = value;
		this.desc = desc;
	}

	public String getValue() {
		return value;
	}

	public String getDesc() {
		return desc;
	}

	public static PaymentProductType getPaymentProduct(String value) {
		for (PaymentProductType paymentProduct : PaymentProductType.values()) {
			if (paymentProduct.getValue().equals(value)) {
				return paymentProduct;
			}
		}
		return PaymentProductType.ADVERT;
	}
}