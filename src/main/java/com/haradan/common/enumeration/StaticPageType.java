package com.haradan.common.enumeration;

import lombok.Getter;

import java.util.stream.Stream;

@Getter
public enum StaticPageType {
    STALLION_INTRODUCE(Path.STALLION_INTRODUCE, "stallion-introduce"),
    CORPORATE_ADVERT(Path.CORPORATE_ADVERT, "corporate-advert"),
    HOW_TO_GIVE_AD(Path.HOW_TO_GIVE_AD, "how-to-give-ad"),
    GIVE_ADVERTISEMENT(Path.GIVE_ADVERTISEMENT, "give-advertisement"),
    GIVE_CORPORATE_AD(Path.GIVE_CORPORATE_AD, "give-corporate-ad"),
    GIVE_AD_WITH_SOCIAL(Path.GIVE_AD_WITH_SOCIAL, "give-ad-with-social-account"),
    AD_CREATION_RULE(Path.AD_CREATION_RULE, "ad-creation-rules"),
    ABOUT(Path.ABOUT, "about"),
    CONTACT(Path.CONTACT, "contact"),
    SERVICES(Path.SERVICES, "services"),
    MEMBERSHIP_AGGREMENT(Path.MEMBERSHIP_AGGREMENT, "membership-aggrement"),
    BANNED_PRODUCT_LIST(Path.BANNED_PRODUCT_LIST, "banned-product-list"),
    TERMS_OF_USE(Path.TERMS_OF_USE, "term-of-use"),
    PRIVACY_POLICY(Path.PRIVACY_POLICY, "privacy-policy"),
    KVKK(Path.KVKK, "kvkk"),
    HELP(Path.HELP, "help"),
	DOPINGS(Path.DOPINGS, "dopings"),
    ERROR(Path.ERROR, Path.ERROR);

    private String path;
    private String page;

    StaticPageType(String path, String page) {
        this.path = path;
        this.page = "static/" + page;
    }

    public static String getPage(String path) {
        return StaticPageType.stream()
                .filter(p -> p.getPath().equals(path))
                .findAny()
                .get()
                .getPage();
    }

    public static void append(StringBuilder builder) {
        stream().forEach(p -> builder.append(",/".concat(p.getPath())));
    }

    public static Stream<StaticPageType> stream() {
        return Stream.of(StaticPageType.values());
    }

    public interface Path {
        String STALLION_INTRODUCE = "aygir-tanitimi";
        String CORPORATE_ADVERT = "ekuri-ilani";
        String HOW_TO_GIVE_AD = "nasil-ilan-verebilirim";
        String GIVE_ADVERTISEMENT = "reklam-ver";
        String GIVE_CORPORATE_AD = "ekuri-ilani-ver";
        String GIVE_AD_WITH_SOCIAL = "sosyal-hesapla-ilan-ver";
        String AD_CREATION_RULE = "ilan-verme-kurallari";
        String ABOUT = "hakkimizda";
        String CONTACT = "iletisim";
        String SERVICES = "hizmetlerimiz";
        String MEMBERSHIP_AGGREMENT = "uyelik-sozlesmesi";
        String TERMS_OF_USE = "kullanim-kosullari";
        String PRIVACY_POLICY = "gizlilik";
        String KVKK = "kvkk";
        String HELP = "yardim";
        String BANNED_PRODUCT_LIST = "yasakli-urunler-listesi";
        String DOPINGS = "dopingler";
        String ERROR = "error";
    }
}