package com.haradan.common.interceptor;

import com.haradan.common.util.CookieUtils;
import com.haradan.domain.dao.entity.User;
import com.haradan.domain.service.SecurityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

@Slf4j
@Component
public class RequestInterceptor implements HandlerInterceptor {
    private static final String DEFAULT_HEADER_SUFFIX = " | haradan.com";
    private static final String DEFAULT_HEADER = "Satılık Yarış Atı İlanları";
    private static final String ACCESS_TOKEN = "access_token";

    @Autowired
    ApplicationContext applicationContext;

    @Autowired
    SecurityService securityService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        if (!isStaticResourceUrl(request) && !securityService.isAuthenticated()) {
            authenticate(request, response);
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
        if (!isStaticResourceUrl(request)) {
            addSeoDetails(request);
            addUserDetails(request);
        }
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        // NA
    }

    private boolean isStaticResourceUrl(HttpServletRequest request) {
        return request.getRequestURI().contains("/static")
                || request.getRequestURI().contains("/js")
                || request.getRequestURI().contains("/css")
                || request.getRequestURI().contains("/fonts")
                || request.getRequestURI().contains("/error")
                || request.getRequestURI().contains(".pdf")
                || request.getRequestURI().contains(".png")
                || request.getRequestURI().contains(".jpg")
                || request.getRequestURI().contains(".jpeg")
                || request.getRequestURI().contains(".gif");
    }

    private void addUserDetails(HttpServletRequest request) {
        if (securityService.isAuthenticated()) {
            request.setAttribute("userInfo", getUserInfo());
            request.setAttribute("isAdmin", securityService.isAdmin());
        }
    }

    private boolean authenticate(HttpServletRequest request, HttpServletResponse response) {
        Optional<Cookie> optCookie = CookieUtils.getCookie(request, ACCESS_TOKEN);
        String token = optCookie.orElse(new Cookie(ACCESS_TOKEN, null)).getValue();
        if (StringUtils.isEmpty(token)) {
            return false;
        }
        try {
            ResourceServerTokenServices tokenServices = applicationContext.getBean("tokenServices", ResourceServerTokenServices.class);
            SecurityContextHolder.getContext().setAuthentication(tokenServices.loadAuthentication(token));
            return true;
        } catch (Exception e) {
            CookieUtils.deleteCookie(request, response, ACCESS_TOKEN);
            log.error("An error occured on Authentication.", e);
            return false;
        }
    }

    private User getUserInfo() {
        User loggedIn = securityService.getLoginUser();
        User user = new User();
        user.setIdentifier(loggedIn.getIdentifier());
        user.setFirstName(loggedIn.getFirstName());
        user.setLastName(loggedIn.getLastName());
        user.setChannel(loggedIn.getChannel());
        return user;
    }

    private void addSeoDetails(HttpServletRequest request) {
        Object ogTitle = request.getAttribute("ogTitle");
        request.setAttribute("ogTitle",
                (StringUtils.isEmpty(ogTitle) ? DEFAULT_HEADER : ogTitle.toString()) + DEFAULT_HEADER_SUFFIX);
        request.setAttribute("ogUrl", "https://haradan.com");
        request.setAttribute("ogType", "haradan.com");
        request.setAttribute("ogImage", "/img/logo.png");
    }
}
