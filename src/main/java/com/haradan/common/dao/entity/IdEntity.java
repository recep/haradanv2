package com.haradan.common.dao.entity;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.core.style.ToStringCreator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@MappedSuperclass
@Inheritance(
        strategy = InheritanceType.TABLE_PER_CLASS
)
public abstract class IdEntity implements Serializable {
    protected static final int ID_LENGTH = 50;
    protected static final int CODE_LENGTH = 250;
    protected static final int NAME_LENGTH = 250;
    protected static final int USERNAME_LENGTH = 250;
    protected static final int STATUS_LENGTH = 50;
    protected static final int LANG_CODE_LENGTH = 5;
    protected static final int COUNTRY_CODE_LENGTH = 5;
    protected static final int DESC_LENGTH = 2000;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Column(name = "id", length = 50)
    protected String identifier;

    protected IdEntity() {
    }

    public String getIdentifier() {
        return this.identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public int hashCode() {
        return Objects.hash(new Object[]{this.identifier});
    }

    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        } else {
            return obj != null && this.getClass() == obj.getClass() && Objects.equals(this.identifier, ((IdEntity) obj).identifier);
        }
    }

    public String toString() {
        return this.toString(new ToStringCreator(this)).toString();
    }

    protected ToStringCreator toString(ToStringCreator creator) {
        return creator.append("id", this.getIdentifier());
    }
}