package com.haradan.common.dao.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.time.ZonedDateTime;

@Data
@EqualsAndHashCode(callSuper = false)
@MappedSuperclass
public abstract class BaseAuditEntity extends BaseEntity {
	private static final long serialVersionUID = 13453465435L;

	//@CreatedBy
	//@Column(name = "created_by", updatable = false)
	//private String createdBy;

	@LastModifiedDate
	@Column(name = "last_modified_date")
	@JsonIgnore
	private ZonedDateTime lastModifiedDate = ZonedDateTime.now();

	//@LastModifiedBy
	//@Column(name = "last_modified_by")
	//private String lastModifiedBy;


}
