package com.haradan.common.dao.repository;

import org.springframework.data.domain.Page;

import java.util.List;

public class CustomPagedResourceConverter {

    public static <T> CustomPagedResource<List<T>> map(Page<T> page) {
        return map(page, null, null);
    }

    public static <T> CustomPagedResource<List<T>> map(Page<T> page, String sort, String direction) {
        return new CustomPagedResource<>(page.getTotalElements(), page.getTotalPages(), page.getNumber(), page.getSize(), sort, direction, page.getContent());
    }
}