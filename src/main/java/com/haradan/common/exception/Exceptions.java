package com.haradan.common.exception;

import com.haradan.common.exception.dto.ExceptionData;

public interface Exceptions {
    ExceptionData DATA_NOT_FOUND = new ExceptionData(10000L, "{item} not found, id: {id} ");
    ExceptionData SERVICE_CALL = new ExceptionData(10001L, "An error occured when call {service}, {detail}");
    ExceptionData USER_NOT_FOUND = new ExceptionData(10002L, "{email} mail adresine kayıtlı kulllanıcı bulunamadı.");
    ExceptionData USER_ALREADY_EXIST = new ExceptionData(10003L, "Kullanıcı sistemde kayıtlı.");
    ExceptionData USER_ID_EMAIL_NOT_MATCH = new ExceptionData(10004L, "Kulllanıcı bilgileri hatalı.");
    ExceptionData PASSWORD_NOT_MATCH = new ExceptionData(10005L, "Mevcut şifreniz hatalı.");
    ExceptionData RESET_PASSWORD_KEY_NOT_MATCH = new ExceptionData(10006L, "Bu işlemi yapmaya yetkiniz bulunmamaktadır.");
    ExceptionData FAVOURITE_ALREADY_EXIST = new ExceptionData(10007L, "Bu ilan favorilerinize zaten ekli.");
    ExceptionData FAVOURITE_NOT_FOUND = new ExceptionData(10008L, "Favori ilan bulunamadı.");
	ExceptionData USER_REGISTERED_WITH_GOOGLE = new ExceptionData(10009L, "Bu e-posta adresi daha önceden Google hesabıyla giriş yapmış. Giriş sayfasından 'Google ile giriş yap' ile devam ediniz.");
	ExceptionData USER_REGISTERED_WITH_FACEBOOK = new ExceptionData(10010L, "Bu e-posta adresi daha önceden Facebook hesabıyla giriş yapmış. Giriş sayfasından 'Facebook ile giriş yap' ile devam ediniz.");
	ExceptionData USER_REGISTERED_WITH_EMAIL = new ExceptionData(10011L, "Giriş yapılan hesaba bağlı e-posta adresi sistemde kayıtlı. Giriş formu ile devam ediniz.");
    ExceptionData INVALID_PHONE_NUMBER = new ExceptionData(10012L, "Geçerli bir telefon numarası giriniz.");;
}
