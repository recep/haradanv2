package com.haradan.common.exception;

import com.haradan.common.exception.dto.ExceptionData;
import com.haradan.common.exception.dto.ValidationError;
import com.haradan.common.exception.exceptions.InvalidRequestException;
import com.haradan.common.exception.exceptions.MicroException;
import com.haradan.common.util.Constants;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestControllerAdvice
public class MicroExceptionHandler extends ResponseEntityExceptionHandler {
    @Value("${spring.application.name}")
    private String application;

    private static final String TRACE_ID_KEY = "X-B3-TraceId";

    @ExceptionHandler(value = AccessDeniedException.class)
    public RedirectView handleMicroException(HttpServletRequest request) {
        String ref = request.getRequestURI().substring(1);
        return new RedirectView(Constants.LOGIN_WITH_REF + ref);
    }

    @ExceptionHandler(value = InvalidRequestException.class)
    public ResponseEntity<ExceptionData> handleMicroException(InvalidRequestException ex) {
        setDefaultValue(ex, ex.getData());
        return new ResponseEntity<>(ex.getData(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = MicroException.class)
    public ResponseEntity<ExceptionData> handleMicroException(MicroException ex) {
        setDefaultValue(ex, ex.getData());
        return new ResponseEntity<>(ex.getData(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler
    public ResponseEntity<ExceptionData> handleException(Exception ex) {
        String message = ExceptionUtils.getRootCauseMessage(ex) != null ? ExceptionUtils.getRootCauseMessage(ex) : ExceptionUtils.getMessage(ex);
        ExceptionData exceptionData = new ExceptionData(MicroErrorType.INTERNAL_ERROR.getCode(), message);
        setDefaultValue(ex, exceptionData);
        return new ResponseEntity<>(exceptionData, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Override
    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ExceptionData exceptionData = new ExceptionData(MicroErrorType.VALIDATION_ERROR.getCode(), MicroErrorType.VALIDATION_ERROR.getMessage());
        List<ValidationError> errors = new ArrayList<ValidationError>();
        for (FieldError error : ex.getBindingResult().
                getFieldErrors()) {
            errors.add(new ValidationError(error.getField(), error.getDefaultMessage()));
        }
        exceptionData.setErrors(errors);
        setDefaultValue(ex, exceptionData);
        return handleExceptionInternal(ex, exceptionData, headers, HttpStatus.BAD_REQUEST, request);
    }

    private void setDefaultValue(Exception ex, ExceptionData exceptionData) {
        log.error(ExceptionUtils.getStackTrace(ex));
        exceptionData.setTraceId(
                StringUtils.isEmpty(exceptionData.getTraceId()) ? MDC.get(TRACE_ID_KEY) : exceptionData.getTraceId());
        exceptionData.setApplication(
                StringUtils.isEmpty(exceptionData.getApplication()) ? application : exceptionData.getApplication());
        exceptionData.getParameters().forEach((k, v) -> {
            exceptionData.setErrorMessage(exceptionData.getErrorMessage().replaceAll("\\{" + k + "\\}", v));
        });
    }
}