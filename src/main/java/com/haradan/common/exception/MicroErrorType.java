package com.haradan.common.exception;

public enum MicroErrorType {
	INVALID_REQUEST(997, "Invalid request"), 
	VALIDATION_ERROR(998, "Validation error"),
	INTERNAL_ERROR(999, "Internal error");

	long code;
	String message;

	MicroErrorType(long code, String message) {
		this.code = code;
		this.message = message;
	}

	public long getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
}
