package com.haradan.common.exception.exceptions;

public class InvalidRequestException extends MicroException {
	private static final long serialVersionUID = -6529565876205743790L;

	public InvalidRequestException(Long errorCode) {
		super(errorCode);
	}

	public InvalidRequestException(Long errorCode, String errorMessage) {
		super(errorCode, errorMessage);
	}

	public InvalidRequestException(String application, Long errorCode, String errorMessage) {
		super(application, errorCode, errorMessage);
	}
}
