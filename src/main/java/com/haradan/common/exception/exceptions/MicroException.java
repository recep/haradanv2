package com.haradan.common.exception.exceptions;

import com.haradan.common.exception.Exceptions;
import com.haradan.common.exception.dto.ExceptionData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class MicroException extends RuntimeException {
    private static final long serialVersionUID = -6529565876205743790L;
    ExceptionData data;

    public MicroException(Long errorCode) {
        this(new ExceptionData(errorCode));
    }

    public MicroException(Long errorCode, String errorMessage) {
        this(new ExceptionData(errorCode, errorMessage));
    }

    public MicroException(String application, Long errorCode, String errorMessage) {
        this(new ExceptionData(application, errorCode, errorMessage));
    }

    public static MicroException newDataNotFoundException(String item, String id) {
        ExceptionData exceptionData = Exceptions.DATA_NOT_FOUND;
        exceptionData.addParameter("item", item);
        exceptionData.addParameter("id", id);
        return new MicroException(exceptionData);
    }

    public static MicroException newServiceCallException(String service, String detail) {
        ExceptionData exceptionData = Exceptions.SERVICE_CALL;
        exceptionData.addParameter("service", service);
        exceptionData.addParameter("detail", detail);
        return new MicroException(exceptionData);
    }
}
