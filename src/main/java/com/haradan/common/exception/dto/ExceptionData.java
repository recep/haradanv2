package com.haradan.common.exception.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
public class ExceptionData implements Serializable {
    private static final long serialVersionUID = -3870523159485127932L;
    private String application;
    private Long errorCode;
    private String errorMessage;
    private String traceId;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<ValidationError> errors;
    private Map<String, String> parameters = new HashMap<String, String>();

    public ExceptionData(Long errorCode) {
        this.errorCode = errorCode;
    }

    public ExceptionData(Long errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public ExceptionData(String application, Long errorCode, String errorMessage) {
        this.application = application;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public ExceptionData(ExceptionData exceptionData) {
        this.application = exceptionData.getApplication();
        this.errorCode = exceptionData.getErrorCode();
        this.errorMessage = exceptionData.getErrorMessage();
    }

    public void addParameter(String key, String value) {
        this.parameters.put(key, value);
    }

    public ExceptionData appendParameter(String key, String value) {
        this.parameters.put(key, value);
        return this;
    }
}
