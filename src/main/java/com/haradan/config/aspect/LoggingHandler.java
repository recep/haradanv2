package com.haradan.config.aspect;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.beans.Introspector;
import java.util.Arrays;
import java.util.stream.Collectors;

@Slf4j
@Aspect
@Component
public class LoggingHandler {

    @Pointcut("execution(* com.haradan.domain.controller.*.*(..))")
    public void controller() {
    }

    //@Before("controller()")
    public void logMethodBefore(JoinPoint joinPoint) {
        String serviceName = joinPoint.getTarget().getClass().getSimpleName();
        String methodName = joinPoint.getSignature().getName();
        log.info(Introspector.decapitalize(serviceName) + "." + methodName + "(), args=["
                + getRequestStr(joinPoint.getArgs()) + "] calling");
    }

    //@AfterReturning(pointcut = "controller()", returning = "object")
    public void logMethodAfterReturning(JoinPoint joinPoint, Object object) {
        String serviceName = joinPoint.getTarget().getClass().getSimpleName();
        String methodName = joinPoint.getSignature().getName();
        log.info(Introspector.decapitalize(serviceName) + "." + methodName + "() called jsonresponse:"
                + object.toString());
    }

    @AfterThrowing(pointcut = "controller()", throwing = "error")
    public void logMethodAfterReturning(JoinPoint joinPoint, Throwable error) {
        String serviceName = joinPoint.getTarget().getClass().getSimpleName();
        String methodName = joinPoint.getSignature().getName();
        log.error(
                Introspector.decapitalize(serviceName) + "." + methodName
                        + ", args=[" + getRequestStr(joinPoint.getArgs()) + "] calling"
                        + "() got exception:" + ExceptionUtils.getRootCauseMessage(error),
                error);
    }

    private String getRequestStr(Object[] args) {
        return Arrays.stream(args).map(Object::toString).collect(Collectors.joining(","));
    }
}
