package com.haradan.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationManager;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.web.context.request.RequestContextListener;

import javax.servlet.http.HttpServletResponse;

@Configuration
@EnableResourceServer
@EnableConfigurationProperties
@EnableOAuth2Client
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {


    @Autowired
    private JwtAccessTokenConverter jwtAccessTokenConverter;

    @Autowired
    private Environment environment;

    @javax.annotation.Resource(name = "tokenServices")
    private ResourceServerTokenServices tokenServices;

    @Bean
    @ConfigurationProperties(prefix = "spring.security.oauth2.client")
    public ClientCredentialsResourceDetails clientCredentialsResourceDetails() {
        return new ClientCredentialsResourceDetails();
    }

    @Bean
    @Primary
    public AuthenticationManager authenticationManager() {
        final OAuth2AuthenticationManager oAuth2AuthenticationManager = new OAuth2AuthenticationManager();
        oAuth2AuthenticationManager.setTokenServices(tokenServices);
        return oAuth2AuthenticationManager;
    }

    @Override
    public void configure(final ResourceServerSecurityConfigurer config) {
        config.tokenServices(tokenServices).authenticationManager(authenticationManager());
    }

    @Override
    public void configure(final HttpSecurity http) throws Exception {
        http.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .exceptionHandling().authenticationEntryPoint((request, response, authException) -> response.sendError(HttpServletResponse.SC_UNAUTHORIZED))
                .accessDeniedHandler(new OAuth2AccessDeniedHandler())
                .and()
                .csrf().disable()
                .headers()
                .frameOptions().sameOrigin()
                .httpStrictTransportSecurity().disable().and()
                .authorizeRequests()
                //.antMatchers(getSecuredMatchers()).authenticated()
                .antMatchers("/", "/**").permitAll()
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                .antMatchers(HttpMethod.GET, "/**").permitAll()
                .antMatchers(HttpMethod.GET, "/**").access("#oauth2.hasScope('r')")
                .antMatchers(HttpMethod.POST, "/**").access("#oauth2.hasScope('w')")
                .antMatchers(HttpMethod.PUT, "/**").access("#oauth2.hasScope('w')")
                .antMatchers(HttpMethod.PATCH, "/**").access("#oauth2.hasScope('w')")
                .antMatchers(HttpMethod.DELETE, "/**").access("#oauth2.hasScope('w')")
                .anyRequest().permitAll();
    }

    private String[] getSecuredMatchers() {
        StringBuilder builder = new StringBuilder();
        builder.append("/users/**");
        builder.append(",/adverts/**");
        builder.append(",/articles/**");
        builder.append(",/categories/**");
        builder.append(",/properties/**");
        builder.append(",/banners/**");
        builder.append(",/files/**");
        builder.append(",/horses/**");
        builder.append(",/payment/**");
        return builder.toString().split(",");
    }

    @Bean
    @ConditionalOnMissingBean(RequestContextListener.class)
    public RequestContextListener requestContextListener() {
        return new RequestContextListener();
    }

}