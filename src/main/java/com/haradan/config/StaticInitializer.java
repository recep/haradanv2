package com.haradan.config;

import com.haradan.domain.service.notification.NotificationUtils;
import com.haradan.domain.service.file.resize.impl.ImageMagickTool;
import com.haradan.domain.service.file.upload.FileOperationService;
import com.haradan.domain.service.file.resize.PhotoResizeFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class StaticInitializer  {

	@Value("${system.parameter.baseUrl}")
	private String baseUrl;
	@Value("${system.parameter.haradanEmail}")
	private String haradanEmail;
	@Value("${system.parameter.file.root}")
	private String root;
	@Value("${system.parameter.file.resizeTool}")
	private String resizeTool;
	@Value("${system.parameter.file.mergeWhite:false}")
	private boolean mergeWhite;
	@Value("${system.parameter.file.whiteImagePath:classpath:/static/img/white/}")
	private String whiteImagePath;
	@Value("${system.parameter.file.imageMagickWindowsPath:magick}")
	private String imageMagickWindowsPath;

	@PostConstruct
	void init() {
		NotificationUtils.baseUrl = baseUrl;
		NotificationUtils.haradanEmail = haradanEmail;
		FileOperationService.photoResize = PhotoResizeFactory.getResizeTool(resizeTool);
		ImageMagickTool.root = root;
		ImageMagickTool.mergeWhite = mergeWhite;
		ImageMagickTool.whiteImagePath = whiteImagePath;
		ImageMagickTool.imageMagickWindowsPath = imageMagickWindowsPath;
	}
}