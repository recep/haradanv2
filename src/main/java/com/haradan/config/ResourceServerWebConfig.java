package com.haradan.config;

import com.haradan.common.interceptor.RequestInterceptor;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mobile.device.DeviceHandlerMethodArgumentResolver;
import org.springframework.mobile.device.DeviceResolverHandlerInterceptor;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.List;

@Data
@Configuration
@ConfigurationProperties(prefix = "system.parameter.file")
public class ResourceServerWebConfig extends WebMvcConfigurationSupport {


    @Autowired
    private RequestInterceptor requestInterceptor;

    String root;
    List<String> categoryPaths;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
        //Seo
        registry.addResourceHandler("sitemap.xml").addResourceLocations("classpath:/seo/sitemap.xml");
        registry.addResourceHandler("robots.txt").addResourceLocations("classpath:/seo/robots.txt");
        registry.addResourceHandler("google827d34eec0d83827.html").addResourceLocations("classpath:/seo/google827d34eec0d83827.html");
        registry.addResourceHandler("yandex_57a1cb0697792d25.html").addResourceLocations("classpath:/seo/yandex_57a1cb0697792d25.html");
        registry.addResourceHandler("BingSiteAuth.xml").addResourceLocations("classpath:/seo/BingSiteAuth.xml");

        //Other static resources
        registry.addResourceHandler("static/**").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("admin/**").addResourceLocations("classpath:/static/admin/");
        registry.addResourceHandler("js/**").addResourceLocations("classpath:/static/js/");
        registry.addResourceHandler("css/**").addResourceLocations("classpath:/static/css/");
        registry.addResourceHandler("img/**").addResourceLocations("classpath:/static/img/");
        registry.addResourceHandler("fonts/**").addResourceLocations("classpath:/static/fonts/");
        categoryPaths.forEach(p -> {
            registry.addResourceHandler(p + "/**").addResourceLocations("file:" + root);
        });
    }

    @Bean
    public DeviceResolverHandlerInterceptor deviceResolverHandlerInterceptor() {
        return new DeviceResolverHandlerInterceptor();
    }

    @Bean
    public DeviceHandlerMethodArgumentResolver deviceHandlerMethodArgumentResolver() {
        return new DeviceHandlerMethodArgumentResolver();
    }

    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(requestInterceptor);
        registry.addInterceptor(deviceResolverHandlerInterceptor());
    }

    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(deviceHandlerMethodArgumentResolver());
    }
}