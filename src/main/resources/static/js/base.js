var baseUrl = "https://haradan.com/";

if(window.location.href.indexOf("localhost") != -1) {
	baseUrl = "http://localhost:8080/";
} else if(window.location.href.indexOf("test.haradan") != -1) {
	baseUrl = "https://test.haradan.com/";
}

//operation types
const _ADD = "ADD";
const _UPDATE = "UPDATE";

//status codes
const _ACTIVE = "ACTIVE";
const _PASSIVE = "PASSIVE";
const _DELETED = "DELETED";
const _WAITING_APPROVAL = "WAITING_APPROVAL";
const _NOT_COMPLETED = "NOT_COMPLETED";

//Ajax call method types
const _GET = "GET";
const _POST = "POST";
const _DELETE = "DELETE";
const _PUT = "PUT";


$(document).ready(function () {

	//Mask phone number
	$(".phone-number").mask('(000) 000-0000');
			
	//Mask price	
	$('.price').mask("#,##0.00", {reverse: true});
	
	//Fill unformatted phone number textbox
	$('.phone-number').on('keyup', function () {
		var phoneNumber = $(this).val().replaceAll(" ", "").replaceAll("-", "").replaceAll("(", "").replaceAll(")", "");
		$("#phoneNumber").val(phoneNumber);
	});
	
	//Check two values equal or not
	$.validator.addMethod("valueNotEquals", function(value, element, arg) {
		return arg != element.value;
	}, "Value must not equal arg.");
	
	//Check a value greater than other
	$.validator.addMethod("greaterThan", function(value, element, params) {

	    if (!/Invalid|NaN/.test(new Date(value))) {
	        return new Date(value) > new Date($(params).val());
	    }

    	return isNaN(value) && isNaN($(params).val()) 
        	|| (Number(value) > Number($(params).val())); 
	},'Must be greater than {0}.');
	
	//General search
	$(".general-search-input").keyup(function(event) {
	    if (event.keyCode === 13) {
	        searchAds($(this).val());
	    }
	});

});

//headerdaki search de çalışır
function searchAds(searchText){
	if($.trim(searchText) != ''){
		window.location.href = baseUrl + 'at-bilgileri/' + $.trim(searchText);
	}else{
		showErrorMsg(messages.NO_KEYWORD_ENTER);
	}
}

//Ajax call to get
function ajaxJsonGet(url, data, success, error) {
	ajaxJsonCall(url, _GET, data, success, error)
}

//Ajax call to post
function ajaxJsonPost(url, data, success, error) {
	ajaxJsonCall(url, _POST, data, success, error)
}

//Ajax call to delete
function ajaxJsonDelete(url, data, success, error) {
	ajaxJsonCall(url, _DELETE, data, success, error)
}

//Ajax call to put
function ajaxJsonPut(url, data, success, error) {
	ajaxJsonCall(url, _PUT, data, success, error)
}

//make generic ajax call
function ajaxJsonCall(url, type, data, success, error) {
	var headers = {};
	if (!isEmpty(getCookie('access_token'))) headers = { Authorization: 'Bearer ' + getCookie('access_token') };
	
	$.ajax({
		url: baseUrl + url,
		data: data,
		type: type,
		processData: true,
		contentType: "application/json",
		timeout: 10000,
		dataType: "json",
		headers: headers,
		beforeSend: function() {
			$('.submit-btn').prepend('<span class="spinner-border spinner-border-sm mr-2" role="status" aria-hidden="true"></span>').attr('disabled', true);
		},
		success: success,
		error: error,
		complete: function() {
			$('.submit-btn').attr('disabled', false).find('span').remove();
		}
	});
}

function ajaxPageCall(url, success, error) {
	var headers = {};
	if (!isEmpty(getCookie('access_token'))) headers = { Authorization: 'Bearer ' + getCookie('access_token') };
	
	$.ajax({
		url: baseUrl + url,
		headers: headers,
		success: success,
		error: error
	});
}

const messages = {
	REGISTER_SUCCESS: "Üyeliğiniz başarılı bir şekilde oluşturuldu. Yönlendiriliyorsunuz, lütfen bekleyiniz...",
	SEND_PASSWORD_LINK_SUCCESS: "Şifrenizi değiştireceğiniz link e-posta adresinize gönderilmiştir.",
	NEW_PASSWORD_SUCCESS: "Şifreniz başarılı bir şekilde oluşturuldu. Giriş sayfasına yönlendiriliyorsunuz...",
	ADMIN_ADD_SUCCESS: "Ekleme işlemi başarılı.",
	ADMIN_UPDATE_SUCCESS: "Bilgiler başarılı bir şekilde güncellendi.",
	UPDATE_ACCOUNT_SUCCESS: "Bilgileriniz başarılı bir şekilde güncellendi.",
	CHANGE_PASSWORD_SUCCESS: "Şifreniz başarılı bir şekilde güncellendi.",
	CONTACT_MESSAGE_SUCCESS: "Mesajınız başarılı bir şekilde gönderildi.",
	AD_ACTION_SUCCESS: "İşleminiz başarılı bir şekilde gerçekleşti.",
	ADD_FAV_SUCCESS: "İlan favorilerinize eklendi.",
	DELETE_STABLE_SUCCESS: "Hara başarılı bir şekilde silindi.",
	NO_KEYWORD_ENTER: "Arama yapmak için bir kelime girin."
}

//Show success message
function showSuccessMsg(msg) {
	//$('#form-success-toast').find(".toast-body").text(msg);
	//$('#form-success-toast').toast('show');
	$('#form-error-alert').addClass('d-none').find(".alert-body").text("");
	$('#form-success-alert').removeClass('d-none').addClass('d-flex').find(".alert-body").text(msg);

}

//Show error message
function showErrorMsg(error) {
	var message = "Sistemsel bir hata oluştu. Lütfen sonra tekrar deneyiniz.";
	if (typeof error.responseJSON.errorCode !== "undefined") message = error.responseJSON.errorMessage;
	//$('#form-error-toast').find(".toast-body").text(message);
	//$('#form-error-toast').toast('show');
	$('#form-error-alert').removeClass('d-none').addClass('d-flex').find(".alert-body").text(message);
}

function clearErrorMsg() {
	$('#form-error-alert').addClass('d-none').removeClass('d-flex').find(".alert-body").text("");
}

//Set cookie
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(new Date().getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

//Get cookie
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

//Check value is empty or not
function isEmpty(value) {
	return (value === null || value === "" || typeof value === "undefined");
}
