# README #

haradan.com readme

### Liquibase ###
* docker-compose up
* ./liquibase update
* ./liquibase --changelog-file=all.changelog.xml generateChangeLog

### Login ###
* Username : admin@haradan.com
* Passwrod : 12345
* Clientid = "haradan";
* ClientSecret = "12345";
    
### Requests ###

GET search: {{url}}/adverts/search/satilik-atlar?size=4&status=ACTIVE&minPrice=50&date=3&userId=19f63fe0-6bc8-46b4-a1d8-f52e964cd764&searchText=ingiliz&hasPhoto=false&page=0&at-adi=test&yas=10

### Security ###

openssl pkcs12 -export -out haradan-ssl-key.p12 -in cert.pem -inkey privkey.pem

keytool -genkeypair -alias selfsigned_localhost_sslserver -keyalg RSA -keysize 2048 -storetype PKCS12 -keystore /Users/recep.yilmaz/development/projects/haradanv2/haradan-ssl-key.p12 -validity 15650

### DB ###

* Open this url on browser  http://localhost:8080/h2-console
* Set below value to JDBC_URL  jdbc:h2:~/haradandb

### Elastic ###

* Username: elastic
* Password: I7BIJgSGTqVCDHvdvWJfnRxS


### Postman set parameters ###

var jsonData = pm.response.json();
pm.globals.set("access-token", jsonData.access_token);
pm.globals.set("refresh-token", jsonData.refresh_token);

### aralarında ilişki olmayan 2 tablo için query ###

class QueryGeneration extends BaseRepository.QueryGeneration {
        public static Specification<Role> search(Set<String> tenantIds, String name, String userId) {
            return (root, query, cb) -> {
                List<Predicate> predicates = getPredicateArray();
                predicates.add(tenantIdsPredicate(cb, root, tenantIds));
                predicates.add(nameLikePredicate(cb, root, name));

                if (StringUtils.isNotBlank(userId)) {
                    Subquery<UserRoles> subquery = query.subquery(UserRoles.class);
                    Root<UserRoles> subRoot = subquery.from(UserRoles.class);
                    subquery.select(subRoot);
                    List<Predicate> subQueryPredicates = getPredicateArray();
                    subQueryPredicates.add(cb.in(subRoot.get("tenantId")).value(tenantIds));
                    subQueryPredicates.add(cb.equal(subRoot.get("userId"), userId));
                    subQueryPredicates.add(cb.equal(subRoot.join("role").get("identifier"), root.get("identifier")));
                    subquery.where(subQueryPredicates.toArray(new Predicate[]{}));
                    predicates.add(cb.exists(subquery));
                }

                return cb.and(predicates.toArray(new Predicate[predicates.size()]));
            };
        }
    }
    
### element collection ###

    @ElementCollection
        @CollectionTable(name = "products_categories", joinColumns = @JoinColumn(name = "product_id"))
        @Column(name = "category_id")
        private Set<String> categoryIds;